﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace LiveInstaller
{
	/// <summary>
	/// Interaction logic for UninstallWindow.xaml
	/// </summary>
	public partial class UninstallWindow : Window
	{
		RegistryKey key;
		InstallWindow iw;

		public UninstallWindow(RegistryKey key)
		{
			InitializeComponent();

			this.key = key;
		}

		private void bCancel_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void bRepair_Click(object sender, RoutedEventArgs e)
		{
			iw = new InstallWindow();
			iw.Closing += iw_Closing;
			iw.ReturnToUninstallWindow = true;
			iw.Show();
			Hide();
		}

		void iw_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
			if(iw.ReturnToUninstallWindow)
			{
				iw = null;
				Show();
			}
			else
			{
				App.Current.Shutdown(0);
			}
		}

		private void bUninstall_Click(object sender, RoutedEventArgs e)
		{
			Task.Factory.StartNew(() => { SwapTabs(1); ProcedeWithUninstall(); SwapTabs(2); });
		}

		private void ProcedeWithUninstall()
		{
			string path = key.GetValue("InstallLocation").ToString() + "\\unins000.exe";

			ProcessStartInfo psi = new ProcessStartInfo();
			psi.FileName = path;
			Process p = Process.Start(psi);
			p.WaitForExit();
		}

		private delegate void d_SwapTabs(int tab);
		private void SwapTabs(int tab)
		{
			if (!Dispatcher.CheckAccess())
			{
				Dispatcher.Invoke(new d_SwapTabs(SwapTabs), tab);
			}
			else
			{
				tabs.SelectedIndex = tab;

				if(tab == 2)
				{
					tabs.Items.Remove(tiUninstall);
					tabs.Items.Remove(tiUninstalling);
				}
			}
		}

		private void bFinish_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}
	}
}
