﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Script.Serialization;

namespace LiveInstaller
{
	class MELauncher
	{
		public string version { get; set; }
	}

	class MELauncherVersionInfo
	{
		public MELauncher melauncher { get; set; }
	}

	class Download
	{
		public string version { get; set; }
		public string download_url { get; set; }
		public string checksum { get; set; }
	}

	class InstallerData
	{
		public List<Download> downloads { get; set; }
	}

	class JsonData
	{
		public InstallerData Installer { get; private set; }
		public MELauncherVersionInfo MELauncher { get; private set; }

		public JsonData()
		{
			HttpGetInstallerInfo();
		}

		private void HttpGetInstallerInfo()
		{
			HttpWebRequest lirequest = (HttpWebRequest)WebRequest.Create("http://cdn.kalebklein.com/mel/v5/liveInstaller.json");
			HttpWebResponse liresponse = (HttpWebResponse)lirequest.GetResponse();

			using (StreamReader reader = new StreamReader(liresponse.GetResponseStream()))
			{
				Installer = new JavaScriptSerializer().Deserialize<InstallerData>(reader.ReadToEnd());
			}

			HttpWebRequest mvirequest = (HttpWebRequest)WebRequest.Create("http://cdn.kalebklein.com/mel/v5/updatev2.json");
			HttpWebResponse mviresponse = (HttpWebResponse)mvirequest.GetResponse();

			using (StreamReader reader = new StreamReader(mviresponse.GetResponseStream()))
			{
				MELauncher = new JavaScriptSerializer().Deserialize<MELauncherVersionInfo>(reader.ReadToEnd());
			}
		}
	}
}
