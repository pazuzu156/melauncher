﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Windows;

namespace LiveInstaller
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	///
	public partial class App : Application
	{
		private Mutex _m;

		private void Application_Startup(object sender, StartupEventArgs e)
		{
			var asm = Assembly.GetExecutingAssembly();
			bool mutexCreated;
			var mutexName = string.Format(CultureInfo.InvariantCulture, "Local\\{{{0}}}{{{1}}}",
				asm.GetType().GUID, asm.GetName().Name);

			_m = new Mutex(true, mutexName, out mutexCreated);

			if(!mutexCreated)
			{
				_m = null;
				MessageBox.Show("MELauncher Live Installer is already running!", "Error",
					MessageBoxButton.OK, MessageBoxImage.Warning);
				Current.Shutdown(1);
				return;
			}

			run();
		}

		private void Application_Exit(object sender, ExitEventArgs e)
		{
			Current.Shutdown(0);
		}

		private void run()
		{
			string keyName;
			if (Environment.Is64BitOperatingSystem)
			{
				keyName = "SOFTWARE\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\{808CB6F1-72C8-4887-8CA3-382B1B1F1236}_is1";
			}
			else
			{
				keyName = "SOFTWARE\\WOW6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\{808CB6F1-72C8-4887-8CA3-382B1B1F1236}_is1";
			}

			RegistryKey key = Registry.LocalMachine.OpenSubKey(keyName);

			//if (key.GetValue("InstallLocation") == null)
			//{
			//	// uninstall or repair
			//	new UninstallWindow(key).Show();
			//}
			//else
			//{
			//	// Install
			//	new InstallWindow().Show();
			//}
			try
			{
				key.GetValue("InstallLocation");
				new UninstallWindow(key).Show();
			}
			catch
			{
				new InstallWindow().Show();
			}
		}
	}
}
