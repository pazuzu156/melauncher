﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.IO;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Security.Cryptography;
using System.Threading;

namespace LiveInstaller
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class InstallWindow : Window
	{
		#region Variables/Properties
		private string download_url;
		private Version version;
		private string checksum;
		private string lchecksum;
		private string installer;
		Task installTask;
		Task validateTask;

		public bool ReturnToUninstallWindow { get; set; }
		#endregion

		#region Constructor
		public InstallWindow()
		{
			InitializeComponent();

			// Get download data
			StatusCheck sc = new StatusCheck("http://cdn.kalebklein.com/mel/testcl.txt");
			if(sc.IsConnected)
			{
				var data = new JsonData();
				var mv = new Version(data.MELauncher.melauncher.version);

				foreach(var dl in data.Installer.downloads)
				{
					var version = new Version(dl.version);
					if(version == mv)
					{
						this.version = version;
						this.download_url = dl.download_url;
						this.checksum = dl.checksum;
					}
				}
			}
			else
			{
				MessageBox.Show("MELauncher Live Installer requires an active internet connection. Please try again or download the full installer.", "Connection Error",
					MessageBoxButton.OK, MessageBoxImage.Error);
				App.Current.Shutdown(1);
			}

			txtAbout.Text = @"Welcome to the MELauncher Live Installer. This installer was built to install MELauncher by downloading from an external source, while minimizing the filesize of the distribution.

To get started, just click the ""Install"" button.
";
			txtVersion.Text = string.Format("MELauncher Version: {0}", this.version.ToString());

			installer = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Temp\\" + GetFileName(download_url);
		}
		#endregion

		#region Component Methods
		private void bCancel_Click(object sender, RoutedEventArgs e)
		{
			Close();
		}

		private void bInstall_Click(object sender, RoutedEventArgs e)
		{
			var res = MessageBox.Show("Once downloading has started, it cannot be stopped. If you wish to cancel during install, please do so after MELauncher has downloaded. Do you wish to continue?",
				"Confirm", MessageBoxButton.YesNo, MessageBoxImage.Question);
			if(res == MessageBoxResult.Yes)
			{
				tabs.SelectedIndex = 1;
				ReturnToUninstallWindow = false;

				if(File.Exists(installer))
				{
					pbProgress.IsIndeterminate = true;
					UpdateUI("Validating Download", "(2/4) Validating integrity of download...", "Progress: Download Complete");
					validateTask = Task.Factory.StartNew(() => { ValidateInstaller();  });
				}
				else
				{
					DownloadInstaller();
				}
			}
		}
		#endregion

		#region Event Called Methods
		private void DownloadInstaller()
		{
			WebClient client = new WebClient();
			client.DownloadProgressChanged += client_DownloadProgressChanged;
			client.DownloadFileCompleted += client_DownloadFileCompleted;
			client.DownloadFileAsync(new Uri(download_url), installer);
		}

		void client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
		{
			pbProgress.IsIndeterminate = true;
			UpdateUI("Validating Download", "(2/4) Validating integrity of download...", "Progress: Download Complete");

			validateTask = Task.Factory.StartNew(() => { ValidateInstaller(); });
		}

		void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
		{
			double bytesIn = double.Parse(e.BytesReceived.ToString());
			double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
			double percent = bytesIn / totalBytes * 100;
			SetProgressValues(int.Parse(Math.Truncate(percent).ToString()));
		}
		#endregion

		#region Threaded Methods
		private delegate void d_UpdateUI(string title, string status, string progress);
		private void UpdateUI(string title, string status, string progress)
		{
			if(!Dispatcher.CheckAccess())
			{
				Dispatcher.Invoke(new d_UpdateUI(UpdateUI), title, status, progress);
			}
			else
			{
				Title = title;
				txtStatus.Text = status;
				txtProgress.Text = progress;
			}
		}

		private delegate void d_SetProgressValues(int value);
		private void SetProgressValues(int value)
		{
			if(!Dispatcher.CheckAccess())
			{
				Dispatcher.Invoke(new d_SetProgressValues(SetProgressValues), value);
			}
			else
			{
				pbProgress.Value = value;
				txtProgress.Text = string.Format("Progress: {0}%", value);
				Title = string.Format("Downloading Installer: {0}%...", value);
			}
		}

		private delegate void d_ValidateInstaller();
		private void ValidateInstaller()
		{
			if(!Dispatcher.CheckAccess())
			{
				Dispatcher.Invoke(new d_ValidateInstaller(ValidateInstaller));
			}
			else
			{
				using(var md5 = MD5.Create())
				{
					using(var stream = File.OpenRead(installer))
					{
						lchecksum = BitConverter.ToString(md5.ComputeHash(stream)).Replace("-", "").ToLower();
						stream.Close();
						if (checksum == lchecksum)
						{
							UpdateUI("Running Installer", "(3/4) Running MELauncher installer...", "Progress: Download Complete");
							installTask = Task.Factory.StartNew(() => { ProceedWithInstall(); InstallComplete(); });
						}
						else
						{
							VerificationFailed();
						}
					}
				}
			}
		}

		private delegate void d_ProceedWithInstall();
		private void ProceedWithInstall()
		{
			if (!Dispatcher.CheckAccess())
			{
				Dispatcher.Invoke(new d_ProceedWithInstall(ProceedWithInstall));
			}
			else
			{
				ProcessStartInfo psi = new ProcessStartInfo();
				psi.FileName = installer;
				Process p = Process.Start(psi);
				p.WaitForExit();

				txtStatus.Text = "(3/4) Cleaning up";
				Title = "Install Complete";
				File.Delete(installer);
				tabs.SelectedIndex = 2; // Success tab
			}
		}

		private delegate void d_InstallComplete();
		private void InstallComplete()
		{
			if(!Dispatcher.CheckAccess())
			{
				Dispatcher.Invoke(new d_InstallComplete(InstallComplete));
			}
			else
			{
				installTask = null;
				validateTask = null;
			}
		}

		private delegate void d_VerificationFailed();
		private void VerificationFailed()
		{
			if (!Dispatcher.CheckAccess())
			{
				Dispatcher.Invoke(new d_VerificationFailed(VerificationFailed));
			}
			else
			{
				installTask = null;
				validateTask = null;

				txtChecksumResults.Text = string.Format(@"Local Filename: {0}
Server Filename: {1}
Local Checksum: {2}
Server Checksum: {3}", GetFileName(installer), GetFileName(download_url), lchecksum, checksum);

				tabs.SelectedItem = tiVFail;
			}
		}
		#endregion

		#region Misc Methods
		private string GetFileName(string url)
		{
			url = url.Replace("\\", "/");
			string[] exp = url.Split('/');
			return exp[exp.Length - 1];
		}
		#endregion
	}
}
