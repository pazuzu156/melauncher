﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace MEL.Locale
{
	[Serializable]
	public class Cereal
	{
		
		public static string Serialize(string lang, object content)
		{
			if (!content.GetType().IsSerializable)
				return null;

			using (MemoryStream stream = new MemoryStream())
			{
				new BinaryFormatter().Serialize(stream, ConvertToJSON(lang, content));
				return Convert.ToBase64String(stream.ToArray());
			}
		}

		public static object Deserialize(string str)
		{
			byte[] bytes = Convert.FromBase64String(str);
			using(MemoryStream stream = new MemoryStream(bytes))
			{
				return new BinaryFormatter().Deserialize(stream);
			}
		}

		private static string ConvertToJSON(string lang, object content)
		{
			string json = new JavaScriptSerializer().Serialize(new
				{
					language = lang,
					content = content
				});

			return json;
		}
	}
}
