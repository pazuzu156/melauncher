﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEL.Locale.JSON
{
	public class Content
		: RootObject
	{
		public string Name { get; set; }
		public string Value { get; set; }
		public string Type { get; set; }
	}

	public class RootObject
	{
		public string language { get; set; }
		public List<Content> content { get; set; }
	}
}
