﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections;
using MEL.Locale.Data;
using System.Web.Script.Serialization;

namespace MEL.Locale
{
    public class Locale
    {
		public static string LocaleDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\MELauncher\locale";
		private string _langPackToLoad;
		protected JSON.RootObject LanguagePack { get; private set; }

		public static string ToBase64(string content)
		{
			//var plainTextBytes = Encoding.UTF8.GetBytes(content);
			//return System.Convert.ToBase64String(plainTextBytes);

			using(MemoryStream stream = new MemoryStream())
			{
				new BinaryFormatter().Serialize(stream, content);
				return Convert.ToBase64String(stream.ToArray());
			}
		}

		public static string FromBase64(string encoded)
		{
			var encodedBytes = System.Convert.FromBase64String(encoded);
			return Encoding.UTF8.GetString(encodedBytes);
		}

		protected Locale()
		{
			LoadLanguagePack(CultureInfo.CurrentCulture.ToString());
		}
		protected Locale(string locale)
		{
			LoadLanguagePack(locale);
		}
		private void LoadLanguagePack(string locale)
		{
			string localeFile = String.Format(@"{0}\{1}.lang", LocaleDir, locale);
			string defaultLocaleFile = LocaleDir + @"\en-US.lang";

			if (File.Exists(localeFile))
				_langPackToLoad = localeFile;
			else
			{
				if (File.Exists(defaultLocaleFile))
					_langPackToLoad = defaultLocaleFile;
				else
				{
					if (File.Exists("locale\\" + locale))
						_langPackToLoad = "locale\\" + locale;
					else
						_langPackToLoad = "locale\\en-US.lang";
				}
			}

			using(var reader = new StreamReader(File.Open(_langPackToLoad, FileMode.Open, FileAccess.Read)))
			{
				//var content = Cereal.Deserialize(reader.ReadToEnd());
				LanguagePack = new JavaScriptSerializer().Deserialize<JSON.RootObject>(reader.ReadToEnd());
			}
		}

		#region Enums
		public class Item
		{
			public string Value { get; set; }
			public string Type { get; set; }
		}
		#endregion
    }
}
