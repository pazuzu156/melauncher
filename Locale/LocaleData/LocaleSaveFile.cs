﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace MEL.Locale.Data
{
    [Serializable]
    public class LocaleSaveFile
    {
        public LocaleSaveFile()
        {
            LocaleCollection = new List<LocaleFileInfo>();
        }

        private List<LocaleFileInfo> _col;
        public List<LocaleFileInfo> LocaleCollection
        {
            get { return _col; }
            set { _col = value; }
        }

        public static LocaleSaveFile DecodeSaveFile(string localFile)
        {
            FileStream localFileStream = null;
            BinaryFormatter decoder = null;
            try
            {
                localFileStream = File.Open(localFile, FileMode.Open, FileAccess.Read);
                decoder = new BinaryFormatter();
                return (LocaleSaveFile)decoder.Deserialize(localFileStream);
            }
            catch(Exception ex)
            {
                throw new Exception("The locale file is corrupt!", ex);
            }
            finally
            {
                if (localFileStream != null)
                    localFileStream.Dispose();
            }
        }
    }
}
