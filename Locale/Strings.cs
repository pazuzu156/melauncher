﻿using MEL.Locale.Data;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MEL.Locale
{
    public class Strings : Locale
    {
		private Dictionary<string, Item> _strings;

		public static Strings Load()
		{
			return new Strings();
		}

		public static Strings Load(string locale)
		{
			return new Strings(locale);
		}

		// Default locale constructor
		private Strings() : base()
		{
			ProcessJSON();
		}

		private Strings(string locale)
			: base(locale)
		{
			ProcessJSON();
		}

		private void ProcessJSON()
		{
			_strings = new Dictionary<string, Item>();
			foreach (var item in LanguagePack.content)
			{
				_strings.Add(item.Name, new Item() { Value = item.Value, Type = item.Type });
			}
		}

		public string GetString(string title)
		{
			try
			{
				return _strings[title.ToLower()].Value;
			}
			catch
			{
				return "";
			}
		}

		public Dictionary<string, Item> GetAllStrings()
		{
			var titles = new Dictionary<string, Item>();
			foreach (var item in _strings)
			{
				if (item.Value.Type.ToLower() == "strings")
				{
					titles.Add(item.Key, new Item() { Value = item.Value.Value, Type = item.Value.Type });
				}
			}

			return titles;
		}
    }
}
