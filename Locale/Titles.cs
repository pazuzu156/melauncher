﻿using MEL.Locale.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.Windows.Forms;

using MEL.Locale.JSON;

namespace MEL.Locale
{
	public class Titles : Locale
	{
		private Dictionary<string, Item> _titles;

		public static Titles Load()
		{
			return new Titles();
		}

		public static Titles Load(string locale)
		{
			return new Titles(locale);
		}

		// Default locale constructor
		private Titles()
			: base()
		{
			ProcessJSON();
		}

		private Titles(string locale)
			: base(locale)
		{
			ProcessJSON();
		}

		private void ProcessJSON()
		{
			_titles = new Dictionary<string, Item>();
			foreach (var item in LanguagePack.content)
			{
				_titles.Add(item.Name, new Item() { Value = item.Value, Type = item.Type });
			}
		}

		public string GetTitle(string title)
		{
			try
			{
				return _titles[title.ToLower()].Value;
			}
			catch
			{
				return "";
			}
		}

		public Dictionary<string, Item> GetAllTitles()
		{
			var titles = new Dictionary<string, Item>();
			foreach (var item in _titles)
			{
				if (item.Value.Type.ToLower() == "titles")
				{
					titles.Add(item.Key, new Item() { Value = item.Value.Value, Type = item.Value.Type });
				}
			}

			return titles;
		}
	}
}
