﻿using MEL.Locale.Data;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MEL.Locale
{
    public class RadioButtons : Locale
    {
		private Dictionary<string, Item> _buttons;

		public static RadioButtons Load()
		{
			return new RadioButtons();
		}

		public static RadioButtons Load(string locale)
		{
			return new RadioButtons(locale);
		}

		// Default locale constructor
		private RadioButtons() : base()
		{
			ProcessJSON();
		}

		private RadioButtons(string locale)
			: base(locale)
		{
			ProcessJSON();
		}

		private void ProcessJSON()
		{
			_buttons = new Dictionary<string, Item>();
			foreach (var item in LanguagePack.content)
			{
				_buttons.Add(item.Name, new Item() { Value = item.Value, Type = item.Type });
			}
		}

		public string GetButton(string title)
		{
			try
			{
				return _buttons[title.ToLower()].Value;
			}
			catch
			{
				return "";
			}
		}

		public Dictionary<string, Item> GetAllButtons()
		{
			var titles = new Dictionary<string, Item>();
			foreach (var item in _buttons)
			{
				if (item.Value.Type.ToLower() == "radio buttons")
				{
					titles.Add(item.Key, new Item() { Value = item.Value.Value, Type = item.Value.Type });
				}
			}

			return titles;
		}
    }
}
