﻿using MEL.Locale.Data;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MEL.Locale
{
    public class CheckBoxes : Locale
    {
		private Dictionary<string, Item> _buttons;

		public static CheckBoxes Load()
		{
			return new CheckBoxes();
		}

		public static CheckBoxes Load(string locale)
		{
			return new CheckBoxes(locale);
		}

		// Default locale constructor
		private CheckBoxes() : base()
		{
			ProcessJSON();
		}

		private CheckBoxes(string locale)
			: base(locale)
		{
			ProcessJSON();
		}

		private void ProcessJSON()
		{
			_buttons = new Dictionary<string, Item>();
			foreach (var item in LanguagePack.content)
			{
				_buttons.Add(item.Name, new Item() { Value = item.Value, Type = item.Type });
			}
		}

		public string GetButton(string title)
		{
			try
			{
				return _buttons[title.ToLower()].Value;
			}
			catch
			{
				return "";
			}
		}

		public Dictionary<string, Item> GetAllButtons()
		{
			var titles = new Dictionary<string, Item>();
			foreach (var item in _buttons)
			{
				if (item.Value.Type.ToLower() == "check boxes")
				{
					titles.Add(item.Key, new Item() { Value = item.Value.Value, Type = item.Value.Type });
				}
			}

			return titles;
		}
    }
}
