﻿using MEL.Locale.Data;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MEL.Locale
{
    public class Buttons : Locale
    {
		private Dictionary<string, Item> _buttons;

		public static Buttons Load()
		{
			return new Buttons();
		}

		public static Buttons Load(string locale)
		{
			return new Buttons(locale);
		}

		// Default locale constructor
		private Buttons() : base()
		{
			ProcessJSON();
		}

		private Buttons(string locale)
			: base(locale)
		{
			ProcessJSON();
		}

		private void ProcessJSON()
		{
			_buttons = new Dictionary<string, Item>();
			foreach (var item in LanguagePack.content)
			{
				_buttons.Add(item.Name, new Item() { Value = item.Value, Type = item.Type });
			}
		}

		public string GetButton(string title)
		{
			try
			{
				return _buttons[title.ToLower()].Value;
			}
			catch
			{
				return "";
			}
		}

		public Dictionary<string, Item> GetAllButtons()
		{
			var titles = new Dictionary<string, Item>();
			foreach (var item in _buttons)
			{
				if (item.Value.Type.ToLower() == "buttons")
				{
					titles.Add(item.Key, new Item() { Value = item.Value.Value, Type = item.Value.Type });
				}
			}

			return titles;
		}
    }
}
