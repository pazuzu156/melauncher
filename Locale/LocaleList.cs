﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEL.Locale
{
    public class LocaleList
    {
        private static StringDictionary _languages;

        public static StringDictionary GetLocales()
        {
            PopulateDictionary();
            return _languages;
        }

        private static void PopulateDictionary()
        {
            _languages = new StringDictionary();
            foreach(CultureInfo ci in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                _languages.Add(ci.Name, ci.DisplayName);
            }
        }
    }
}
