#define Version GetFileVersion('bin\MELauncher.exe')

#define AppName "MELauncher"
#define TransName "Translation Tool"
#define Publisher "Kaleb Klein"
#define AppURL "http://www.nexusmods.com/masseffect3/mods/320/?"
#define AuthorURL "http://www.kalebklein.com"
#define SetupName "mel_setup"
#define VCLStylesSkinPath "{localappdata}\VCLStylesSkin"

#include "setup_code.iss"

[Setup]
AppId={{808CB6F1-72C8-4887-8CA3-382B1B1F1236}
AppName={#AppName}
AppVersion={#Version}
AppVerName={#AppName} {#Version}
VersionInfoProductVersion={#Version}
VersionInfoProductTextVersion={#Version}
VersionInfoCopyright=Copyright � 2016
AppPublisher={#Publisher}
AppPublisherURL={#AuthorURL}
AppSupportURL={#AppURL}
AppUpdatesURL={#AppURL}
DefaultDirName={pf}\{#AppName}
DefaultGroupName={#AppName}
OutputBaseFilename={#SetupName}-{#Version}-{#Build}
OutputDir={#Build}
SetupIconFile=mel.ico
WizardSmallImageFile=mel.bmp
Compression=lzma
SolidCompression=yes
UninstallDisplayName=MELauncher
UninstallDisplayIcon={app}\{#AppName}.exe
ShowTasksTreeLines=True
PrivilegesRequired=admin
WizardImageFile="C:\Program Files (x86)\The Road To Delphi\VCL Styles Inno\Images\WizModernImage-IS_Orange.bmp"
LicenseFile=license.rtf
DisableWelcomePage=no           

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Types]
Name: "default"; Description: "Default Installation"
Name: "translator"; Description: "Default installation with the Translation tool"; Flags: iscustom

[Components]
Name: "app"; Description: "Default installation"; Types: default translator; Flags: fixed
Name: "trans"; Description: "Default installation with the Translation tool"; Types: translator

[CustomMessages]
Nexus=%1 on NexusMods
Website=Visit My Website
DesktopIcon=Create Desktop Icon for %1
License=View MELauncher License Agreement

[Tasks]
Name: "desktopicon"; Description: "{cm:DesktopIcon,{#AppName}}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked
Name: "translatoricon"; Description: "{cm:DesktopIcon,{#TransName}}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked; Components: trans

[InstallDelete]
Type: dirifempty; Name: "{app}\locale"

[Files]
;========
; MELauncher Core
;========
Source: "bin\{#AppName}.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: app
Source: "mel_help\MELauncherHelp.chm"; DestDir: "{app}"; Flags: ignoreversion
Source: "bin\MahApps.Metro.dll"; DestDir: "{app}"; Flags: ignoreversion; Components: app
Source: "bin\MaterialDesignColors.dll"; DestDir: "{app}"; Flags: ignoreversion; Components: app
Source: "bin\MaterialDesignThemes.Wpf.dll"; DestDir: "{app}"; Flags: ignoreversion; Components: app
Source: "bin\System.Windows.Interactivity.dll"; DestDir: "{app}"; Flags: ignoreversion; Components: app
Source: "bin\MEL.Locale.dll"; DestDir: "{app}"; Flags: ignoreversion; Components: app
Source: "bin\MEL.Settings.dll"; DestDir: "{app}"; Flags: ignoreversion; Components: app
Source: "bin\MEL.Logger.dll"; DestDir: "{app}"; Flags: ignoreversion; Components: app
Source: "bin\MELauncher.exe.config"; DestDir: "{app}"; Flags: ignoreversion; Components: app
Source: "license.rtf"; DestDir: "{app}"; Flags: ignoreversion; Components: app
Source: "redist\dotNetFx45_Full_setup.exe"; DestDir: "{app}\redist"; Flags: ignoreversion deleteafterinstall; Components: app; Check: not IsReqDotNetDetected

;========
; Updater
;========
Source: "bin\Updater.exe"; DestDir: "{userappdata}\MELauncher"; Flags: ignoreversion recursesubdirs createallsubdirs; Components: app

;========
; Translator
;========
Source: "bin\Translator.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: trans

;========
; Languages
;========
Source: "Languages\locale\*.lang"; DestDir: "{userappdata}\MELauncher\locale"; Flags: ignoreversion; Components: app

;========
; Styles
;========
Source: "C:\Program Files (x86)\The Road To Delphi\VCL Styles Inno\VclStylesinno.dll"; DestDir: {#VCLStylesSkinPath};
Source: "C:\Program Files (x86)\The Road To Delphi\VCL Styles Inno\Styles\Amakrits.vsf"; DestDir: {#VCLStylesSkinPath};

[Icons]
Name: "{group}\{#AppName}"; Filename: "{app}\{#AppName}.exe"
Name: "{group}\{cm:Nexus,{#AppName}}"; Filename: "{#AppURL}"
Name: "{group}\{cm:Website}"; Filename: "{#AuthorURL}"
Name: "{group}\{cm:License}"; Filename: "{app}\license.rtf"
Name: "{app}\{cm:Nexus,{#AppName}}"; Filename: "{#AppURL}"
Name: "{app}\{cm:Website}"; Filename: "{#AuthorURL}"
Name: "{group}\MELauncherHelp.chm"; Filename: "{app}\MELauncherHelp.chm"
Name: "{group}\{cm:UninstallProgram,{#AppName}}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#AppName}"; Filename: "{app}\{#AppName}.exe"; Tasks: desktopicon
Name: "{commondesktop}\{#TransName}"; Filename: "{app}\Translator.exe"; Tasks: translatoricon

[Run]
Filename: "{app}\{#AppName}"; Description: "{cm:LaunchProgram,{#StringChange(AppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent shellexec unchecked
Filename: "{app}\redist\dotNetFx45_Full_setup.exe"; Parameters: "/passive /norestart"; Check: not IsReqDotNetDetected; StatusMsg: "Installing .NET 4.5. Please wait..."

[UninstallRun]
Filename: "{app}\MELauncher.exe"; Parameters: "/uninstall"

[UninstallDelete]
Type: files; Name: "{userappdata}\MELauncher\Settings\MELauncherSettings.xml";
Type: files; Name: "{userappdata}\MELauncher\Settings\MELauncherSettings.json";
Type: files; Name: "{userappdata}\MELauncher\Settings\LoggerSettings.xml";
Type: files; Name: "{userappdata}\MELauncher\MELauncherChangelog.log";
Type: files; Name: "{userappdata}\MELauncher\MELauncherChangelog.html";
Type: files; Name: "{userappdata}\MELauncher\MELauncherCrashHandler.lock";
Type: files; Name: "{userappdata}\MELauncher\locale\*.lang";
Type: files; Name: "{userappdata}\MELauncher\logs\*.log";
Type: files; Name: "{#VCLStylesSkinPath}\*";
Type: dirifempty; Name: "{userappdata}\MELauncher\logs";
Type: dirifempty; Name: "{userappdata}\MELauncher\Settings";
Type: dirifempty; Name: "{userappdata}\{#VCLStylesSkinPath}";

[Registry]
Root: "HKCR"; Subkey: "MELauncher"; Flags: uninsdeletekey
Root: "HKCR"; Subkey: "MELauncher\DefaultIcon"; ValueType: string; ValueData: "URL:melauncher Protocol"
Root: "HKCR"; Subkey: "MELauncher\DefaultIcon"; ValueType: string; ValueName: "URL Protocol"
Root: "HKCR"; Subkey: "MELauncher\shell\open\command"
