﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MEL.Settings;
using System.IO;
using WinForms = System.Windows.Forms;

namespace tmCleanTool
{
    public class Cleaner
    {
        Settings s;
        Settings.DRMType drm;

        public Cleaner()
        {
            s = Settings.getInstance();
            s.LoadSettings();

            this.drm = Settings.DRM;
        }

        public void clean()
        {
            if (File.Exists(Settings.MELocation + @"\Binaries\TexMod.exe"))
                File.Delete(Settings.MELocation + @"\Binaries\TexMod.exe");

            if (File.Exists(Settings.ME2Location + @"\Binaries\TexMod.exe"))
                File.Delete(Settings.ME2Location + @"\Binaries\TexMod.exe");
            
            if(this.drm == Settings.DRMType.Origin)
                if (File.Exists(Settings.ME3Location + @"\Binaries\Win32\TexMod.exe"))
                    File.Delete(Settings.ME3Location + @"\Binaries\Win32\TexMod.exe");

            WinForms.MessageBox.Show("TexMod has been cleaned from your system. Now just redownload TexMod through MELauncher and you're golden");
        }
    }
}
