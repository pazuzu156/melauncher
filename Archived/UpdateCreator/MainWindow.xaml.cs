﻿using MEL.UpdateData;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UpdateCreator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void bAdd_Click(object sender, RoutedEventArgs e)
        {
            if (tbName.Text.Equals("") || tbDesc.Text.Equals(""))
            {
                MessageBox.Show("Please fill out the name and description!");
            }
            else
            {
                itemList.Items.Add(new ListViewData(tbName.Text, tbDesc.Text));
                tbName.Text = "";
                tbDesc.Text = "";
            }
        }

        private void bRemove_Click(object sender, RoutedEventArgs e)
        {
            int selectedIndex = itemList.SelectedIndex;
            itemList.Items.Remove(itemList.SelectedItem);
        }

        private void bGenerate_Click(object sender, RoutedEventArgs e)
        {
            if (tbVersion.Text.Equals("") || itemList.Items.Count == 0)
            {
                MessageBox.Show("Please fill out entire form!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                
                UpdateSaveFile file = new UpdateSaveFile(tbVersion.Text);
                foreach (var item in itemList.Items)
                {
                    ListViewData lvd = (ListViewData)item;

                    file.UpdateFileCollection.Add(new UpdateFileInfo(lvd.Name, lvd.Description));
                    BinaryFormatter bf = new BinaryFormatter();
                    FileStream bfStream = File.Open(@".\UpdateInfo.dat", FileMode.Create, FileAccess.Write);
                    bf.Serialize(bfStream, file);
                    bfStream.Close();
                }
                MessageBox.Show("Generation Complete!");
            }
        }

        private void bLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "UpdateInfo.dat|UpdateInfo.dat";
            if (ofd.ShowDialog() == true)
            {
                if (File.Exists(ofd.FileName))
                {
                    UpdateSaveFile file = UpdateSaveFile.DecodeSaveFile("UpdateInfo.dat");
                    foreach (UpdateFileInfo fileInfo in file.UpdateFileCollection)
                    {
                        itemList.Items.Add(new ListViewData(fileInfo.Name, fileInfo.Description));
                    }
                    tbVersion.Text = file.VersionString;
                }
            }
        }

        public class ListViewData
        {
            public ListViewData() { }

            public ListViewData(string name, string desc)
            {
                Name = name;
                Description = desc;
            }

            public string Name { get; set; }
            public string Description { get; set; }
        }
    }
}
