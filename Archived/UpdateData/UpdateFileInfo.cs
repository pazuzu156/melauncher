﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEL.UpdateData
{
    [Serializable]
    public class UpdateFileInfo
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public UpdateFileInfo(string name, string desc)
        {
            Name = name;
            Description = desc;
        }
    }
}
