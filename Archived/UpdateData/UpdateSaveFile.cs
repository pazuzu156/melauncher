﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace MEL.UpdateData
{
    [Serializable]
    public class UpdateSaveFile
    {
        public UpdateSaveFile(string version)
        {
            VersionString = version;
            UpdateFileCollection = new List<UpdateFileInfo>();
        }

        public string VersionString { get; set; }
        private List<UpdateFileInfo> _coll;

        public List<UpdateFileInfo> UpdateFileCollection
        {
            get { return _coll; }
            set { _coll = value; }
        }

        public static UpdateSaveFile DecodeSaveFile(string LocalUpdateFile)
        {
            FileStream localFileStream = null;
            BinaryFormatter decoder = null;
            try
            {
                localFileStream = File.Open(LocalUpdateFile, FileMode.Open, FileAccess.Read);
                decoder = new BinaryFormatter();
                return (UpdateSaveFile)decoder.Deserialize(localFileStream);
            }
            catch(Exception ex)
            {
                throw new Exception("The local file is corrupt!", ex);
            }
            finally
            {
                if (localFileStream != null)
                    localFileStream.Dispose();
            }
        }
    }
}
