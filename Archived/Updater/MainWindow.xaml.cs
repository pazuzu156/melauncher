﻿using HttpStatusCheck;
using MEL.UpdateData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.ComponentModel;
using System.IO;
using MEL.Settings;
using MEL.Locale;
using System.Diagnostics;

namespace Updater
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string baseUrl = "http://cdn.kalebklein.com/mel";
        string updateDatFileUrl;
        string Version;
        int index = 0;
        Dictionary<string, string> arguments = new Dictionary<string, string>();
        Strings strings;
        Buttons buttons;
        Titles titles;
        Settings settings;

        public MainWindow()
        {
            InitializeComponent();

            updateDatFileUrl = baseUrl + "/UpdateInfo.dat";

            string[] args = Environment.GetCommandLineArgs();

            settings = Settings.getInstance();
            settings.LoadSettings();

            strings = Strings.Load(Settings.Language);
            buttons = Buttons.Load(Settings.Language);
            titles = Titles.Load(Settings.Language);

            UpdateSaveFile file = UpdateSaveFile.DecodeSaveFile("UpdateInfo.dat");
            foreach(UpdateFileInfo fileInfo in file.UpdateFileCollection)
            {
                itemList.Items.Add(new ListViewData(fileInfo.Name, "Waiting...", fileInfo.Description));
            }
            pbInstall.Maximum = (file.UpdateFileCollection.Count);
            Version = file.VersionString;

            SetLocale();
        }

        private void SetLocale()
        {
            this.Title = titles.GetTitle("Updater/MainWindow");
            lblStatus.Text = strings.GetString("waitingStatus");

            itemListGrid.Columns.Add(new GridViewColumn
            {
                DisplayMemberBinding = new Binding("Name"),
                Header = strings.GetString("grid_Name"),
                Width = 130
            });
            itemListGrid.Columns.Add(new GridViewColumn
            {
                DisplayMemberBinding = new Binding("Status"),
                Header = strings.GetString("grid_Status"),
                Width = 130
            });
            itemListGrid.Columns.Add(new GridViewColumn
            {
                DisplayMemberBinding = new Binding("Description"),
                Header = strings.GetString("grid_Description"),
                Width = 230
            });

            gbDetails.Header = strings.GetString("dlDetails");
            lblDLProgress.Text = strings.GetString("dlProgress");
            lblInsProgress.Text = strings.GetString("insProgress");

            bInstall.Content = buttons.GetButton("installU");
            bCancel.Content = buttons.GetButton("cancelU");
        }

        private void bInstall_Click(object sender, RoutedEventArgs e)
        {
            if (bInstall.Content.Equals("Finish"))
            {
                ProcessStartInfo pi = new ProcessStartInfo();
                pi.FileName = Environment.CurrentDirectory + @"\MELauncher.exe";
                pi.Arguments = "/CleanUpdate";
                Process.Start(pi);
                if(File.Exists(@".\UpdateInfo.dat"))
                    File.Delete(@".\UpdateInfo.dat");
                this.Close();
            }
            else
            {
                bInstall.IsEnabled = false;
                bCancel.IsEnabled = false;
                DownloadFile();
            }
        }

        private void DownloadFile()
        {
            WebClient client = new WebClient();
            client.DownloadProgressChanged += new DownloadProgressChangedEventHandler(client_DownloadProcessChanged);
            client.DownloadFileCompleted += new AsyncCompletedEventHandler(client_DownloadFileCompleted);

            ListViewData lvd = (ListViewData)itemList.Items[index];
            string name = lvd.Name;

            SetStatus(String.Format("Downloading {0}...", name));
            lvd.Status = "Downloading...";

            string local = name;
            string online = String.Format(@"{0}/{1}", baseUrl, name);
            string temp = System.IO.Path.GetTempPath();

            if (!Directory.Exists(temp + @"\locale"))
                Directory.CreateDirectory(temp + @"\locale");

            if (File.Exists(local))
            {
                //if (File.Exists(local + ".old"))
                //    File.Delete(local + ".old");
                File.Move(local, local + ".old");
            }

            client.DownloadFileAsync(new Uri(online), temp + name);
        }

        private void client_DownloadFileCompleted(object sender, AsyncCompletedEventArgs e)
        {
            ListViewData lvd = (ListViewData)itemList.Items[index];
            lvd.Status = "Updated";
            itemList.Items.Refresh();

            index += 1;

            pbInstall.Value += index;
            pbDownload.Value = 0;

            if (itemList.Items.Count - 1 >= index)
                DownloadFile();
            else
                MoveFiles();
        }

        private void client_DownloadProcessChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            pbDownload.Value = e.ProgressPercentage;
        }

        private void SetStatus(string status)
        {
            lblStatus.Text = String.Format("Status: {0}", status);
        }

        private void MoveFiles()
        {
            SetStatus("Finishing update...");
            for(int i = 0; i < itemList.Items.Count; i++)
            {
                ListViewData lvd = (ListViewData)itemList.Items[i];
                File.Move(System.IO.Path.GetTempPath() + lvd.Name, lvd.Name);
            }
            Directory.Delete(System.IO.Path.GetTempPath() + @"\locale");
            UpdateLocalVersion();
        }

        private void UpdateLocalVersion()
        {
            Settings s = Settings.getInstance();
            s.LoadSettings();
            s.SaveSettings(
                Settings.AppID,
                Version,
                Settings.SteamLocation,
                Settings.OriginLocation,
                Settings.MELocation,
                Settings.ME2Location,
                Settings.ME3Location,
                Settings.DRM,
                Settings.Theme,
                Settings.Language,
                Settings.CloseOnRun,
                Settings.AOT,
                Settings.StartUp
            );
            Complete();
        }

        private void Complete()
        {
            SetStatus("Finished");
            bInstall.Content = "Finish";
            bInstall.IsEnabled = true;
        }

        public class ListViewData
        {
            public string Name { get; set; }
            public string Status { get; set; }
            public string Description { get; set; }

            public ListViewData(string name, string status, string desc)
            {
                Name = name;
                Status = status;
                Description = desc;
            }
        }

        private void bCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
