﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MEL.Settings;
using MEL.Locale;
using System.Net;
using System.IO;

namespace Updater
{
    /// <summary>
    /// Interaction logic for NewUpdates.xaml
    /// </summary>
    public partial class NewUpdates : Window
    {
        Settings settings;
        Titles titles;
        Buttons buttons;

        public NewUpdates()
        {
            InitializeComponent();
            //Process[] p = Process.GetProcessesByName("MELauncher");
            //if (p.Length > 0)
            //    p[0].Kill();

            if (GetUpdateInfo.NotificationPingedForUpdate)
                Application.Current.Shutdown(500);

            settings = Settings.getInstance();
            settings.LoadSettings();

            SetLocale();
            LoadChanges();
        }

        private void SetLocale()
        {
            titles = Titles.Load(Settings.Language);
            buttons = Buttons.Load(Settings.Language);

            this.Title = titles.GetTitle("updater/newupdates");
            bUpdate.Content = buttons.GetButton("update");
            bCancel.Content = buttons.GetButton("dont_update");
        }

        private void LoadChanges()
        {
            WebRequest req = WebRequest.Create("http://cdn.kalebklein.com/mel/newupdates.txt");
            WebResponse res = req.GetResponse();
            using (StreamReader reader = new StreamReader(res.GetResponseStream()))
            {
                string all = reader.ReadToEnd();
                tbUpdates.Text = all;
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            //CancelUpdate();
        }

        private void CancelUpdate()
        {
            ProcessStartInfo psi = new ProcessStartInfo();
            psi.FileName = Environment.CurrentDirectory + @"\MELauncher.exe";
            psi.Arguments = "/CancelUpdate";
            Process.Start(psi);
            Process p = Process.GetCurrentProcess();
            p.Kill();
        }

        private void bCancel_Click(object sender, RoutedEventArgs e)
        {
            CancelUpdate();
        }

        private void bUpdate_Click(object sender, RoutedEventArgs e)
        {
            new MainWindow().Show();
            this.Close();
        }
    }
}
