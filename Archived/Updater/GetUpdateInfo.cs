﻿using MEL.Settings;
using MEL.UpdateData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MEL.Locale;

namespace Updater
{
    public class GetUpdateInfo
    {
        public Uri UpdateUrl = new Uri("http://cdn.kalebklein.com/mel/UpdateInfo.dat");
        private const String LocalUpdateFile = "UpdateInfo.dat";

        public static bool NotificationPingedForUpdate { get; private set; }

        Strings strings;

        private string GetLocalVersion()
        {
            Settings s = Settings.getInstance();
            s.LoadSettings();
            return Settings.Version;
        }

        public void CheckForUpdates()
        {
            CheckForUpdates(false, false);
        }

        public void CheckForUpdates(bool FromMainWindow, bool FromNotificationService)
        {
            strings = Strings.Load(Settings.Language);
            try
            {
                WebClient client = new WebClient();
                client.DownloadFile(UpdateUrl, LocalUpdateFile);
                client.Dispose();

                if (!File.Exists(LocalUpdateFile))
                    throw new FileNotFoundException(strings.GetString("updateFileNotFound"), LocalUpdateFile);

                UpdateSaveFile localFile = UpdateSaveFile.DecodeSaveFile(LocalUpdateFile);
                Version localVersion = Version.Parse(GetLocalVersion());
                Version onlineVersion = Version.Parse(localFile.VersionString);

                if (onlineVersion > localVersion)
                {
                    Process[] p = Process.GetProcessesByName("MELauncher");
                    if (p.Length > 0)
                        p[0].Kill();
                }
                else
                {
                    if(FromMainWindow)
                    {
                        Process pr = Process.GetCurrentProcess();
                        pr.Kill();
                    }
                    else if(FromNotificationService)
                    {
                        NotificationPingedForUpdate = true;
                    }
                    else
                    {
                        if (MessageBox.Show(strings.GetString("upToDate"), strings.GetString("upd_mb_title")) == MessageBoxResult.OK)
                        {
                            Process pr = Process.GetCurrentProcess();
                            pr.Kill();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex);
            }
        }
    }
}
