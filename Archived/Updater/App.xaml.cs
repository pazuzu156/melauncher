﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows;

namespace Updater
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            string[] args = Environment.GetCommandLineArgs();
            if (args.Length > 1)
            {
                if (args[1].Equals("/FromMainWindow"))
                {
                    start(true, false);
                }
                else if (args[1].Equals("/FromNotificationService"))
                {
                    start(false, true);
                }
                else
                {
                    start(false, false);
                }
            }
            else
            {
                start(false, false);
            }

            AppDomain.CurrentDomain.AssemblyResolve += (sender, argss) =>
            {
                string resourceName = "Updater" + "." +
                    new AssemblyName(argss.Name).Name + ".dll";

                using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourceName))
                {
                    Byte[] assemblyData = new Byte[stream.Length];
                    stream.Read(assemblyData, 0, assemblyData.Length);
                    return Assembly.Load(assemblyData);
                }
            };
        }

        private void start(bool fmw, bool fns)
        {
            HttpStatusCheck.StatusCheck sc = new HttpStatusCheck.StatusCheck("http://cdn.kalebklein.com/mel/UpdateInfo.dat");
            if(sc.IsConnected)
            {
                GetUpdateInfo gui = new GetUpdateInfo();
                gui.CheckForUpdates(fmw, fns);
            }
        }
    }
}
