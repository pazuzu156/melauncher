﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Updater
{
	/// <summary>
	/// Interaction logic for App.xaml
	/// </summary>
	public partial class App : Application
	{
		public static string MELLocation { get; private set; }
		public App()
		{
			string[] args = Environment.GetCommandLineArgs();
			if(args.Length > 0)
			{
				MELLocation = args[1];
				//MELLocation = @"C:\Users\kklei\Desktop\MELauncher\MELauncher\bin\Debug";
			}
		}
	}
}
