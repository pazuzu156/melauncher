﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Compression;
using System.Diagnostics;
using System.IO;
using System.Web.Script.Serialization;

namespace Updater
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private string ZipLocation = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\MELauncher\update.zip";
		private string ZipLocation2 = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\MELauncher\updateWithTranslate.zip";
		private string LZipLocation = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\MELauncher\locale.zip";
		//private Dictionary<string, string> json;
		private JsonData json;

		public MainWindow()
		{
			InitializeComponent();
			StatusCheck sc = new StatusCheck("http://cdn.kalebklein.com/mel/testcl.txt");
			if(sc.IsConnected)
			{
#if DEBUG
				json = JsonData.Load("http://cdn.kalebklein.com/mel/v5/debug/updatev2.json");
#else
				json = JsonData.Load("http://cdn.kalebklein.com/mel/v5/updatev2.json");
#endif
				BeginUpdate();
			}
		}

		private void BeginUpdate()
		{
			Process[] procs = Process.GetProcessesByName("MELauncher");
			foreach(Process p in procs)
			{
				p.Kill();
			}

			WebClient client = new WebClient();
			client.DownloadProgressChanged += client_DownloadProgressChanged;
			client.DownloadFileCompleted += client_DownloadFileCompleted;

			if (File.Exists(App.MELLocation + @"\Translator.exe"))
				client.DownloadFileAsync(new Uri(json.Data.melauncher.packages[1]), ZipLocation2);
			else
				client.DownloadFileAsync(new Uri(json.Data.melauncher.packages[0]), ZipLocation);
		}

		void client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
		{
			lblAction.Text = "Extracting package...";
			pbProgress.Value = 0;
			pbProgress.IsIndeterminate = true;

			try
			{
				using (ZipArchive archive = ZipFile.OpenRead((File.Exists(ZipLocation)) ? ZipLocation : ZipLocation2))
				{
					foreach (ZipArchiveEntry entry in archive.Entries)
					{
						entry.ExtractToFile(System.IO.Path.Combine(App.MELLocation, entry.FullName), true);
					}
				}
			}
			catch(Exception ex)
			{
				MessageBox.Show(ex.Message);
			}

			WebClient client = new WebClient();
			client.DownloadFileCompleted += client_DownloadFileCompleted2;
			client.DownloadFileAsync(new Uri(json.Data.locale.packages[0]), LZipLocation);
		}

		void client_DownloadFileCompleted2(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
		{
			using(ZipArchive archive = ZipFile.OpenRead(LZipLocation))
			{
				foreach(ZipArchiveEntry entry in archive.Entries)
				{
					entry.ExtractToFile(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @".\MELauncher\locale", entry.FullName), true);
				}
			}

			File.Delete(LZipLocation);
			if(File.Exists(ZipLocation))
				File.Delete(ZipLocation);
			else
				File.Delete(ZipLocation2);

			ProcessStartInfo psi = new ProcessStartInfo();
			psi.Arguments = "/Updated";
			psi.FileName = App.MELLocation + @".\MELauncher.exe";
			Process.Start(psi);
			this.Close();
		}

		void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
		{
			double bytesIn = double.Parse(e.BytesReceived.ToString());
			double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
			double percent = bytesIn / totalBytes * 100;
			pbProgress.Value = int.Parse(Math.Truncate(percent).ToString());
		}
	}
}
