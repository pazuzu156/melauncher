[Code]
// Import the LoadVCLStyle function from VclStylesInno.DLL
procedure LoadVCLStyle(VClStyleFile: String); external 'LoadVCLStyleW@files:VclStylesInno.dll stdcall setuponly';
procedure LoadVCLStyle_UnInstall(VClStyleFile: String); external 'LoadVCLStyleW@{#VCLStylesSkinPath}\VclStylesInno.dll stdcall uninstallonly';
// Import the UnLoadVCLStyles function from VclStylesInno.DLL
procedure UnLoadVCLStyles; external 'UnLoadVCLStyles@files:VclStylesInno.dll stdcall setuponly';
procedure UnLoadVCLStyles_UnInstall; external 'UnLoadVCLStyles@{#VCLStylesSkinPath}\VclStylesInno.dll stdcall uninstallonly';

const
  NET_FW_SCOPE_ALL = 0;
  NET_FW_IP_VERSION_ANY = 2;

procedure SetFirewallException(AppName,FileName:string);
var
  FirewallObject: Variant;
  FirewallManager: Variant;
  FirewallProfile: Variant;
begin
  try
    FirewallObject := CreateOleObject('HNetCfg.FwAuthorizedApplication');
    FirewallObject.ProcessImageFileName := FileName;
    FirewallObject.Name := AppName
    FirewallObject.Scope := NET_FW_SCOPE_ALL;
    FirewallObject.IpVersion := NET_FW_IP_VERSION_ANY;
    FirewallObject.Enabled := True;
    FirewallManager := CreateOleObject('HNetCfg.FwMgr');
    FireWallProfile := FirewallManager.LocalPolicy.CurrentProfile;
    FirewallProfile.AuthorizedApplications.Add(FirewallObject);
  except
  end;
end;

procedure RemoveFirewallException(FileName:string);
var
  FirewallManager: Variant;
  FirewallProfile: Variant;
begin
  try
    FirewallManager := CreateOleObject('HNetCfg.FwMgr');
    FirewallProfile := FirewallManager.LocalPolicy.CurrentProfile;
    FirewallProfile.AuthorizedApplications.Remove(FileName);
  except
  end;
end;

procedure CurStepChanged(CurStep:TSetupStep);
begin
  if CurStep=ssPostInstall then begin
    SetFirewallException('MELauncher', ExpandConstant('{app}\')+'MELauncher.exe');
    SetFirewallException('MELauncher Updater', ExpandConstant('{userappdata}\')+'MELauncher\Updater.exe');
  end;
end;

procedure CurUninstallStepChanged(CurUninstallStep:TUninstallStep);
begin
  if CurUninstallStep=usPostUninstall then begin
    RemoveFirewallException(ExpandConstant('{app}\')+'MELauncher.exe');
    RemoveFirewallException(ExpandConstant('{userappdata}\')+'MELauncher\Updater.exe');
  end;
end;

function IsDotNetDetected(version: string; service: cardinal): boolean;

var
  key: string;
  install, release, serviceCount: cardinal;
  check45, success: boolean;
//var reqNetVer: string;
begin
   // .NET 4.5 installes as update to 4.0
   if version = 'v4.5' then begin
     version := 'v4\Full';
     check45 := true;
   end else
     check45 := false;

   // Key group for .NET
   key := 'SOFTWARE\Microsoft\NET Framework Setup\NDP\' + version;

   // .NET 3.0
   if Pos('v3.0', version) = 1 then begin
     success := RegQueryDWordValue(HKLM, key + '\Setup', 'InstallSuccess', install);
   end else begin
     success := RegQueryDWordValue(HKLM, key, 'Install', install);
   end;

   // .NET 4
   if Pos('v4', version) = 1 then begin
     success := success and RegQueryDWordValue(HKLM, key, 'Servicing', serviceCount);
   end else begin
     success := success and RegQueryDWordValue(HKLM, key, 'SP', serviceCount);
   end;

   // .NET 4.5
   if check45 then begin
     success := success and RegQueryDWordValue(HKLM, key, 'Release', release);
     success := success and (release >= 378389);
   end;

   result := success and (install = 1) and (serviceCount >= service);
end;

function IsReqDotNetDetected(): Boolean;
begin
  result := IsDotNetDetected('v4\Full', 0);
end;

function InitializeSetup(): Boolean;
begin


  ExtractTemporaryFile('Amakrits.vsf');
  LoadVCLStyle(ExpandConstant('{tmp}\Amakrits.vsf'));


  if not IsDotNetDetected('v4\Full', 0) then begin
    MsgBox('{#AppName} requires Microsoft .NET Framework 4.5.'#13#13
      'The installer will attempt to install it for you.', mbInformation, MB_OK);
  end;

  result := true;
end;

procedure DeinitializeSetup();
begin
  UnLoadVCLStyles;
end;

function InitializeUninstall: Boolean;
begin
  Result := True;
  LoadVCLStyle_UnInstall(ExpandConstant('{#VCLStylesSkinPath}\Amakrits.vsf'));
end;

procedure DeinitializeUninstall();
begin
  UnLoadVCLStyles_UnInstall;
end;
