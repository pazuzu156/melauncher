﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MEL.Logger
{
    public class Commands
    {
        static Commands()
        {
			Open = new RoutedUICommand("_Open Logs Directory", "Open", typeof(Commands),
				new InputGestureCollection
				{
					new KeyGesture(Key.O, ModifierKeys.Control, "Ctrl+O")
				});

            Exit = new RoutedUICommand("E_xit", "Exit", typeof(Commands),
                new InputGestureCollection
                {
                    new KeyGesture(Key.X, ModifierKeys.Control, "Ctrl+X")
                });

            // Font Colors
            FG_Green = new RoutedUICommand("Green", "FG_Green", typeof(Commands));
            FG_Red = new RoutedUICommand("Red", "FG_Red", typeof(Commands));
            FG_Yellow = new RoutedUICommand("Yellow", "FG_Yellow", typeof(Commands));
            FG_Blue = new RoutedUICommand("Blue", "FG_Blue", typeof(Commands));
            FG_Purple = new RoutedUICommand("Purple", "FG_Purple", typeof(Commands));
            FG_White = new RoutedUICommand("White", "FG_White", typeof(Commands));
            FG_Black = new RoutedUICommand("Black", "FG_Black", typeof(Commands));
            FG_Custom = new RoutedUICommand("Custom Value", "FG_Custom", typeof(Commands));


            // Backing Colors
			BG_Default = new RoutedUICommand("Default", "BG_Default", typeof(Commands));
            BG_Green = new RoutedUICommand("Green", "BG_Green", typeof(Commands));
            BG_Red = new RoutedUICommand("Red", "BG_Red", typeof(Commands));
            BG_Yellow = new RoutedUICommand("Yellow", "BG_Yellow", typeof(Commands));
            BG_Blue = new RoutedUICommand("Blue", "BG_Blue", typeof(Commands));
            BG_Purple = new RoutedUICommand("Purple", "BG_Purple", typeof(Commands));
            BG_White = new RoutedUICommand("White", "BG_White", typeof(Commands));
            BG_Black = new RoutedUICommand("Black", "BG_Black", typeof(Commands));
            BG_Custom = new RoutedUICommand("Custom Value", "BG_Custom", typeof(Commands));
        }

		public static RoutedUICommand Open { get; private set; }
        public static RoutedUICommand Exit { get; private set; }

        // Font Colors
        public static RoutedUICommand FG_Green { get; private set; }
        public static RoutedUICommand FG_Red { get; private set; }
        public static RoutedUICommand FG_Yellow { get; private set; }
        public static RoutedUICommand FG_Blue { get; private set; }
        public static RoutedUICommand FG_Purple { get; private set; }
        public static RoutedUICommand FG_White { get; private set; }
        public static RoutedUICommand FG_Black { get; private set; }
        public static RoutedUICommand FG_Custom { get; private set; }

        // Backing Colors
		public static RoutedUICommand BG_Default { get; private set; }
        public static RoutedUICommand BG_Green { get; private set; }
        public static RoutedUICommand BG_Red { get; private set; }
        public static RoutedUICommand BG_Yellow { get; private set; }
        public static RoutedUICommand BG_Blue { get; private set; }
        public static RoutedUICommand BG_Purple { get; private set; }
        public static RoutedUICommand BG_White { get; private set; }
        public static RoutedUICommand BG_Black { get; private set; }
        public static RoutedUICommand BG_Custom { get; private set; }
    }
}
