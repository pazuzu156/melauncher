﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEL.Logger.Json
{
	public class Colors
	{
		public string foreground { get; set; }
		public string background { get; set; }
	}

	public class Settings
	{
		public Colors colors { get; set; }
	}

	public class JsonData
	{
		public Settings settings { get; set; }
	}
}
