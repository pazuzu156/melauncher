﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using SS = MEL.Settings.Settings;

namespace MEL.Logger
{
    /// <summary>
    /// Interaction logic for CustomColorWindow.xaml
    /// </summary>
    public partial class CustomColorWindow
    {
        LoggerWindow logger;

        string ColorValue;
        string Item;

        public CustomColorWindow(LoggerWindow logger, string item)
        {
            InitializeComponent();

            SS s = SS.getInstance();
			string theme = (SS.Theme == SS.ThemeType.Light) ? "Light" : "Dark";

			this.Resources.MergedDictionaries.Add(
				new ResourceDictionary { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme." + theme + ".xaml", UriKind.Absolute) });

            this.logger = logger;
            this.Item = item;
        }

        private void bApply_Click(object sender, RoutedEventArgs e)
        {
            if (tbValue.Text.Equals(""))
                MessageBox.Show("NULL value cannot be passed!");
            else
                logger.SetCustomColor(ColorValue, Item);
            this.Close();
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tbValue.Text.StartsWith("#"))
                ColorValue = tbValue.Text;
            else
                ColorValue = "#" + tbValue.Text;
        }

        private void tbValue_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                if (tbValue.Text.Equals(""))
                    MessageBox.Show("NULL value cannot be passed!");
                else
                    logger.SetCustomColor(ColorValue, Item);
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
