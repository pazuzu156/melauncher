﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Windows;
using System.Windows.Media;
using System.Web.Script.Serialization;

namespace MEL.Logger
{
	internal class LoggerSettings
	{
		private string SettingsDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @".\MELauncher\Settings\";
		private string SettingsFile;
		private string SettingsFileOld;
		private Dictionary<string, string> csc;

		public struct Colors
		{
			public const string Default = "#37474F";
			public const string Green = "#00C300";
			public const string Red = "#C91313";
			public const string Yellow = "#FFFF00";
			public const string Blue = "#0F0FE6";
			public const string Purple = "#950D95";
			public const string White = "#FFFFFF";
			public const string Black = "#000000";
		}

		public LoggerSettings()
		{
			SettingsFileOld = SettingsDir + @".\LoggerSettings.xml";
			SettingsFile = SettingsDir + @".\LoggerSettings.json";
			csc = new Dictionary<string, string>();

			if (File.Exists(SettingsFile) || File.Exists(SettingsFileOld))
				LoadSettings();
			else
				CreateSettingsFile();
		}

		private void LoadSettings()
		{
			if (File.Exists(SettingsFileOld))
			{
				XmlDocument doc = new XmlDocument();
				doc.Load(SettingsFileOld);

				XmlNode oNode = doc["Options"];
				Dictionary<string, XmlNode> nodeList = new Dictionary<string, XmlNode>();

				foreach (XmlNode node in oNode.ChildNodes)
				{
					nodeList.Add(node.Name, node);
				}

				string fg_color = nodeList["Foreground"].Attributes[0].Value;
				string bg_color = nodeList["Background"].Attributes[0].Value;

				csc = new Dictionary<string, string>();
				csc.Add("foreground", fg_color);
				csc.Add("background", bg_color);

				File.Delete(SettingsFileOld);
				WriteSettings(fg_color, bg_color);
			}
			else
			{
				FileStream file = File.Open(SettingsFile, FileMode.Open, FileAccess.Read);
				StreamReader reader = new StreamReader(file);
				var json = new JavaScriptSerializer().Deserialize<Json.JsonData>(reader.ReadToEnd());
				csc = new Dictionary<string, string>();
				csc.Add("foreground", json.settings.colors.foreground);
				csc.Add("background", json.settings.colors.background);
			}
		}

		public string currentSetColor(string key)
		{
			return csc[key];
		}

		public void setNewColor(string key, string value)
		{
			csc[key] = value;
		}

		private void CreateSettingsFile()
		{
			if (!Directory.Exists(SettingsDir))
				Directory.CreateDirectory(SettingsDir);

			SaveColors(LoggerSettings.Colors.Green, LoggerSettings.Colors.Default);

			LoadSettings();
		}

		public void SaveColors(string fg, string bg)
		{
			setNewColor("foreground", fg);
			setNewColor("background", bg);
			WriteSettings(fg, bg);
		}

		private void WriteSettings(string fg, string bg)
		{
			FileStream file = File.Open(SettingsFile, FileMode.OpenOrCreate, FileAccess.Write);
			StreamWriter writer = new StreamWriter(file);
			var json = new JavaScriptSerializer().Serialize(new
			{
				settings = new
				{
					colors = new
					{
						foreground = csc["foreground"],
						background = csc["background"]
					}
				}
			});
			writer.Write(json);
			writer.Flush();
			writer.Close();
			file.Close();
		}

		private void WriteXmlElement(XmlWriter writer, string element, string name, string value)
		{
			writer.WriteStartElement(element);
			writer.WriteAttributeString(name, value);
			writer.WriteEndElement();
		}
	}

	static class ColorExtention
	{
		public static Color ToColor(this uint value)
		{
			return Color.FromArgb((byte)((value >> 24) & 0xFF),
				(byte)((value >> 16) & 0xFF),
				(byte)((value >> 8) & 0xFF),
				(byte)(value & 0xFF));
		}
	}
}
