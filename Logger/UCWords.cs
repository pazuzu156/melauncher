﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEL.Logger
{
    /// <summary>
    /// The C# Equivilant if PHP's ucwords function
    /// 
    /// This class capitalizes the first letter in a word
    /// </summary>
    class UCWords
    {
        // Executes the UCWords class
        // Static. Called like UCWords.Exec("string to run on");
        public static string Exec(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            char[] a = s.ToCharArray();
            a[0] = char.ToUpper(a[0]);
            return new string(a);
        }
    }
}
