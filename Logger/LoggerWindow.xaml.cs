﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;

using SS = MEL.Settings.Settings;
using System.Diagnostics;

namespace MEL.Logger
{
    /// <summary>
    /// Interaction logic for LoggerWindow.xaml
    /// </summary>
    public partial class LoggerWindow
    {
        string window;
		DateTime BeginTime = DateTime.Now;
        LoggerSettings ls;

        CustomColorWindow ccw;

        public string ColorValue { get; set; }

        public LoggerWindow(string window)
        {
            InitializeComponent();

            this.window = window;

            WriteToLog("Initializing logger for " + UCWords.Exec(window) + "Window");

            SS s = SS.getInstance();
            ls = new LoggerSettings();

            tbLog.Background = (Brush)new BrushConverter().ConvertFromString(ls.currentSetColor("background"));
            tbLog.Foreground = (Brush)new BrushConverter().ConvertFromString(ls.currentSetColor("foreground"));

            SetMenuItemsChecks();

			string theme = (SS.Theme == SS.ThemeType.Light) ? "Light" : "Dark";

			this.Resources.MergedDictionaries.Add(
				new ResourceDictionary { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme." + theme + ".xaml", UriKind.Absolute) });
        }

        private void SetMenuItemsChecks()
        {
            switch(ls.currentSetColor("foreground"))
            {
                case LoggerSettings.Colors.Green:
                    miFG_Green.IsChecked = true;
                    break;
                case LoggerSettings.Colors.Red:
                    miFG_Red.IsChecked = true;
                    break;
                case LoggerSettings.Colors.Yellow:
                    miFG_Yellow.IsChecked = true;
                    break;
                case LoggerSettings.Colors.Blue:
                    miFG_Blue.IsChecked = true;
                    break;
                case LoggerSettings.Colors.Purple:
                    miFG_Purple.IsChecked = true;
                    break;
                case LoggerSettings.Colors.White:
                    miFG_White.IsChecked = true;
                    break;
                case LoggerSettings.Colors.Black:
                    miFG_Black.IsChecked = true;
                    break;
            }

            switch (ls.currentSetColor("background"))
            {
				case LoggerSettings.Colors.Default:
					miBG_Default.IsChecked = true;
					break;
                case LoggerSettings.Colors.Green:
                    miBG_Green.IsChecked = true;
                    break;
                case LoggerSettings.Colors.Red:
                    miBG_Red.IsChecked = true;
                    break;
                case LoggerSettings.Colors.Yellow:
                    miBG_Yellow.IsChecked = true;
                    break;
                case LoggerSettings.Colors.Blue:
                    miBG_Blue.IsChecked = true;
                    break;
                case LoggerSettings.Colors.Purple:
                    miBG_Purple.IsChecked = true;
                    break;
                case LoggerSettings.Colors.White:
                    miBG_White.IsChecked = true;
                    break;
                case LoggerSettings.Colors.Black:
                    miBG_Black.IsChecked = true;
                    break;
            }
        }

        public void WriteToLog(string write)
        {
            tbLog.Text += DateTime.Now + ": " + write + "\r\n";
            tbLog.ScrollToEnd();
        }

        public void WriteToLogFile()
        {
            if(!tbLog.Text.Equals(""))
            {
				if (File.Exists(SS.LauncherSettingsRootDir + @"\logs\" + window + "_log.log"))
					File.Delete(SS.LauncherSettingsRootDir + @"\logs\" + window + "_log.log");

				if (!Directory.Exists(SS.LauncherSettingsRootDir + @"\logs\"))
					Directory.CreateDirectory(SS.LauncherSettingsRootDir + @"\logs");

				FileStream file = File.Open(SS.LauncherSettingsRootDir + @"\logs\" + window + "_log_" + BeginTime.ToString("yyyyMMddHHmm") + ".log", FileMode.OpenOrCreate, FileAccess.Write);
                StreamWriter writer = new StreamWriter(file);
                writer.WriteLine("==== Begin log. Start time: " + BeginTime.ToString() + " ====\r\n");
                writer.WriteLine(tbLog.Text);
                writer.WriteLine("==== End log. Time: " + DateTime.Now + " ====");
                writer.Flush();
                writer.Close();
            }
        }

        private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        #region Color options menu
        #region Foreground
        private void FG_Green_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Check Boxes house keeping
            miFG_Green.IsChecked = true;
            tbLog.Foreground = (Brush)new BrushConverter().ConvertFromString(LoggerSettings.Colors.Green);
            ls.SaveColors(LoggerSettings.Colors.Green, ls.currentSetColor("background"));
            WriteToLog("Setting logger foreground color to green. Hex: " + LoggerSettings.Colors.Green);
        }

        private void FG_Red_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Check Boxes house keeping
            miFG_Red.IsChecked = true;
            tbLog.Foreground = (Brush)new BrushConverter().ConvertFromString(LoggerSettings.Colors.Red);
			ls.SaveColors(LoggerSettings.Colors.Red, ls.currentSetColor("background"));
            WriteToLog("Setting logger foreground color to red. Hex: " + LoggerSettings.Colors.Red);
        }

        private void FG_Yellow_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Check Boxes house keeping
            miFG_Yellow.IsChecked = true;
            tbLog.Foreground = (Brush)new BrushConverter().ConvertFromString(LoggerSettings.Colors.Yellow);
			ls.SaveColors(LoggerSettings.Colors.Yellow, ls.currentSetColor("background"));
            WriteToLog("Setting logger foreground color to yellow. Hex: " + LoggerSettings.Colors.Yellow);
        }

        private void FG_Blue_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Check Boxes house keeping
            miFG_Blue.IsChecked = true;
            tbLog.Foreground = (Brush)new BrushConverter().ConvertFromString(LoggerSettings.Colors.Blue);
			ls.SaveColors(LoggerSettings.Colors.Blue, ls.currentSetColor("background"));
            WriteToLog("Setting logger foreground color to blue. Hex: " + LoggerSettings.Colors.Blue);
        }

        private void FG_Purple_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Check Boxes house keeping
            miFG_Purple.IsChecked = true;
            tbLog.Foreground = (Brush)new BrushConverter().ConvertFromString(LoggerSettings.Colors.Purple);
			ls.SaveColors(LoggerSettings.Colors.Purple, ls.currentSetColor("background"));
            WriteToLog("Setting logger foreground color to purple. Hex: " + LoggerSettings.Colors.Purple);
        }

        private void FG_White_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Check Boxes house keeping
            miFG_White.IsChecked = true;
            tbLog.Foreground = (Brush)new BrushConverter().ConvertFromString(LoggerSettings.Colors.White);
			ls.SaveColors(LoggerSettings.Colors.White, ls.currentSetColor("background"));
            WriteToLog("Setting logger foreground color to white. Hex: " + LoggerSettings.Colors.White);
        }

        private void FG_Black_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Check Boxes house keeping
            miFG_Black.IsChecked = true;
            tbLog.Foreground = (Brush)new BrushConverter().ConvertFromString(LoggerSettings.Colors.Black);
			ls.SaveColors(LoggerSettings.Colors.Black, ls.currentSetColor("background"));
            WriteToLog("Setting logger foreground color to black. Hex: " + LoggerSettings.Colors.Black);
        }

        private void FG_Custom_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Check Boxes house keeping
            miBG_Custom.IsChecked = true;
            var ccw = new CustomColorWindow(this, "foreground");
            ccw.ShowDialog();
        }
        #endregion

        #region Background
		private void BG_Default_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			miBG_Default.IsCheckable = true;
			tbLog.Background = (Brush)new BrushConverter().ConvertFromString(LoggerSettings.Colors.Default);
			ls.SaveColors(ls.currentSetColor("foreground"), LoggerSettings.Colors.Default);
			WriteToLog("Setting logger background to default color. Hex: " + LoggerSettings.Colors.Default);
		}
        private void BG_Green_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Check Boxes house keeping
            miBG_Green.IsChecked = true;
            tbLog.Background = (Brush)new BrushConverter().ConvertFromString(LoggerSettings.Colors.Green);
			ls.SaveColors(ls.currentSetColor("foreground"), LoggerSettings.Colors.Green);
			WriteToLog("Setting logger background color to green. Hex: " + LoggerSettings.Colors.Green);
        }

        private void BG_Red_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Check Boxes house keeping
            miBG_Red.IsChecked = true;
            tbLog.Background = (Brush)new BrushConverter().ConvertFromString(LoggerSettings.Colors.Red);
			ls.SaveColors(ls.currentSetColor("foreground"), LoggerSettings.Colors.Red);
			WriteToLog("Setting logger background color to red. Hex: " + LoggerSettings.Colors.Red);
        }

        private void BG_Yellow_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Check Boxes house keeping
            miBG_Yellow.IsChecked = true;
            tbLog.Background = (Brush)new BrushConverter().ConvertFromString(LoggerSettings.Colors.Yellow);
			ls.SaveColors(ls.currentSetColor("foreground"), LoggerSettings.Colors.Yellow);
			WriteToLog("Setting logger background color to yellow. Hex: " + LoggerSettings.Colors.Yellow);
        }

        private void BG_Blue_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Check Boxes house keeping
            miBG_Blue.IsChecked = true;
            tbLog.Background = (Brush)new BrushConverter().ConvertFromString(LoggerSettings.Colors.Blue);
			ls.SaveColors(ls.currentSetColor("foreground"), LoggerSettings.Colors.Blue);
			WriteToLog("Setting logger background color to blue. Hex: " + LoggerSettings.Colors.Blue);
        }

        private void BG_Purple_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Check Boxes house keeping
            miBG_Purple.IsChecked = true;
            tbLog.Background = (Brush)new BrushConverter().ConvertFromString(LoggerSettings.Colors.Purple);
			ls.SaveColors(ls.currentSetColor("foreground"), LoggerSettings.Colors.Purple);
			WriteToLog("Setting logger background color to purple. Hex: " + LoggerSettings.Colors.Purple);
        }

        private void BG_White_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Check Boxes house keeping
            miBG_White.IsChecked = true;
            tbLog.Background = (Brush)new BrushConverter().ConvertFromString(LoggerSettings.Colors.White);
			ls.SaveColors(ls.currentSetColor("foreground"), LoggerSettings.Colors.White);
			WriteToLog("Setting logger background color to white. Hex: " + LoggerSettings.Colors.White);
        }

        private void BG_Black_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Check Boxes house keeping
            miBG_Black.IsChecked = true;
            tbLog.Background = (Brush)new BrushConverter().ConvertFromString(LoggerSettings.Colors.Black);
			ls.SaveColors(ls.currentSetColor("foreground"), LoggerSettings.Colors.Black);
			WriteToLog("Setting logger background color to black. Hex: " + LoggerSettings.Colors.Black);
        }

        private void BG_Custom_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            // Check Boxes house keeping
            miBG_Custom.IsChecked = true;
            var ccw = new CustomColorWindow(this, "background");
            ccw.ShowDialog();
        }
        #endregion
        #endregion

        public void SetCustomColor(string color, string item)
        {
            switch(item)
            {
                case "foreground":
                    tbLog.Foreground = (Brush)new BrushConverter().ConvertFromString(color);
                    ls.SaveColors(color, ls.currentSetColor("background"));
					WriteToLog("Setting logger foreground color to custom color. Hex: " + color);
                    break;
                case "background":
                    tbLog.Background = (Brush)new BrushConverter().ConvertFromString(color);
                    ls.SaveColors(ls.currentSetColor("foreground"), color);
					WriteToLog("Setting logger background color to custom color. Hex: " + color);
                    break;
            }
        }

		private void CommandBinding_Executed_1(object sender, ExecutedRoutedEventArgs e)
		{
			string dir = SS.LauncherSettingsRootDir + @"logs";
			if(Directory.Exists(dir))
			{
				ProcessStartInfo psi = new ProcessStartInfo();
				psi.FileName = Environment.GetFolderPath(Environment.SpecialFolder.Windows) + @"\explorer.exe";
				psi.Arguments = dir;
				Process.Start(psi);
			}
			else
			{
				MessageBox.Show("There are currently no logs saved yet!");
			}
		}
    }
}
