﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEL.Settings.JSON
{
	public class Setting
		: RootObject
	{
		public string name { get; set; }
		public string type { get; set; }
		public string value { get; set; }
	}

	public class RootObject
	{
		public List<Setting> settings { get; set; }
	}
}
