﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml;
using System.Collections.Specialized;
using Microsoft.Win32;
using System.Globalization;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace MEL.Settings
{
    public class Settings
    {
        public enum DRMType
        {
            Origin, Steam, NULL
        }

        public enum ThemeType
        {
            Light, Dark
        }

		public static Dictionary<string, string> OriginGameIDs { get; private set; }
		public static Dictionary<string, string> SteamGameIDs { get; private set; }

        public static string AppID { get; private set; }
        public static bool AOT { get; private set; }
        public static bool StartUp { get; private set; }
        public static bool CloseOnRun { get; private set; }
        public static bool Exists { get; private set; }
        public static DRMType DRM { get; private set; }
        public static string Version { get; private set; }
        public static string SteamLocation { get; private set; }
        public static string OriginLocation { get; private set; }
        public static string MELocation { get; private set; }
        public static string ME2Location { get; private set; }
        public static string ME3Location { get; private set; }
		public static string MEALocation { get; private set; }
        public static ThemeType Theme { get; private set; }
        public static string Language { get; private set; }
		public static string LauncherSettingsRootDir { get; private set; }
        public static string LauncherSettingsFile { get; private set; }
		public static string LauncherSettingsFileOld { get; private set; }
        public static string ChangelogFile { get; private set; }
        public static string CrashHandlerFile { get; private set; }

		private List<Setting> _settings;

        public static Settings getInstance() { return new Settings(); }

        private Settings()
        {
			LauncherSettingsRootDir = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + @"\MELauncher\";
			LauncherSettingsFile = LauncherSettingsRootDir + @"\Settings\MELauncherSettings.json";
			LauncherSettingsFileOld = LauncherSettingsRootDir + @"\Settings\MELauncherSettings.xml";
			ChangelogFile = LauncherSettingsRootDir + @"\MELauncherChangelog.html";
			CrashHandlerFile = LauncherSettingsRootDir + @"\MELauncherCrashHandler.lock";

			OriginGameIDs = new Dictionary<string, string>();
			SteamGameIDs = new Dictionary<string, string>();

			OriginGameIDs.Add("me", "DR:102427200");
			OriginGameIDs.Add("me2", "DR:150391400");
			OriginGameIDs.Add("me3", "DR:230773600");
			OriginGameIDs.Add("mea", "Origin.OFR.50.0000367");

			SteamGameIDs.Add("me1", "17460");
			SteamGameIDs.Add("me2", "24980");

			_settings = new List<Setting>();
        }

        public void LoadSettings()
        {
			if(File.Exists(LauncherSettingsFile))
			{
				var json = new JSON.RootObject();
				using(var reader = new StreamReader(File.Open(LauncherSettingsFile, FileMode.Open, FileAccess.Read)))
				{
					json = new JavaScriptSerializer().Deserialize<JSON.RootObject>(reader.ReadToEnd());
				}

				foreach(var item in json.settings)
				{
					switch(item.name)
					{
						case "AppID":
							AppID = item.value;
							break;
						case "Version":
							Version = item.value;
							break;
						case "SteamLocation":
							SteamLocation = item.value;
							break;
						case "OriginLocation":
							OriginLocation = item.value;
							break;
						case "MELocation":
							MELocation = item.value;
							break;
						case "ME2Location":
							ME2Location = item.value;
							break;
						case "ME3Location":
							ME3Location = item.value;
							break;
						case "MEALocation":
							MEALocation = item.value;
							break;
						case "TopLevel":
							if (item.value.Equals("True"))
								AOT = true;
							else
								AOT = false;
							break;
						case "StartUp":
							if (item.value.Equals("True"))
								StartUp = true;
							else
								StartUp = false;
							break;
						case "CloseOnRun":
							if (item.value.Equals("True"))
								CloseOnRun = true;
							else
								CloseOnRun = false;
							break;
						case "DRMType":
							if (item.value.Equals("Steam"))
								DRM = DRMType.Steam;
							else if (item.value.Equals("Origin"))
								DRM = DRMType.Origin;
							else if (item.value.Equals("Unset"))
								DRM = DRMType.NULL;
							break;
						case "Theme":
							if (item.value.Equals("Light"))
								Theme = ThemeType.Light;
							else if (item.value.Equals("Dark"))
								Theme = ThemeType.Dark;
							break;
						case "Language":
							Language = item.value;
							break;
					}
				}
			}
            else if (File.Exists(LauncherSettingsFileOld))
            {
                Exists = true;
                XmlDocument doc = new XmlDocument();
                doc.Load(LauncherSettingsFileOld);
                XmlNodeList nodes = doc.GetElementsByTagName("Option");

                foreach (XmlNode node in nodes)
                {
                    XmlAttributeCollection attribs = node.Attributes;
                    StringDictionary nodeAttribList = new StringDictionary();

                    foreach (XmlAttribute attrib in attribs)
                    {
                        nodeAttribList.Add(attrib.Name, attrib.Value);
                    }

                    switch (nodeAttribList["name"])
                    {
                        case "AppID":
                            AppID = nodeAttribList["value"];
                            break;
                        case "Version":
                            Version = nodeAttribList["value"];
                            break;
                        case "SteamLocation":
                            SteamLocation = nodeAttribList["value"];
                            break;
                        case "OriginLocation":
                            OriginLocation = nodeAttribList["value"];
                            break;
                        case "MELocation":
                            MELocation = nodeAttribList["value"];
                            break;
                        case "ME2Location":
                            ME2Location = nodeAttribList["value"];
                            break;
                        case "ME3Location":
                            ME3Location = nodeAttribList["value"];
                            break;
						case "MEALocation":
							MEALocation = nodeAttribList["value"];
							break;
                        case "TopLevel":
                            if (nodeAttribList["value"] == "true")
                                AOT = true;
                            else
                                AOT = false;
                            break;
                        case "StartUp":
                            if (nodeAttribList["value"] == "true")
                                StartUp = true;
                            else
                                StartUp = false;
                            break;
                        case "CloseOnRun":
                            if (nodeAttribList["value"] == "true")
                                CloseOnRun = true;
                            else
                                CloseOnRun = false;
                            break;
                        case "DRMType":
                            if (nodeAttribList["value"] == "Steam")
                                DRM = DRMType.Steam;
                            else if (nodeAttribList["value"] == "Origin")
                                DRM = DRMType.Origin;
                            else if (nodeAttribList["value"] == "Unset")
                                DRM = DRMType.NULL;
                            break;
                        case "Theme":
                            if (nodeAttribList["value"] == "Light")
                                Theme = ThemeType.Light;
                            else if (nodeAttribList["value"] == "Dark")
                                Theme = ThemeType.Dark;
                            break;
                        case "Language":
                            Language = nodeAttribList["value"];
                            break;
                    }
                }

				SaveSettings(AppID, Version, SteamLocation, OriginLocation, MELocation, ME2Location, ME3Location, MEALocation, DRM, Theme, Language, CloseOnRun, AOT, StartUp);
				LoadSettings();
            }
            else
            {
                var lang = CultureInfo.CurrentCulture.ToString();
                SaveSettings("", "0.0.0.0", "", "", "", "", "", "", DRMType.NULL, ThemeType.Light, lang, false, false, false);
                Exists = false;
            }
        }

		public class Setting
		{
			public string name { get; set; }
			public string type { get; set; }
			public string value { get; set; }
		}

        public void SaveSettings(string id, string v, string sl, string ol, string ml, string m2l, string m3l, string mal, DRMType drm, ThemeType theme, string lang, bool cor, bool aot, bool startup)
        {
			if (!Directory.Exists(LauncherSettingsRootDir + @"\Settings"))
				Directory.CreateDirectory(LauncherSettingsRootDir + @"\Settings");

            setSettings(id, v, sl, ol, ml, m2l, m3l, mal, drm, theme, lang, cor, aot, startup);

			FileStream file = File.Open(LauncherSettingsFile, FileMode.Create, FileAccess.Write);
			using(var writer = new StreamWriter(file))
			{
				var json = new JavaScriptSerializer().Serialize(new
					{
						settings = _settings
					});
				writer.Write(json);
				writer.Flush();
			}
			file.Close();
        }

        private void WriteXmlElement(XmlWriter writer, string name, string type, string value)
        {
            writer.WriteStartElement("Option");
            writer.WriteAttributeString("name", name);
            writer.WriteAttributeString("type", type);
            writer.WriteAttributeString("value", value);
            writer.WriteEndElement();
        }

        private void setSettings(string id, string v, string sl, string ol, string ml, string m2l, string m3l, string mal, DRMType drm, ThemeType theme, string lang, bool cor, bool aot, bool startup)
        {
            AppID = id;
            Version = v;
            SteamLocation = sl;
            OriginLocation = ol;
            MELocation = ml;
            ME2Location = m2l;
            ME3Location = m3l;
			MEALocation = mal;
            DRM = drm;
            Theme = theme;
            Language = lang;
            CloseOnRun = cor;
            AOT = aot;
            StartUp = startup;

			string dr = string.Empty;
			switch(DRM)
			{
				case DRMType.Origin:
					dr = "Origin";
					break;
				case DRMType.Steam:
					dr = "Steam";
					break;
				case DRMType.NULL:
					dr = "Unset";
					break;
			}

			_settings.Add(new Setting() { name = "AppID", value = AppID, type = "GUID" });
			_settings.Add(new Setting() { name = "Version", value = Version, type = "Version" });
			_settings.Add(new Setting() { name = "SteamLocation", value = SteamLocation, type = "string" });
			_settings.Add(new Setting() { name = "OriginLocation", value = OriginLocation, type = "string" });
			_settings.Add(new Setting() { name = "MELocation", value = MELocation, type = "string" });
			_settings.Add(new Setting() { name = "ME2Location", value = ME2Location, type = "string" });
			_settings.Add(new Setting() { name = "ME3Location", value = ME3Location, type = "string" });
			_settings.Add(new Setting() { name = "MEALocation", value = MEALocation, type = "string" });
			_settings.Add(new Setting() { name = "DRMType", value = dr, type = "enum" });
			_settings.Add(new Setting() { name = "Theme", value = Theme.ToString(), type = "enum" });
			_settings.Add(new Setting() { name = "Language", value = Language, type = "string" });
			_settings.Add(new Setting() { name = "CloseOnRun", value = CloseOnRun.ToString(), type = "boolean" });
			_settings.Add(new Setting() { name = "TopLevel", value = AOT.ToString(), type = "boolean" });
			_settings.Add(new Setting() { name = "StartUp", value = StartUp.ToString(), type = "boolean" });

			

			if (File.Exists(LauncherSettingsFileOld))
				File.Delete(LauncherSettingsFileOld);

            RegistryKey startupKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
            if (startup)
                startupKey.SetValue("MELauncher", Environment.CurrentDirectory + @"\MELauncher.exe");
            else
                startupKey.DeleteValue("MELauncher", false);
        }
    }
}
