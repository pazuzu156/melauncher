@echo off

rem Uncomment the first PF variable and comment the second
rem if you are on a 32 bit OS
rem set PF=%PROGRAMFILES%
set PF=%PROGRAMFILES(x86)%

set CWD=%~dp0
set MSBUILD="%PF%\Microsoft Visual Studio\2019\Community\MSBuild\Current\Bin\MSBuild.exe"
set NUGET=utils\nuget.exe
set HHC="%PF%\HTML Help Workshop\hhc.exe"
set ISS="%PF%\Inno Setup 5\iscc.exe"

set CFLAGS=/m /nodereuse:false /p:Configuration=Release;AllowUnsafeBlocks=true /p:CLSCompliant=False
set LDFLAGS=/tv:4.0 /p:TargetFrameworkVersion=v4.5 /p:Platform="Any Cpu" /p:OutputPath="%CWD%\bin"

if exist build.log del build.log

echo.
echo Compiling HTML help file
%HHC% mel_help\MELauncherHelp.hhp
echo.

echo Retrieving NuGet packages..
%NUGET% install MELauncher\packages.config -o packages
echo.
echo Building project...
%MSBUILD% MELauncher.sln %CFLAGS% %LDFLAGS%
echo.
echo Building installer...
%ISS% setup.iss /dBuild=Release
echo.
echo Building translater installer...
%ISS% trans-setup.iss /dVersion=%VERSIONT% /dBuild=Release
pause
