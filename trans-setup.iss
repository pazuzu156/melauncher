#define AppName "Translator Standalone"
;#define Version "1.0"
;#define Build "Debug"
#define TransName "Translation Tool"
#define Publisher "Kaleb Klein"
#define SetupName "translator_standalone_setup"

[Setup]
AppId={{19A14197-C7BD-4084-9206-4F24CA649569}
AppName={#AppName}
AppVersion={#Version}
AppVerName={#AppName} {#Version}
AppPublisher={#Publisher}
DefaultDirName={pf}\{#AppName}
DefaultGroupName={#AppName}
OutputBaseFilename={#SetupName}-{#Version}-{#Build}
OutputDir={#Build}
SetupIconFile=Translator\translate-big.ico
WizardImageFile=compiler:WizModernImage-IS.bmp
Compression=lzma
SolidCompression=yes
UninstallDisplayName=Translator
UninstallDisplayIcon={app}\{#AppName}.exe
ShowTasksTreeLines=True
PrivilegesRequired=admin

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Types]
Name: "default"; Description: "Default Installation"

[Components]
Name: "app"; Description: "Default Installation"; Types: default; Flags: fixed

[CustomMessages]
DesktopIcon=Create desktop icon for %1

[Tasks]
Name: "desktopicon"; Description: "{cm:DesktopIcon,{#AppName}}"; GroupDescription: "{cm:AdditionalIcons}";

[Code]
function IsDotNetDetected(version: string; service: cardinal): boolean;

var
  key: string;
  install, release, serviceCount: cardinal;
  check45, success: boolean;
//var reqNetVer: string;
begin
   // .NET 4.5 installes as update to 4.0
   if version = 'v4.5' then begin
     version := 'v4\Full';
     check45 := true;
   end else
     check45 := false;

   // Key group for .NET
   key := 'SOFTWARE\Microsoft\NET Framework Setup\NDP\' + version;

   // .NET 3.0
   if Pos('v3.0', version) = 1 then begin
     success := RegQueryDWordValue(HKLM, key + '\Setup', 'InstallSuccess', install);
   end else begin
     success := RegQueryDWordValue(HKLM, key, 'Install', install);
   end;

   // .NET 4
   if Pos('v4', version) = 1 then begin
     success := success and RegQueryDWordValue(HKLM, key, 'Servicing', serviceCount);
   end else begin
     success := success and RegQueryDWordValue(HKLM, key, 'SP', serviceCount);
   end;

   // .NET 4.5
   if check45 then begin
     success := success and RegQueryDWordValue(HKLM, key, 'Release', release);
     success := success and (release >= 378389);
   end;

   result := success and (install = 1) and (serviceCount >= service);
end;

function IsReqDotNetDetected(): Boolean;
begin
  result := IsDotNetDetected('v4\Full', 0);
end;

function InitializeSetup(): Boolean;
begin
  if not IsDotNetDetected('v4\Full', 0) then begin
    MsgBox('{#AppName} requires Microsoft .NET Framework 4.5.'#13#13
      'The installer will attempt to install it for you.', mbInformation, MB_OK);
  end;

  result := true;
end;

[Files]
Source: "bin\Translator.exe"; DestDir: "{app}"; Flags: ignoreversion; Components: app
Source: "bin\MEL.Locale.dll"; DestDir: "{app}"; Flags: ignoreversion; Components: app
Source: "bin\Translator.exe.config"; DestDir: "{app}"; Flags: ignoreversion; Components: app
Source: "readme.html"; DestDir: "{app}"; Flags: ignoreversion; Components: app
Source: "redist\dotNetFx45_Full_setup.exe"; DestDir: "{app}\redist"; Flags: ignoreversion deleteafterinstall; Components: app; Check: not IsReqDotNetDetected

; Language Packs
Source: "Languages\locale\*.lang"; DestDir: "{userappdata}\MELauncher\locale"; Components: app; Flags: uninsneveruninstall

[Icons]
Name: "{group}\{#AppName}"; Filename: "{app}\Translator.exe"
Name: "{commondesktop}\{#AppName}"; Filename: "{app}\Translator.exe"
Name: "{group}\{cm:UninstallProgram,{#AppName}}"; Filename: "{uninstallexe}"

[Run]
Filename: "{app}\redist\dotNetFx54_Fill_setup.exe"; Parameters: "/passive /norestart"; Check: not IsReqDotNetDetected
Filename: "{app}\readme.html"; Description: "View Readme File (Recommended)"; Flags: nowait postinstall skipifsilent shellexec
Filename: "{app}\{#AppName}"; Description: "{cm:LaunchProgram,{#StringChange(AppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent shellexec unchecked