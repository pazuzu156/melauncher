# MELauncher Translation Tool

## Contents
* About
* Prerequisites
* Setup
* Editing
* Saving
* Submitting
* Keybindings
* Future Features

### About
MELauncher is a mod tool for modding the Mass Effect games, utilizing TexMod. This tool is used to translate MELauncher into multiple languages.

### Prerequisites
[.NET Framework 4.5](http://www.microsoft.com/en-us/download/details.aspx?id=30653)

### Setup
Make sure the .NET Framework v4.5 is installed. The installer should have set this up automatically, but if not, use the link in the prerequisites section to get an installer for .NET.

### Editing
To create a new translation, using the menu bar at the top: "File > New". Once you've started a new translation, use the language selection dropdown menu at the bottom to select the language you will be translating to.  

To Save your translation, click the big "Save Translation" button at the bottom or by using the menu option: "File > Save"  

To edit one of the cells, double click on it. Be careful in the edit box, the option to rename the key is present, and changing this name could prevent that string from showing up in MELauncher.  

To add or remove items from the list, use "Language Items > Add New Item | Remove Selected Item".  
The Add New Item option will give you a window, where you can select what section to place the item under, and the key for that item.

### Saving
In order to submit a translation, it needs to be saved. If you haven't already, you need to select the language that this translation is for before saving it. 

### Submitting
To submit a translation, be sure you're at least finished with it. This isn't a requirement, but it's helpful so an unfinished translation isn't being used. (It's quite confusing to see a Spanish translation with half of it in English, no?)

To submit, use the menu option: "File > Sync Language Pack" This option will submit the language pack to the translation's upload server.

### Keybindings
```
Ctrl+N - Creates a new Translation based off of the English language pack
Ctrl+O - Opens a translation pack
Ctrl+S - Saves current translation
Ctrl+Shift+S - Syncs language pack
Ctrl+F - Find
Ins - Add a new item
Del - Remove selected item(s)
F1 - Opens help window
Alt+F4 - Closes application
```

### Future Features
* Adding support for obtaining submitted language packs from upload server
