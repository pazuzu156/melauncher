# MELauncher

A mod tool for Mass Effect 1, 2, 3, and Andromeda. Utilizes TexMod for applying mods.

TexMod is not included, but does obtain it from the official TexMod repository

## Build/Install

To build, make sure that you have Visual Studio 2019 installed, or edit the MSBUILD variable in `build.bat` appropriately to point to MSBuild for your version of Visual Studio. With all the prerequesites installed, run `build.bat` and the project will build, install all needed NuGet packages, and build the installers for both MELauncher and the standalone translator tool.

## Install

Just run the installer generated from the build script. Same goes for the standalone translator (The translator is also packaged with MELauncher, so you don't need to install both)

## Prerequisites

[VCL Styles for Inno Setup][2]  
[Visual Studio 2019][3]  
[Inno Setup 5 v5.5.6][4]  
[HTML Help Workshop][5]  

[2]:https://github.com/RRUZ/vcl-styles-plugins/wiki/Inno-Setup
[3]:https://visualstudio.microsoft.com
[4]:http://www.jrsoftware.org/isdl.php
[5]:https://msdn.microsoft.com/en-us/library/windows/desktop/ms669985%28v=vs.85%29.aspx

## Notes

This project was a passion project, and I spent years building it, from a small batch script I found online, to what it became. Please be kind and share this code, do what you want with it, but remember who made it ;)

MELauncher is licensed under the MIT license. The old license which was for a closed source version of MELauncher is no longer valid, but kept for archival purposes.
