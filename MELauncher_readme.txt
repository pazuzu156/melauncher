MELauncher Readme
=================

Disclaimer
----------
MELauncher is freeware: meaning it's free to use and share,
but you cannot modify/reverse engineer it in any way.
Distributing this software elsewhere than the NexusMods is granted,
but with the credits of the author included. (This Readme)

This program is distributed in the hope that it will be useful,
but WITHOUT ANT WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. MELauncher is
provided AS IS, and the author assumes NO LIABILITY for the outcomes
of using this software. You hereby agree to this disclaimer by using
MELauncher. If you do not agree to adhere to this disclaimer, you
should not use this software. Should you revoke this disclaimer in
the future, you should immediately discontinue use of this software


About
-----
MELauncher started as a batch script. A simple batch script published on one of the
Mass Effect 3 mod forums. The script was then adapted and modified to suit my needs.
After some time with it, the script was then rewriten from the ground up and added
into a GUI application with the same goals as before, but with a GUI instead.

Soon, the script was dropped as all the functionality of that script was integrated
into the application itself, becoming ME3Launcher. Soon, extra features such as auto-detect, settings, 
integrated updater, and others began taking form.

Over the next year, more upgrades came about, many bugfixes and patches that began to shape
the application into something truely useful. March of 2014, version 2.0 was released. A
complete rewrite of the entire application, and a new UI. Many problems were squashed, but
many new ones were introduced. Over the next several months, Many were clened, and the project abandoned.
Late 2014, ME3Launcher was picked back up, patched, and released on NexusMods for the first time.

Over the next month, ME3Launcher gained a foothold, making the project alive once again.
Version 3.0 was released soon after, adding support for Mass Effect 2, and effectively became
MELauncher.

Now, MELauncher 4.0 was been released, with a completely new UI, support for all three Mass Effect
titles, and support for both Origin and Steam.


Use of MELauncher
-----------------
Using MELauncher is simple, even with it's many buttons and such.

Depending on the meduim you use to play Mass Effect will determine the choice
you make when first starting MELauncher.

The DRM selection screen is where you'll make your choice. If you have ME on Steam,
that will be your choice, or Origin maybe? :p

When the main window is shown, you have a couple options of setting up MELauncher.
You can manually browse for the locations of the DRM and Mass Effect, or you can
use the auto-detection.

To launch the game, it must be done through the DRM software, not MELauncher.
First, you need TexMod, the universal modding tool for applying mods to games.
TexMod does not come with MELauncher, but will grab it for you, or give you the option
of obtaining it manually. The easiest way is the automatic way.

Select a game you wish to apply mods to from the Game Selection box, and click
"Enable TexMod" If you do not have TexMod placed for that game, a window will
come up with two options for obtaining it. Once TexMod is in place, press the
enable button again, then click the "Launch <DRM>" button. From there you will
launch your game as normal, but be presented with TexMod instead of your game.

You will select and launch your game from TexMod, along with the mods you want
to apply.


Removing MELauncher
-------------------
MELauncher comes with an uninstaller. Use this to completely remove MELauncher
from your machine. Doing so will also remove any TexMod installations that were
applied through MELauncher.


Updating MELauncher
-------------------
MELauncher upgrades are always packaged into their own installer, and can me
obtained from NexusMods. Minor updates, a.k.a. patches, are only obtained through
the built-in updater for MELauncher. If a patch has been released, but you only have
the installer for an older version, the patches will be applied given you choose to
do the update when the updater comes up.


Issues
------
No software is perfect, and bugs are always a possibility. If you ever come across
a bug or issue that you can reproduce, please feel free to submit a bug request either
through the bug reporter inside MELauncher, or via email at melbugs123@gmail.com


Contact
-------
If you have any personal inquiries about MELauncher, or jsut questions in general,
you can contact me through NexusMods, either through the Q&A section of MELauncher's
page, or through private messaging. You can also send an email at klein.jae@gmail.com
