@echo off

if exist MELauncher\bin rmdir /S /Q MELauncher\bin
if exist MELauncher\obj rmdir /S /Q MELauncher\obj
if exist LiveInstaller\bin rmdir /S /Q LiveInstaller\bin
if exist LiveInstaller\obj rmdir /S /Q LiveInstaller\obj
if exist Settings\bin rmdir /S /Q Settings\bin
if exist Settings\obj rmdir /S /Q Settings\obj
if exist Updater\bin rmdir /S /Q Updater\bin
if exist Updater\obj rmdir /S /Q Updater\obj
if exist Locale\bin rmdir /S /Q Locale\bin
if exist Locale\obj rmdir /S /Q Locale\obj
if exist Logger\obj rmdir /S /Q Logger\obj
if exist Logger\bin rmdir /S /Q Logger\bin
if exist Translator\bin rmdir /S /Q Translator\bin
if exist Translator\obj rmdir /S /Q Translator\obj
if exist MELauncher.suo del MELauncher.suo
if exist MELauncher.v12.suo del MELauncher.v12.suo
if exist Release rmdir /S /Q Release
if exist build rmdir /S /Q build
if exist Debug rmdir /S /Q Debug
if exist mel_help\MELauncherHelp.chm del mel_help\MELauncherHelp.chm
if exist packages rmdir /S /Q packages
if exist bin rmdir /S /Q bin
if exist build.log del build.log
