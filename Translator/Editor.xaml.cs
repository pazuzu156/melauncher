﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Translator
{
    /// <summary>
    /// Interaction logic for Editor.xaml
    /// </summary>
    public partial class Editor : Window
    {
        public string Name { get; private set; }
        public string Value { get; private set; }
        public bool Changed { get; private set; }
        public Editor(string name, string value)
        {
            InitializeComponent();
            Name = name;
            Value = value;
            tbName.Text = Name;
            tbEdit.Text = Value;
            this.Changed = false;
        }

        private void bSave_Click(object sender, RoutedEventArgs e)
        {
            this.Value = tbEdit.Text;
            this.Name = tbName.Text;
            this.Changed = true;
            this.Close();
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
    }
}
