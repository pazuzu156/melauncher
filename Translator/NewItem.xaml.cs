﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Translator
{
    /// <summary>
    /// Interaction logic for NewItem.xaml
    /// </summary>
    public partial class NewItem : Window
    {
        public string ItemType { get; private set; }
        public string ItemName { get; private set; }
        public bool Changed { get; private set; }
        public NewItem()
        {
            InitializeComponent();
            Changed = false;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.ItemType = cmbItemType.Text;
            this.ItemName = tbItemName.Text;
            this.Changed = true;
            this.Close();
        }

        private void tbItemName_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                this.ItemType = cmbItemType.Text;
                this.ItemName = tbItemName.Text;
                this.Changed = true;
                this.Close();
            }
        }

        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }
    }
}
