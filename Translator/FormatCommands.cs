﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Translator
{
    public static class FormatCommands
    {
        static FormatCommands()
        {
            Exit = new RoutedUICommand("E_xit", "Exit", typeof(FormatCommands),
                new InputGestureCollection
                {
                    new KeyGesture(Key.F4, ModifierKeys.Alt, "Alt+F4")
                });
            Sync = new RoutedUICommand("S_ync Language Pack", "Sync", typeof(FormatCommands),
                new InputGestureCollection
                {
                    new KeyGesture(Key.S, ModifierKeys.Control | ModifierKeys.Shift, "Ctrl+Shift+S")
                });
            About = new RoutedUICommand("_About", "About", typeof(FormatCommands),
                new InputGestureCollection
                {
                    new KeyGesture(Key.F1, ModifierKeys.None, "F1")
                });
            Add = new RoutedUICommand("A_dd New Item", "Add", typeof(FormatCommands),
                new InputGestureCollection
                {
                    new KeyGesture(Key.Insert, ModifierKeys.None, "Ins")
                });
            Delete = new RoutedUICommand("_Remove Selected Item", "Remove", typeof(FormatCommands),
                new InputGestureCollection
                {
                    new KeyGesture(Key.Delete, ModifierKeys.None, "Del")
                });
            ExitAbout = new RoutedUICommand("Close About Window", "Close", typeof(FormatCommands),
                new InputGestureCollection
                {
                    new KeyGesture(Key.Escape, ModifierKeys.None, "Escape")
                });
            FindAndReplace = new RoutedUICommand("Find and _Replace", "FindAndReplace", typeof(FormatCommands),
                new InputGestureCollection
                {
                    new KeyGesture(Key.F, ModifierKeys.Shift | ModifierKeys.Control, "Ctrl+Shift+F")
                });
            Exit2 = new RoutedUICommand("E_xit", "Exit2", typeof(FormatCommands),
                new InputGestureCollection
                {
                    new KeyGesture(Key.Escape, ModifierKeys.None, "Escape")
                });
			Import = new RoutedUICommand("Import", "Import", typeof(FormatCommands));
			Export = new RoutedUICommand("Export", "Export", typeof(FormatCommands));
			ToXML = new RoutedUICommand("To XML", "ToXML", typeof(FormatCommands),
				new InputGestureCollection
				{
					new KeyGesture(Key.M, ModifierKeys.Control | ModifierKeys.Shift, "Ctrl+Shift+M")
				});
			FromXML = new RoutedUICommand("From XML", "FromXML", typeof(FormatCommands),
				new InputGestureCollection
				{
					new KeyGesture(Key.M, ModifierKeys.Control, "Ctrl+M")
				});
			ToINI = new RoutedUICommand("To INI", "ToINI", typeof(FormatCommands),
				new InputGestureCollection
				{
					new KeyGesture(Key.I, ModifierKeys.Control, "Ctrl+Shift+I")
				});
			FromINI = new RoutedUICommand("From INI", "FromINI", typeof(FormatCommands),
				new InputGestureCollection
				{
					new KeyGesture(Key.I, ModifierKeys.Control | ModifierKeys.Shift, "Ctrl+Shift+I")
				});
        }

        public static RoutedUICommand Exit { get; private set; }
        public static RoutedUICommand Exit2 { get; private set; }
        public static RoutedUICommand About { get; private set; }
        public static RoutedUICommand Sync { get; private set; }
        public static RoutedUICommand Add { get; private set; }
        public static RoutedUICommand Delete { get; private set; }
        public static RoutedUICommand ExitAbout { get; private set; }
        public static RoutedUICommand FindAndReplace { get; private set; }
		public static RoutedUICommand Import { get; private set; }
		public static RoutedUICommand Export { get; private set; }
		public static RoutedUICommand ToXML { get; private set; }
		public static RoutedUICommand FromXML { get; private set; }
		public static RoutedUICommand ToINI { get; private set; }
		public static RoutedUICommand FromINI { get; private set; }
    }
}
