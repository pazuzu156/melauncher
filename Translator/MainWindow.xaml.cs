﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MEL.Locale;
using MEL.Locale.Data;
using System.Collections.Specialized;
using System.Collections;
using WinForms = System.Windows.Forms;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Globalization;
using System.Data;
using System.Collections.ObjectModel;
using System.Net;
using System.ComponentModel;
using System.Web.Script.Serialization;

namespace Translator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Enums
        public class Item
        {
            public string Name { get; set; }
            public string Value { get; set; }
            public string Type { get; set; }
        }
        #endregion

        #region Variables
        private bool isEdited = false, sync = false;
        Strings _strings;
        Titles _titles;
        Buttons _buttons;
        RadioButtons _rButtons;
        CheckBoxes _cBoxes;
        string _lang = "en-US";
        StringDictionary languages;
        bool packLoaded = false;
        ObservableCollection<Item> lItems;
        #endregion

        #region Constructor
        public MainWindow()
        {
            InitializeComponent();
            FillLanguageComboBox();
            DataContext = this;
        }
        #endregion

        #region Main Component Methods
        private void lvTest_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
			var item = ((FrameworkElement)e.OriginalSource).DataContext as Item;
            if(item != null)
            {
				
				Editor ed = new Editor(item.Name, item.Value);
				ed.ShowDialog();
				if (ed.Changed)
				{
					item.Name = ed.Name;
					item.Value = ed.Value;
					lvTest.Items.Refresh();
					isEdited = true;
					bSaveButton.IsEnabled = true;
				}
            }
        }

        private void NewBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            bool close = false;
            if (isEdited)
            {
                if (MessageBox.Show("You have made changes to your translation. Are you sure you want to close the current translation? You will loose all changes made.", "Close Translation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    close = true;
                }
            }

            if (close && isEdited || !isEdited)
            {
                if (MessageBox.Show("A new language translation is being created. In order to translate MELauncher, the English template will be loaded, by which you can edit", "New Translation", MessageBoxButton.OK, MessageBoxImage.Information) == MessageBoxResult.OK)
                {
                    LoadLanguagePack();
                }
            }
        }

        private void OpenBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            bool close = false;
            if (isEdited)
            {
                if (MessageBox.Show("You have made changes to your translation. Are you sure you want to close the current translation? You will loose all changes made.", "Close Translation", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    close = true;
                }
            }

            if (close && isEdited || !isEdited)
            {
                WinForms.OpenFileDialog ofd = new WinForms.OpenFileDialog();
                ofd.Filter = "Language Translation Pack|*.lang";
                ofd.InitialDirectory = Locale.LocaleDir;
                try
                {
                    if (ofd.ShowDialog() == WinForms.DialogResult.OK)
                    {
						string file = ofd.FileName;
						int start = file.IndexOf("locale\\");
						int end = file.Length;
						string ld = file.Substring(start, end - start);
						string f = ld.Substring(ld.IndexOf("\\") + 1, ld.Length - ld.IndexOf("\\") - 1);
						_lang = f.Substring(0, f.IndexOf("."));
                        LoadLanguagePack();
                    }
                }
                catch(Exception)
                {
                    MessageBox.Show("Error: Only locales within the locale directory shipped with the translation tool can be opened!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (isEdited)
            {
                if (MessageBox.Show("You have made changes to your translation. Are you sure you want to quit? You will loose all changes made.", "Quit Translation Tool", MessageBoxButton.YesNo, MessageBoxImage.Question) != MessageBoxResult.Yes)
                {
                    e.Cancel = true;
                }
            }
        }

        private void AboutBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            new About().ShowDialog();
        }

        private void bSaveButton_Click(object sender, RoutedEventArgs e)
        {
            Save();
        }

        private void AddBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            AddItem();
        }

        private void DeleteBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            DeleteItem();
        }

        private void CommandBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            e.CanExecute = true;
        }

        private void ExitBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.Close();
        }

        private void SaveBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            Save();
        }

        private void SaveBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (isEdited)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        private void SyncBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            StatusCheck sc = new StatusCheck("http://cdn.kalebklein.com/mel/testcl.txt");
            if(sc.IsConnected)
            {
                sync = true;
                bool sas = false;
                if (isEdited)
                {
                    sas = true;
                    Save();
                }

                try
                {
                    string file = _lang + "_" + DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".lang";
                    Uri url = new Uri(@"http://cdn.kalebklein.com/mel/synced_language_packs/sync.php");
					File.Copy(Locale.LocaleDir + "\\" + _lang + ".lang", Locale.LocaleDir + "\\" + file);
                    using (WebClient client = new WebClient())
                    {
                        client.Headers.Add("Content-Type", "binary/octet-stream");
						client.UploadFile(url, Locale.LocaleDir + "\\" + file);
                        var stc = client.ResponseHeaders["statusCode"];
                        if (sas)
                            MessageBox.Show("Language pack has been synced and your settings have been saved.");
                        else
                            MessageBox.Show("Language pack has been synced.");
						File.Delete(Locale.LocaleDir + "\\" + file);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex);
                }
            }
            else
            {
                MessageBox.Show("You need to be connected to the internet in order to use the sync option!", "Connection Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SyncBinding_CanExecute(object sender, CanExecuteRoutedEventArgs e)
        {
            if (packLoaded)
                e.CanExecute = true;
            else
                e.CanExecute = false;
        }

        private void cmbLanguage_DropDownClosed(object sender, EventArgs e)
        {
            SetNewLang();
            isEdited = true;
        }

        private void FindBinding_Execute(object sender, ExecutedRoutedEventArgs e)
        {
            Find f = new Find();
            f.ShowDialog();
            if (f.CanFind)
            {
                var itemName = f.Name;
               var i = lItems.ToList<Item>();
                for (int ii = 0; ii < i.Count; ii++)
                {
                    var iii = i[ii];
                    if (iii.Name == itemName)
                    {
                        ListBoxItem lbi = (ListBoxItem)lvTest.ItemContainerGenerator.ContainerFromIndex(ii);
                        lbi.IsSelected = true;
                        lbi.Focus();
                    }
                }
            }
        }

        private void FindReplaceBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            FindReplace fr = new FindReplace();
            fr.ShowDialog();
            if (fr.CanFind)
            {
                var itemName = fr.Name;
                var replace = fr.Replace;
                var i = lItems.ToList<Item>();
                int index = 0;
                for (int ii = 0; ii < i.Count; ii++)
                {
                    var iii = i[ii];
                    if (iii.Name == itemName)
                    {
                        index = ii;
                        ListBoxItem lbi = (ListBoxItem)lvTest.ItemContainerGenerator.ContainerFromIndex(ii);
                        lbi.IsSelected = true;
                        var tmp = lvTest.SelectedItems.Cast<Item>().ToList();
                        tmp[0].Name = replace;
                        lvTest.Items.Refresh();
                        lbi.Focus();
                        FindBinding_Execute(sender, e);
                        break;
                    }

                }
            }
        }
        #endregion

        #region Event Called Methods
        private void FillLanguageComboBox()
        {
            var inclangs = new List<string>();
            languages = new StringDictionary();
            foreach (CultureInfo ci in CultureInfo.GetCultures(CultureTypes.SpecificCultures))
            {
                inclangs.Add(ci.Name + " - " + ci.DisplayName);
                languages.Add(ci.Name, ci.DisplayName);
            }

            cmbLanguage.ItemsSource = inclangs;
        }
        
        private void AddItem()
        {
            NewItem ni = new NewItem();
            ni.ShowDialog();
            string type = null;

            switch (ni.ItemType)
            {
                case "Title":
                    type = "Titles";
                    break;
                case "String":
                    type = "Strings";
                    break;
                case "Button":
                    type = "Buttons";
                    break;
                case "Radio Button":
                    type = "Radio Buttons";
                    break;
                case "Check Box":
                    type = "Check Boxes";
                    break;
            }

            if(ni.Changed)
            {
                lItems.Add(new Item() { Name = ni.ItemName, Value = "", Type = type });
                lvTest.Items.Refresh();
                bSaveButton.IsEnabled = true;
                isEdited = true;
            }
        }

        private void DeleteItem()
        {
            bool confirm = false;
            if (lvTest.SelectedItems.Count > 1)
            {
                if (MessageBox.Show("You have multiple items selected to remove. Are you sure you want to remove all of these items?", "Delete Items", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    confirm = true;
                }
            }
            else
            {
                confirm = true;
            }

            if(confirm)
            {
                var tmp = lvTest.SelectedItems.Cast<Item>().ToList();
                foreach (var item in tmp)
                {
                    lItems.Remove(item);
                }

                if (lItems.Count > 0)
                {
                    bSaveButton.IsEnabled = true;
                    isEdited = true;
                }
                else
                {
                    bSaveButton.IsEnabled = false;
                    isEdited = false;
                }
            }
        }

        private void Save()
        {
            if (packLoaded)
            {
                if(lItems.Count > 0)
                {
                    LocaleSaveFile lsf = new LocaleSaveFile();
                    SetNewLang();
					string location = Locale.LocaleDir + "\\" + _lang + ".lang";
                    foreach (var item in lvTest.Items)
                    {
						if(!File.Exists(location))
						{
							string desktopDir = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\MELauncher\\locale";
							
							try
							{
								// This fails if the directory is write protected
								// Catch will quell the issue
								if (!Directory.Exists("locale"))
									Directory.CreateDirectory("locale");

								location = "locale\\" + _lang + ".lang";
							}
							catch
							{
								if (!Directory.Exists(desktopDir))
									Directory.CreateDirectory(desktopDir);

								location = desktopDir + "\\" + _lang + ".lang";
							}
						}

						FileStream file = File.Open(location, FileMode.Create, FileAccess.Write);
						StreamWriter writer = new StreamWriter(file);
						string json = new JavaScriptSerializer().Serialize(new
						{
							language = _lang,
							content = lvTest.ItemsSource
						});
						//string json = Cereal.Serialize(_lang, lvTest.ItemsSource);
						//writer.Write(Locale.ToBase64(json));
						writer.Write(json);
						writer.Flush();
						writer.Close();
						file.Close();
                    }
                    isEdited = false;
                    if (!sync)
                        MessageBox.Show("Your changes have been saved!\r\n\r\nYou can find the file at: " + location);
                    sync = false;
                    bSaveButton.IsEnabled = false;
                }
                else
                {
                    MessageBox.Show("Your translation list is empty!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("There are no open language packs! You cannot save anything right now!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SetNewLang()
        {
            string lt = cmbLanguage.Text;
            string culture = lt.Substring(0, lt.IndexOf(" "));
            _lang = culture;
        }

        private void LoadLanguagePack()
        {
            lItems = new ObservableCollection<Item>();
            LoadLocaleHandlers();

			foreach(var itemCol in _titles.GetAllTitles())
			{
				lItems.Add(new Item() { Name = itemCol.Key, Value = itemCol.Value.Value, Type = itemCol.Value.Type });
			}

			foreach (var itemCol in _strings.GetAllStrings())
			{
				lItems.Add(new Item() { Name = itemCol.Key, Value = itemCol.Value.Value, Type = itemCol.Value.Type });
			}

			foreach (var itemCol in _buttons.GetAllButtons())
			{
				lItems.Add(new Item() { Name = itemCol.Key, Value = itemCol.Value.Value, Type = itemCol.Value.Type });
			}

			foreach (var itemCol in _rButtons.GetAllButtons())
			{
				lItems.Add(new Item() { Name = itemCol.Key, Value = itemCol.Value.Value, Type = itemCol.Value.Type });
			}

			foreach (var itemCol in _cBoxes.GetAllButtons())
			{
				lItems.Add(new Item() { Name = itemCol.Key, Value = itemCol.Value.Value, Type = itemCol.Value.Type });
			}

			lvTest.ItemsSource = lItems;

			CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvTest.ItemsSource);
			PropertyGroupDescription gd = new PropertyGroupDescription("Type");
			view.GroupDescriptions.Add(gd);

            cmbLanguage.IsEnabled = true;
            cmbLanguage.SelectedValue = _lang + " - " + languages[_lang];
            miLangItems.IsEnabled = true;
            packLoaded = true;
        }

        private void LoadLocaleHandlers()
        {
            _strings = Strings.Load(_lang);
            _titles = Titles.Load(_lang);
            _buttons = Buttons.Load(_lang);
            _rButtons = RadioButtons.Load(_lang);
            _cBoxes = CheckBoxes.Load(_lang);
        }

        private void SelectItem(int index)
        {
            System.Threading.Thread.Sleep(200);
        }
        #endregion

		private void ImportXML_Executed(object sender, ExecutedRoutedEventArgs e)
		{

		}

		private void ExportOptions_CanExecute(object sender, CanExecuteRoutedEventArgs e)
		{
			if (packLoaded)
				e.CanExecute = true;
		}

		private void ExportXML_Executed(object sender, ExecutedRoutedEventArgs e)
		{
			//ExportXML.Export(lItems, _lang);
			var items = lvTest.ItemsSource;
			string json = new JavaScriptSerializer().Serialize(items);
			FileStream file = File.Open("language.json", FileMode.OpenOrCreate, FileAccess.Write);
			StreamWriter writer = new StreamWriter(file);
			writer.Write(json);
			writer.Flush();
			writer.Close();
			file.Close();
		}

		private void FromINI_Executed(object sender, ExecutedRoutedEventArgs e)
		{

		}

		private void ToINI_Executed(object sender, ExecutedRoutedEventArgs e)
		{

		}
    }
}
