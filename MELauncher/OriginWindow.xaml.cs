﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WinForms = System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Net;
using System.Web.Script.Serialization;
using MahApps.Metro.Controls.Dialogs;
using MahApps.Metro.Controls;

using MEL.Settings;
using MEL.Locale;
using MEL.Logger;

namespace MELauncher
{
    /// <summary>
    /// Interaction logic for OriginWindow.xaml
    /// </summary>
    public partial class OriginWindow : MetroWindow
    {
        #region Variables
        Settings settings;
        bool isTexModEnabled = false,
            hasOrigin = false,
            hasME = false,
            hasME2 = false,
            hasME3 = false,
            hasMEA = false,
            autoDetect = false,
            tempAutoClose = false,
            isResetting = false;
        Titles titles;
        Strings strings;
        Buttons buttons;
        RadioButtons radioButtons;
        private volatile bool threadRunning = false;
        private volatile bool closeWindow = false;
        private Task listenTask;
        GameSelected gamesel;
        LoggerWindow logger;
		MetroDialogSettings mds = new MetroDialogSettings() { ColorScheme = MetroDialogColorScheme.Accented };
        #endregion

        #region Delegates
        private delegate void d_KillThreads();
        private delegate void d_WriteLog(string write);
        #endregion

        #region Enums
        private enum GameSelected
        {
            MESelected,
            ME2Selected,
            ME3Selected,
			MEASelected,
            NoneSelected
        }
        #endregion

        #region Constructor
        public OriginWindow()
        {
            settings = Settings.getInstance();
            settings.LoadSettings();

            gamesel = GameSelected.NoneSelected;

            InitializeComponent();

//#if DEBUG
//			MessageBox.Show("Attatch debugger");
//#endif

            this.Activate();

            SetLocale();

			string theme = (Settings.Theme == Settings.ThemeType.Light) ? "Light" : "Dark";

			App.Current.Resources.MergedDictionaries.Add(
				new ResourceDictionary { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme." + theme + ".xaml", UriKind.Absolute) });

            if (Settings.AOT)
                this.Topmost = true;
            else
                this.Topmost = false;

            if(!Settings.OriginLocation.Equals(""))
            {
                tbOrigin.Text = Settings.OriginLocation;
                tbME.Text = Settings.MELocation;
                tbME2.Text = Settings.ME2Location;
                tbME3.Text = Settings.ME3Location;
				tbMEA.Text = Settings.MEALocation;

                checkLocations();
            }

			//Task.Factory.StartNew(() => { new UpdateChecker(Settings.Version, true); });

			new UpdateChecker(Settings.Version, true);

            runCrashHandler();

            DataContext = this; // Allows the use of the F1 key for help window

			if (App.updated)
				AlertUpdated();

			if (App.LaunchFromShortcut)
				LaunchFromShortcut();
        }

        private void CheckForMessages()
        {
            StatusCheck sc = new StatusCheck("http://cdn.kalebklein.com/mel/testcl.txt");
            if (sc.IsConnected)
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://cdn.kalebklein.com/mel/messages/get");
                request.ContentType = "application/json";
                request.Method = "POST";

                using (var writer = new StreamWriter(request.GetRequestStream()))
                {
                    string json = new JavaScriptSerializer().Serialize(new
                    {
                        request = "GetMessages"
                    });
                    writer.Write(json);
                    writer.Flush();

                    var response = (HttpWebResponse)request.GetResponse();
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        var res = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(reader.ReadToEnd());
                        
                    }
                }
            }
        }
        #endregion

        #region Main Component Methods
        private void bBrowseOrigin_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Origin browsing button clicked");
            if (bBrowseOrigin.Content.Equals(buttons.GetButton("open")))
            {
                ProcessStartInfo psi = new ProcessStartInfo();
                psi.FileName = Environment.GetFolderPath(Environment.SpecialFolder.Windows) + @"\explorer.exe";
                psi.Arguments = Settings.OriginLocation;
                Process.Start(psi);
                WriteLog("Opening Origin location in Windows Explorer");
            }
            else
            {
                WriteLog("Opening folder explorer dialog");
                string path = BrowseForLocation();
                if (path != null)
                    tbOrigin.Text = path;
            }
        }

        private void bBrowseME_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Mass Effect browsing button clicked");
            if (bBrowseME.Content.Equals(buttons.GetButton("open")))
            {
                WriteLog("Opening selection window");
                new SelectOpenWindow(Settings.MELocation, "origin", logger).ShowDialog();
            }
            else
            {
                WriteLog("Opening folder explorer dialog");
                string path = BrowseForLocation();
                if (path != null)
                    tbME.Text = path;
            }
        }

        private void bBrowseME2_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Mass Effect 2 browsing button clicked");
            if (bBrowseME2.Content.Equals(buttons.GetButton("open")))
            {
                WriteLog("Opening selection window");
                new SelectOpenWindow(Settings.ME2Location, "origin", logger).ShowDialog();
            }
            else
            {
                WriteLog("Opening folder explorer dialog");
                string path = BrowseForLocation();
                if (path != null)
                    tbME2.Text = path;
            }
        }

        private void bBrowseME3_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Mass Effect 3 browsing button clicked");
            if (bBrowseME3.Content.Equals(buttons.GetButton("open")))
            {
                WriteLog("Opening selection window");
                new SelectOpenWindow(Settings.ME3Location, "origin", logger).ShowDialog();
            }
            else
            {
                WriteLog("Opening folder explorer dialog");
                string path = BrowseForLocation();
                if (path != null)
                    tbME3.Text = path;
            }
        }

		private void bBrowseMEA_Click(object sender, RoutedEventArgs e)
		{
			WriteLog("Mass Effect Andromeda browsing button clicked");
			if (bBrowseMEA.Content.Equals(buttons.GetButton("open")))
			{
				WriteLog("Opening selection window");
				new SelectOpenWindow(Settings.MEALocation, "origin", logger).ShowDialog();
			}
			else
			{
				WriteLog("Opening folder explorer dialog");
				string path = BrowseForLocation();
				if (path != null)
					tbMEA.Text = path;
			}
		}

        private void bAutoDetect_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Auto detect button clicked");
            autoDetect = true;
            findLocations();
        }

        private void bClose_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Close button clicked");
            this.Close();
        }

        private void bCheckLocations_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Check locations button clicked");
            checkLocations();
        }

        private void bSettings_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Settings button clicked");
            new SettingsWindow(logger).ShowDialog();
        }

        private void bEnable_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Enable TexMod button clicked");
            if (!isTexModEnabled)
            {
                WriteLog("Starting TexMod enabling process");
                enable();
            }
            else
            {
                WriteLog("Disabling TexMod");
                if (listenTask != null && threadRunning == true)
                {
                    closeWindow = false;
                    threadRunning = false;
                }
            }
        }

        private async void bOrOpen_Click(object sender, RoutedEventArgs e)
        {
			WriteLog("Game launch button clicked");
			string game = "";
			if ((bool)rbME.IsChecked)
				game = "me";
			else if ((bool)rbME2.IsChecked)
				game = "me2";
			else if ((bool)rbME3.IsChecked)
				game = "me3";
			else if ((bool)rbMEA.IsChecked)
				game = "mea";
			else
				await this.ShowMessageAsync(strings.GetString("nogamesel_title"), strings.GetString("nogamesel_notm"), MessageDialogStyle.Affirmative, mds);
			
			if(game.Equals("me") || game.Equals("me2") || game.Equals("me3") || game.Equals("mea"))
			{
				var gameID = Settings.OriginGameIDs;
				//new SelectRunWindow(game, gameID[game], "origin", logger).ShowDialog();
				var win = new SelectRunWindow(game, gameID[game], "origin", logger);
				if (isTexModEnabled)
				{
					win.ShowDialog();
				}
				else
				{
					win.RunGame();
				}
			}
        }

        private void BugReporterCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            WriteLog("Launching bug reporting tool");
            var w = new BugReporter(logger);
            w.ShowDialog();
        }

        private void OriginWindowName_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            WriteLog("Starting application shutdown procedure");
            if (isTexModEnabled)
            {
                WriteLog("TexMod is still enabled, disabling");
                if (
                    File.Exists(Settings.MELocation + @"\Binaries\mel.backup") ||
                    File.Exists(Settings.ME2Location + @"\Binaries\mel.backup") ||
                    File.Exists(Settings.ME3Location + @"\Binaries\Win32\mel.backup")
                  )
                {
                    MessageBox.Show(strings.GetString("texmod_still_on"), strings.GetString("texmod_still_on_title"), MessageBoxButton.OK, MessageBoxImage.Warning);
                    threadRunning = false;
                    stopListening();
                }
            }
            WriteLog("Terminating application");

            if (this.isResetting)
            {
                this.isResetting = false;
                new OriginWindow().Show();
            }
            else
            {
                App.Current.Shutdown();
            }
        }

        private async void HelpCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            WriteLog("Preparing to launch compiled help file");
			bool error = false;
            try
            {
                WriteLog("Launching compiled help file");
                Process.Start("MELauncherHelp.chm");
            }
            catch (Exception ex)
            {
                WriteLog("Error opening compiled help file. Error: " + ex.Message);
                //MessageBox.Show(strings.GetString("no_help"), strings.GetString("mb_error_title"), MessageBoxButton.OK, MessageBoxImage.Error);
				error = true;
            }
			if(error)
				await this.ShowMessageAsync(strings.GetString("mb_error_title"), strings.GetString("no_help"), MessageDialogStyle.Affirmative, mds);
        }

        private void LoggerCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            logger = new LoggerWindow("origin");
            logger.Closing += logger_Closing;
            logger.Show();
        }

        private void ReloadCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.isResetting = true;
            this.Close();
        }
        #endregion

        #region Event called methods
        private void WriteLog(string write)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(new d_WriteLog(WriteLog), write);
            else
            {
                if (logger != null)
                    logger.WriteToLog(write);
            }
        }

        private void logger_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            WriteLog("Terminating logger");
            logger.WriteToLogFile();
            logger = null;
        }

        private String BrowseForLocation()
        {
            WinForms.FolderBrowserDialog fbd = new WinForms.FolderBrowserDialog();
            WinForms.DialogResult result = fbd.ShowDialog();
            if (result == WinForms.DialogResult.OK)
            {
                WriteLog("Directory selected: " + fbd.SelectedPath);
                return fbd.SelectedPath;
            }
            WriteLog("User canceled browsing action");
            return null;
        }

        private async void checkLocations()
        {
            WriteLog("Checking supplied locations");
            string ol = tbOrigin.Text;
            string ml = tbME.Text;
            string m2l = tbME2.Text;
            string m3l = tbME3.Text;
			string mal = tbMEA.Text;

            WriteLog("Initializing location detector");

            WriteLog("Checking if supplied Origin location exists");
            hasOrigin = LocationDetector.CheckLocation(ol + @"\Origin.exe", tbOrigin, bBrowseOrigin, lblOriginCheck);
            if (!hasOrigin)
                WriteLog("Origin was not found");

            WriteLog("Checking if supplied Mass Effect location exists");
            hasME = LocationDetector.CheckLocation(ml + @"\Binaries\MassEffect.exe", tbME, bBrowseME, lblMECheck);
            if (!hasME)
                WriteLog("Mass Effect was not found");

            WriteLog("Checking if supplied Mass Effect 2 location exists");
            hasME2 = LocationDetector.CheckLocation(m2l + @"\Binaries\ME2Game.exe", tbME2, bBrowseME2, lblME2Check);
            if (!hasME2)
                WriteLog("Mass Effect 2 was not found");

            WriteLog("Checking if supplied Mass Effect 3 location exists");
            hasME3 = LocationDetector.CheckLocation(m3l + @"\Binaries\Win32\MassEffect3.exe", tbME3, bBrowseME3, lblME3Check);
            if (!hasME3)
                WriteLog("Mass Effect 3 was not found");

			WriteLog("Checking if supplied Mass Effect Andromeda location exists");
			hasMEA = LocationDetector.CheckLocation(mal + @"\MassEffectAndromeda.exe", tbMEA, bBrowseMEA, lblMEACheck);
			if (!hasMEA)
				WriteLog("Mass Effect Andromeda was not found");

            WriteLog("Detection complete");
            if(hasOrigin && (hasME3 || hasME2 || hasME || hasMEA))
            {
                bEnable.Visibility = System.Windows.Visibility.Visible;
                bOrOpen.Visibility = System.Windows.Visibility.Visible;
                gbGames.Visibility = System.Windows.Visibility.Visible;

                string message = null;
                if (hasME2 && hasME3 && hasME && hasMEA)
                {
                    WriteLog("All supplied locations were found");
                    message = strings.GetString("mb_ad_o_all");
                }
                else if (hasME2 && hasME3)
                {
                    WriteLog("Locations for Origin, Mass Effect 2, and 3 were found");
                    message = strings.GetString("mb_ad_o_23");
                }
                else if (hasME && hasME3)
                {
                    WriteLog("Locations for Origin, Mass Effect, and 3 were found");
                    message = strings.GetString("mb_ad_o_13");
                }
                else if (hasME && hasME2)
                {
                    WriteLog("Locations for Origin, Mass Effect, and 2 were found");
                    message = strings.GetString("mb_ad_o_12");
                }
				else if (hasME2 && hasME3 && hasMEA)
				{
					WriteLog("Locations for Origin, Mass Effect 2, 3, and Andromeda were found");
					message = strings.GetString("mb_ad_o_23a");
				}
				else if (hasME && hasME3 && hasMEA)
				{
					WriteLog("Locations for Origin, Mass Effect, 3, and Andromeda were found");
					message = strings.GetString("mb_ad_o_13a");
				}
				else if (hasME && hasME2 && hasMEA)
				{
					WriteLog("Locations for Origin, Mass Effect, 2, and Andromeda were found");
					message = strings.GetString("mb_ad_o_12a");
				}
				else if (hasMEA)
				{
					WriteLog("Locations for Origin ans Mass Effect Andromeda were found");
					message = strings.GetString("mb_ad_o_a");
				}
                else if (hasME3)
                {
                    WriteLog("Locations for Origin ans Mass Effect 3 were found");
                    message = strings.GetString("mb_ad_o_3");
                }
                else if (hasME2)
                {
                    WriteLog("Locations for Origin ans Mass Effect 2 were found");
                    message = strings.GetString("mb_ad_o_2");
                }
                else if (hasME)
                {
                    WriteLog("Locations for Origin ans Mass Effect were found");
                    message = strings.GetString("mb_ad_o_1");
                }

                WriteLog("Writing new locations to application settings");
                settings.SaveSettings(Settings.AppID,
                    Settings.Version,
                    Settings.SteamLocation,
                    ol, ml, m2l, m3l, mal,
                    Settings.DRM,
                    Settings.Theme,
                    Settings.Language,
                    Settings.CloseOnRun,
                    Settings.AOT,
                    Settings.StartUp);

				if (autoDetect)
					await this.ShowMessageAsync(strings.GetString("mb_ad_title"), message, MessageDialogStyle.Affirmative, mds);
            }
        }

        private async void findLocations()
        {
            string ol;
            string ml;
            string m2l;
            string m3l;
			string mal;

            WriteLog("Initializing location detector");
            LocationDetector old = new LocationDetector();
            LocationDetector mld = new LocationDetector();
            LocationDetector m2ld = new LocationDetector();
            LocationDetector m3ld = new LocationDetector();
			LocationDetector mald = new LocationDetector();

            LocationDetector.SetText(lblOriginCheck, strings.GetString("searching"), Brushes.Brown);
            LocationDetector.SetText(lblMECheck, strings.GetString("searching"), Brushes.Brown);
            LocationDetector.SetText(lblME2Check, strings.GetString("searching"), Brushes.Brown);
            LocationDetector.SetText(lblME3Check, strings.GetString("searching"), Brushes.Brown);
			LocationDetector.SetText(lblMEACheck, strings.GetString("searching"), Brushes.Brown);

            WriteLog("Comparing supplied locations against the registry");

            WriteLog("Sniffing registry for Origin's location");
            old.SniffForLocation(@"SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\Origin",
                old.ItemsList(new String[] { "location" }, new String[] { "InstallLocation" }), "origin");
            ol = old.getLocation();

            WriteLog("Sniffing registry for Mass Effect's location");
            mld.SniffForLocation(@"SOFTWARE\Wow6432Node\BioWare\Mass Effect",
                mld.ItemsList(new String[] { "location", "steam" }, new String[] { "Path", "flavour" }), "origin");
            ml = mld.getLocation();

            WriteLog("Sniffing registry for Mass Effect 2's location");
            m2ld.SniffForLocation(@"SOFTWARE\Wow6432Node\BioWare\Mass Effect 2",
                m2ld.ItemsList(new String[] { "location", "steam" }, new String[] { "Install Dir", "flavour" }), "origin");
            m2l = m2ld.getLocation();

            WriteLog("Sniffing registry for Mass Effect 3's location");
            m3ld.SniffForLocation(@"SOFTWARE\Wow6432Node\BioWare\Mass Effect 3",
                m3ld.ItemsList(new String[] { "location" }, new String[] { "Install Dir" }), "origin");
            m3l = m3ld.getLocation();

			WriteLog("Sniffing registry for Mass Effect Andromeda's location");
			mald.SniffForLocation(@"SOFTWARE\Wow6432Node\BioWare\Mass Effect Andromeda",
				mald.ItemsList(new String[] { "location" }, new String[] { "Install Dir" }), "origin");
			mal = mald.getLocation();

            if (ol != null)
            {
                WriteLog("Origin found in Windows registry");
                tbOrigin.Text = ol;
            }
            else
            {
                WriteLog("Origin was not found in Windows registry!");
				await this.ShowMessageAsync(strings.GetString("mb_check_locations"), strings.GetString("mb_no_origin"), MessageDialogStyle.Affirmative, mds);
                LocationDetector.SetText(lblOriginCheck, strings.GetString("nfound"), Brushes.DarkRed);
            }

            if (ml != null)
            {
                if(!mld.IsSteamFlavor())
                {
                    WriteLog("Mass Effect was found in the Windows registry");
                    tbME.Text = ml.TrimEnd('\\');
                }
                else
                {
                    WriteLog("Mass Effect was found in the Windows registry, however it's the Steam version. Disregarding registry entry");
					await this.ShowMessageAsync(strings.GetString("mb_check_locations"), strings.GetString("mb_no_me_origin"), MessageDialogStyle.Affirmative, mds);
                    LocationDetector.SetText(lblMECheck, strings.GetString("nfound"), Brushes.Red);
                }
            }
            else
            {
                WriteLog("Mass Effect was not found in the Windows registry!");
				await this.ShowMessageAsync(strings.GetString("mb_check_locations"), strings.GetString("mb_no_me"), MessageDialogStyle.Affirmative, mds);
                LocationDetector.SetText(lblMECheck, strings.GetString("nfound"), Brushes.Red);
            }

            if (m2l != null)
            {
                if(!m2ld.IsSteamFlavor())
                {
                    WriteLog("Mass Effect 2 was found in the Windows registry");
                    tbME2.Text = m2l.TrimEnd('\\');
                }
                else
                {
                    WriteLog("Mass Effect 2 was found in the Windows registry, however it's the Steam version. Disregarding registry entry");
					await this.ShowMessageAsync(strings.GetString("mb_check_locations"), strings.GetString("mb_no_me2_origin"), MessageDialogStyle.Affirmative, mds);
                    LocationDetector.SetText(lblME2Check, strings.GetString("nfound"), Brushes.Red);
                }
            }
            else
            {
                WriteLog("Mass Effect 2 was not found in the Windows registry!");
				await this.ShowMessageAsync(strings.GetString("mb_check_locations"), strings.GetString("mb_no_me2"), MessageDialogStyle.Affirmative, mds);
                LocationDetector.SetText(lblME2Check, strings.GetString("nfound"), Brushes.Red);
            }

            if (m3l != null)
            {
                WriteLog("Mass Effect 3 was found in the Windows registry");
                tbME3.Text = m3l.TrimEnd('\\');
            }
            else
            {
                WriteLog("Mass Effect 3 was not found in the Windows registry!");
				await this.ShowMessageAsync(strings.GetString("mb_check_locations"), strings.GetString("mb_no_me3"), MessageDialogStyle.Affirmative, mds);
                LocationDetector.SetText(lblME3Check, strings.GetString("nfound"), Brushes.Red);
            }

			if (mal != null)
			{
				WriteLog("Mass Effect Andromeda was found in the Windows registry");
				tbMEA.Text = mal.TrimEnd('\\');
			}
			else
			{
				WriteLog("Mass Effect Andromeda was not found in the Windows registry");
				await this.ShowMessageAsync(strings.GetString("mb_check_locations"), strings.GetString("mb_no_mea"), MessageDialogStyle.Affirmative, mds);
				LocationDetector.SetText(lblMEACheck, strings.GetString("nfound"), Brushes.Red);
			}

            WriteLog("Beginning location checking of locations obtained from the Windows registry");
            checkLocations();
        }

        private void runCrashHandler()
        {
            WriteLog("Checking if the application exited successfully in the previous session");
            if(File.Exists(Settings.CrashHandlerFile))
            {
                WriteLog("Application crash handler detected an unexpected exit");
                if (MessageBox.Show(strings.GetString("crashHandlerErrorMessage"), strings.GetString("mb_error_title"), MessageBoxButton.OK, MessageBoxImage.Error) == MessageBoxResult.OK)
                {
                    WriteLog("Reverting changes from previous session according the application crash handler");
                    disable();
                    File.Delete(Settings.CrashHandlerFile);
                }
            }
        }

        private void StartUp(bool check)
        {
            RegistryKey startupKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
            if (check)
            {
                WriteLog("Writing startup key to registry");
                startupKey.SetValue("MELauncher", Environment.CurrentDirectory + @"\MELauncher.exe");
            }
            else
            {
                WriteLog("Removing startup key from registry");
                startupKey.DeleteValue("MELauncher", false);
            }
        }
        #endregion

        #region Enable / Disable Methods
        private async void enable()
        {
            WriteLog("Initializing TexMod enable methods");
			GameSelected gs;
			if (!App.LaunchFromShortcut)
				gs = GameSelected.NoneSelected;
			else
				gs = gamesel;

            if ((bool)rbME.IsChecked)
            {
                WriteLog("Mass Effect is currently selected");
                gs = GameSelected.MESelected;
            }
            else if ((bool)rbME2.IsChecked)
            {
                WriteLog("Mass Effect 2 is currently selected");
                gs = GameSelected.ME2Selected;
            }
            else if ((bool)rbME3.IsChecked)
            {
                WriteLog("Mass Effect 3 is currently selected");
                gs = GameSelected.ME3Selected;
            }
			else if ((bool)rbMEA.IsChecked)
			{
				WriteLog("Mass Effect Andromeda is currently selected. Andromeda also doesn't support TexMod yet, display message to user");
				gs = GameSelected.MEASelected;
			}

            if(gamesel == GameSelected.NoneSelected && File.Exists(Settings.CrashHandlerFile))
            {
                WriteLog("Error: TexMod is already enabled");
				//MessageBox.Show(strings.GetString("tmAlreadyEnabled"), strings.GetString("mb_error_title"), MessageBoxButton.OK, MessageBoxImage.Error);
				try
				{
					await this.ShowMessageAsync(strings.GetString("mb_error_title"), strings.GetString("tmAlreadyEnabled"), MessageDialogStyle.Affirmative, mds);
				}
				catch
				{
					MessageBox.Show(strings.GetString("tmAlreadyEnabled"), strings.GetString("mb_error_title"), MessageBoxButton.OK, MessageBoxImage.Error);
				}
            }
            else
            {
                gamesel = gs;
                bool enabled = false;

                switch (gs)
                {
                    case GameSelected.MESelected:
                        if (hasME)
                        {
                            WriteLog("Checking if TexMod is installed for Mass Effect");
                            if (File.Exists(Settings.MELocation + @"\Binaries\TexMod.exe"))
                            {
                                WriteLog("TexMod was found. Initializing replacements");

                                WriteLog("Replacing MassEffect.exe with TexMod, backing up game executable");
                                File.Move(Settings.MELocation + @"\Binaries\MassEffect.exe", Settings.MELocation + @"\Binaries\mel.backup");
                                File.Move(Settings.MELocation + @"\Binaries\TexMod.exe", Settings.MELocation + @"\Binaries\MassEffect.exe");

                                // May not always happen, but leave old renaming open as well for those who may still have to open MassEffectLauncher
                                if(File.Exists(Settings.MELocation + @"\MassEffectLauncher.exe"))
								{
									WriteLog("Replacing MassEffectLauncher.exe with TexMod for compatibility reasons, backing up executable");
									File.Move(Settings.MELocation + @"\MassEffectLauncher.exe", Settings.MELocation + @"\mell.backup");
									File.Copy(Settings.MELocation + @"\Binaries\MassEffect.exe", Settings.MELocation + @"\MassEffectLauncher.exe");
								}

                                WriteLog("Initializing game launch thread");
                                ListenForTexModLaunch("me");
                                enabled = true;
                            }
                            else
                            {
                                WriteLog("TexMod was not found, launching TexMod downloader window");
                                new TexModDownloadWindow(Settings.OriginLocation, Settings.MELocation + @"\Binaries\", logger).ShowDialog();
                            }
                        }
                        else
                        {
                            WriteLog("Mass Effect is not currently installed onto the system");
							//MessageBox.Show(strings.GetString("ni_me"), "MELauncher", MessageBoxButton.OK, MessageBoxImage.Error);
							await this.ShowMessageAsync("MELauncher", strings.GetString("ni_me"), MessageDialogStyle.Affirmative, mds);
                        }
                        break;
                    case GameSelected.ME2Selected:
                        if (hasME2)
                        {
                            WriteLog("Checking if TexMod is installed for Mass Effect 2");
                            if (File.Exists(Settings.ME2Location + @"\Binaries\TexMod.exe"))
                            {
                                WriteLog("TexMod was found. Initializing replacements");

                                WriteLog("Replacing ME2Game.exe with TexMod, backing up game executable");
                                File.Move(Settings.ME2Location + @"\Binaries\ME2Game.exe", Settings.ME2Location + @"\Binaries\mel.backup");
                                File.Move(Settings.ME2Location + @"\Binaries\TexMod.exe", Settings.ME2Location + @"\Binaries\ME2Game.exe");

                                WriteLog("Initializing game launch thread");
                                ListenForTexModLaunch("me2");
                                enabled = true;
                            }
                            else
                            {
                                WriteLog("TexMod was not found, launching TexMod downloader window");
                                new TexModDownloadWindow(Settings.OriginLocation, Settings.ME2Location + @"\Binaries\", logger).ShowDialog();
                            }
                        }
                        else
                        {
                            WriteLog("Mass Effect 2 is not currently installed onto the system");
							//MessageBox.Show(strings.GetString("ni_me2"), "MELauncher", MessageBoxButton.OK, MessageBoxImage.Error);
							await this.ShowMessageAsync("MELauncher", strings.GetString("ni_me2"), MessageDialogStyle.Affirmative, mds);
                        }
                        break;
                    case GameSelected.ME3Selected:
                        if (hasME3)
                        {
                            WriteLog("Checking if TexMod is installed for Mass Effect 3");
                            if (File.Exists(Settings.ME3Location + @"\Binaries\Win32\TexMod.exe"))
                            {
                                WriteLog("TexMod was found. Initializing replacements");

                                WriteLog("Replacing MassEffect3.exe with TexMod, backing up game executable");
                                File.Move(Settings.ME3Location + @"\Binaries\Win32\MassEffect3.exe", Settings.ME3Location + @"\Binaries\Win32\mel.backup");
                                File.Move(Settings.ME3Location + @"\Binaries\Win32\TexMod.exe", Settings.ME3Location + @"\Binaries\Win32\MassEffect3.exe");

                                WriteLog("Initializing game launch thread");
                                ListenForTexModLaunch("me3");
                                enabled = true;
                            }
                            else
                            {
                                WriteLog("TexMod was not found, launching TexMod downloader window");
                                new TexModDownloadWindow(Settings.OriginLocation, Settings.ME3Location + @"\Binaries\Win32\", logger).ShowDialog();
                            }
                        }
                        else
                        {
                            WriteLog("Mass Effect 3 is not currently installed onto the system");
							//MessageBox.Show(strings.GetString("ni_me3"), "MELauncher", MessageBoxButton.OK, MessageBoxImage.Error);
							try
							{
								await this.ShowMessageAsync("MELauncher", strings.GetString("ni_me3"), MessageDialogStyle.Affirmative, mds);
							}
							catch
							{
								MessageBox.Show(strings.GetString("ni_me3"), "MELauncher", MessageBoxButton.OK, MessageBoxImage.Error);
							}
                        }
                        break;
					case GameSelected.MEASelected:
						await this.ShowMessageAsync(strings.GetString("mb_error_title"), strings.GetString("tm_not_supported_in_andromeda"), MessageDialogStyle.Affirmative, mds);
						break;
                    case GameSelected.NoneSelected:
						//MessageBox.Show(strings.GetString("nogamesel"), strings.GetString("nogamesel_title"), MessageBoxButton.OK, MessageBoxImage.Error);
						try
						{
							await this.ShowMessageAsync(strings.GetString("nogamesel_title"), strings.GetString("nogamesel"), MessageDialogStyle.Affirmative, mds);
						}
						catch
						{
							MessageBox.Show(strings.GetString("nogamesel"), strings.GetString("nogamesel_title"), MessageBoxButton.OK, MessageBoxImage.Error);
						}
                        break;
                }

                if (enabled)
                {
                    WriteLog("TexMod is enabled, taking care of a few UI changes to reflect it");
                    bEnable.Content = buttons.GetButton("disable");
                    bOrOpen.IsEnabled = true;
                    bClose.IsEnabled = false;
                    bSettings.IsEnabled = false;
                    bCheckLocations.IsEnabled = false;
                    bAutoDetect.IsEnabled = false;
                    gbGames.IsEnabled = false;
                    isTexModEnabled = true;

                    WriteLog("Starting up crash handler");
                    FileStream fs = File.Open(Settings.CrashHandlerFile, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    StreamWriter writer = new StreamWriter(fs);
                    writer.WriteLine(gs);
                    writer.Flush();
                    writer.Dispose();
                    fs.Close();

                    WriteLog("Initializing application startup flow upon app crash");
                    if (!Settings.StartUp)
                        StartUp(true);
                }
            }
        }

        private void disable()
        {
            WriteLog("Initializing TexMod disable methods");
            GameSelected gs = (gamesel == GameSelected.NoneSelected) ? GameSelected.NoneSelected : gamesel;
            if ((bool)rbME.IsChecked)
                gs = GameSelected.MESelected;
            else if ((bool)rbME2.IsChecked)
                gs = GameSelected.ME2Selected;
            else if ((bool)rbME3.IsChecked)
                gs = GameSelected.ME3Selected;

            bool hasToRecheck = false;

            switch (gs)
            {
                case GameSelected.MESelected:
                    WriteLog("Making sure everything is as it should be for reverting");
                    if (File.Exists(Settings.MELocation + @"\Binaries\mel.backup"))
                    {
                        WriteLog("Reverting changes to MassEffect.exe");
                        File.Move(Settings.MELocation + @"\Binaries\MassEffect.exe", Settings.MELocation + @"\Binaries\TexMod.exe");
                        File.Move(Settings.MELocation + @"\Binaries\mel.backup", Settings.MELocation + @"\Binaries\MassEffect.exe");

                        // May not always happen, but leave old renaming open as well for those who may still have to open MassEffectLauncher
                        if(File.Exists(Settings.MELocation + @"\MassEffectLauncher.exe"))
						{
							WriteLog("Reverting changes to MassEffectLauncher.exe");
							File.Delete(Settings.MELocation + @"\MassEffectLauncher.exe");
							File.Move(Settings.MELocation + @"\mell.backup", Settings.MELocation + @"\MassEffectLauncher.exe");
						}
                    }
                    break;
                case GameSelected.ME2Selected:
                    WriteLog("Making sure everything is as it should be for reverting");
                    if (File.Exists(Settings.ME2Location + @"\Binaries\mel.backup"))
                    {
                        WriteLog("Reverting changes to ME2Game.exe");
                        File.Move(Settings.ME2Location + @"\Binaries\ME2Game.exe", Settings.ME2Location + @"\Binaries\TexMod.exe");
                        File.Move(Settings.ME2Location + @"\Binaries\mel.backup", Settings.ME2Location + @"\Binaries\ME2Game.exe");
                    }
                    break;
                case GameSelected.ME3Selected:
                    WriteLog("Making sure everything is as it should be for reverting");
                    if (File.Exists(Settings.ME3Location + @"\Binaries\Win32\mel.backup"))
                    {
                        WriteLog("Reverting changes to MassEffect2.exe");
                        File.Move(Settings.ME3Location + @"\Binaries\Win32\MassEffect3.exe", Settings.ME3Location + @"\Binaries\Win32\TexMod.exe");
                        File.Move(Settings.ME3Location + @"\Binaries\Win32\mel.backup", Settings.ME3Location + @"\Binaries\Win32\MassEffect3.exe");
                    }
                    break;
                case GameSelected.NoneSelected:
                    WriteLog("No game was selected. This is indicative of an app crash, doing a second pass through");
                    hasToRecheck = true;
                    break;
            }

            FileStream fs = null;
            try
            {
                WriteLog("Checking integrity of crash handler");
                fs = new FileStream(Settings.CrashHandlerFile, FileMode.Open, FileAccess.Read);
            }
            catch(Exception)
            {
                WriteLog("Crash handler was corrupted. This happens when another instance interferes with another one, fixing...");
                if (MessageBox.Show(strings.GetString("secondInstance"), strings.GetString("mb_error_title"), MessageBoxButton.OK, MessageBoxImage.Error) == MessageBoxResult.OK)
                    this.Close();
            }
            StreamReader reader = new StreamReader(fs);
            string line = "";
            while ((line = reader.ReadLine()) != null)
            {
                switch (line)
                {
                    case "MESelected":
                        gamesel = GameSelected.MESelected;
                        break;
                    case "ME2Selected":
                        gamesel = GameSelected.ME2Selected;
                        break;
                    case "ME3Selected":
                        gamesel = GameSelected.ME3Selected;
                        break;
                    default:
                        gamesel = GameSelected.NoneSelected;
                        break;
                }
                break;
            }

            reader.Dispose();
            fs.Close();

            if (hasToRecheck)
                disable();
            else
            {
                WriteLog("Reverting completed successfully, changing the UI to reflect this");
                bEnable.Content = buttons.GetButton("enable");
                bOrOpen.IsEnabled = false;
                bClose.IsEnabled = true;
                bSettings.IsEnabled = true;
                bCheckLocations.IsEnabled = true;
                bAutoDetect.IsEnabled = true;
                gbGames.IsEnabled = true;
                isTexModEnabled = false;

                WriteLog("Terminating crash handler");
                File.Delete(Settings.CrashHandlerFile);

                WriteLog("Removing startup key from registry if not set manually");
                if (!Settings.StartUp)
                    StartUp(false);

                if (closeWindow || tempAutoClose)
                    this.Close();
            }
        }
        #endregion

        #region Threaded Methods
        private void ListenForTexModLaunch(string launched)
        {
            threadRunning = true;
            WriteLog("Starting game launch thread");
            listenTask = Task.Factory.StartNew(() => { listen(launched); stopListening(); });
        }

        private void listen(string launched)
        {
            //string game = null;
            List<string> gameList = new List<string>();
            closeWindow = (Settings.CloseOnRun) ? true : false;
            switch(launched)
            {
                case "me":
                    //game = "MassEffect";
                    gameList.Add("MassEffect");
                    gameList.Add("MassEffectLauncher");
                    break;
                case "me2":
                    //game = "ME2Game";
                    gameList.Add("ME2Game");
                    break;
                case "me3":
                    //game = "MassEffect3";
                    gameList.Add("MassEffect3");
                    break;
            }

            WriteLog("Listening for game launch...");
            while(threadRunning)
            {
                foreach(string game in gameList)
                {
                    Process[] p = Process.GetProcessesByName(game);
                    if (p.Length >= 1)
                        threadRunning = false;
                    System.Threading.Thread.Sleep(500); // Pause thread every half second. Keeps SPU usage low
                }
            }
            WriteLog("Game launched. Terminating thread");

            return;
        }

        private void stopListening()
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(new d_KillThreads(disable));
            else
            {
                WriteLog("Thread terminated. Initializing TexMod disable methods");
                disable();
            }
        }
        #endregion

        #region Extra Methods
        private void SetLocale()
        {
            titles = Titles.Load(Settings.Language);
            strings = Strings.Load(Settings.Language);
            buttons = Buttons.Load(Settings.Language);
            radioButtons = RadioButtons.Load(Settings.Language);

			this.Title = titles.GetTitle("MELauncher/OriginWindow");
            lblOriginBase.Text = strings.GetString("originBase");
            lblMEBase.Text = strings.GetString("MEBase");
            lblME2Base.Text = strings.GetString("ME2Base");
            lblME3Base.Text = strings.GetString("ME3Base");
            gbGames.Header = strings.GetString("gamesGroup");

            bBrowseOrigin.Content = buttons.GetButton("browse");
            bBrowseME.Content = buttons.GetButton("browse");
            bBrowseME2.Content = buttons.GetButton("browse");
            bBrowseME3.Content = buttons.GetButton("browse");

            bSettings.Content = buttons.GetButton("settings");
            bCheckLocations.Content = buttons.GetButton("checkLocations");
            bAutoDetect.Content = buttons.GetButton("autoDetect");
            bEnable.Content = buttons.GetButton("enable");
            bClose.Content = buttons.GetButton("closeME");
            bOrOpen.Content = buttons.GetButton("originLaunch");

            rbME.Content = radioButtons.GetButton("me");
            rbME2.Content = radioButtons.GetButton("me2");
            rbME3.Content = radioButtons.GetButton("me3");
        }

		private async Task<MessageDialogResult> ShowMessageDialog(string title, string message, MessageDialogStyle style = MessageDialogStyle.Affirmative)
		{
			return await this.ShowMessageAsync(title, message, style);
		}

		private void AlertUpdated()
		{
			new ChangelogWindow().ShowDialog();
		}

		private void LaunchFromShortcut()
		{
			tempAutoClose = true;
			switch(App.LaunchGameFromShortcut)
			{
				case "me":
					gamesel = GameSelected.MESelected;
					rbME.IsChecked = true;
					break;
				case "me2":
					gamesel = GameSelected.ME2Selected;
					rbME2.IsChecked = true;
					break;
				case "me3":
					gamesel = GameSelected.ME3Selected;
					rbME3.IsChecked = true;
					break;
				case "mea":
					gamesel = GameSelected.MEASelected;
					rbMEA.IsChecked = true;
					break;
			}
			//bEnable_Click(null, null);
			//bOrOpen_Click(null, null);

			//var win = new SelectRunWindow(game, gameID[game], "steam", logger);
			SelectRunWindow win = new SelectRunWindow(App.LaunchGameFromShortcut, Settings.OriginGameIDs[App.LaunchGameFromShortcut], "origin", logger, true);
			win.ShowDialog();

			if (!win.DisableTexMod)
			{
				bEnable_Click(null, null);
				win.RunGame();
			}
		}
        #endregion
    }
}
