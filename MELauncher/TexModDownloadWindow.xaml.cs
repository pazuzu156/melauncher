﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MEL.Settings;
using MEL.Locale;
using System.Net;

using MEL.Logger;

using MahApps.Metro.Controls.Dialogs;

namespace MELauncher
{
    /// <summary>
    /// Interaction logic for TexModDownloadWindow.xaml
    /// </summary>
    public partial class TexModDownloadWindow
    {
        Settings settings;
        Strings strings;
        Buttons buttons;
        Titles titles;

        LoggerWindow logger;

        private DownloaderWindow downloadWindow;
        private string melocation, originLocation;

        public void WriteLog(string write)
        {
            if (logger != null)
                logger.WriteToLog(write);
        }

        public TexModDownloadWindow(string ol, string ml, LoggerWindow logger)
        {
            InitializeComponent();

            this.logger = logger;

            WriteLog("Initializing TexMod download window");

            WriteLog("Loading settings");

            settings = Settings.getInstance();
            settings.LoadSettings();

            strings = Strings.Load(Settings.Language);
            buttons = Buttons.Load(Settings.Language);
            titles = Titles.Load(Settings.Language);

            WriteLog("Applying application theme");

			string theme = (Settings.Theme == Settings.ThemeType.Light) ? "Light" : "Dark";

			App.Current.Resources.MergedDictionaries.Add(
				new ResourceDictionary { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme." + theme + ".xaml", UriKind.Absolute) });

			dlLink.Foreground = (theme.Equals("Dark")) ? Brushes.LightBlue : Brushes.Blue;

            SetLocale();

            if (Settings.AOT)
                this.Topmost = true;

            melocation = ml;
            originLocation = ol;
            downloadWindow = new DownloaderWindow(this, logger, melocation);
        }

        private void SetLocale()
        {
            WriteLog("Setting locale");
            this.Title = titles.GetTitle("MELauncher/TMDownloadWindow");
            lblTMDLABT.Text = strings.GetString("tm_nf");
            bDownload.Content = buttons.GetButton("tmDownload");
            bClose.Content = buttons.GetButton("tmClose");
        }

        private async void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
        {
			MessageDialogResult res = await this.ShowMessageAsync(strings.GetString("tmAnchorTitle"), strings.GetString("tmAnchorClicked"), MessageDialogStyle.Affirmative, new MetroDialogSettings() { ColorScheme = MetroDialogColorScheme.Accented });
			//if(MessageBox.Show(strings.GetString("tmAnchorClicked"), strings.GetString("tmAnchorTitle"), MessageBoxButton.OK, MessageBoxImage.Information) == MessageBoxResult.OK)
			if(res == MessageDialogResult.Affirmative)
            {
                WriteLog("Opening TexMod webpage in default browser");
                System.Diagnostics.Process.Start(e.Uri.ToString());
                System.Diagnostics.ProcessStartInfo pi = new System.Diagnostics.ProcessStartInfo();
                pi.FileName = "explorer";
                pi.Arguments = (System.IO.File.Exists(melocation + "MassEffectConfig.exe")) ? melocation + @"..\" : melocation;
                System.Diagnostics.Process.Start(pi);
            }
        }

        private void bClose_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Terminating download window");
            this.Close();
        }

        private void bDownload_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Checking for an internet connection");
            StatusCheck check = new StatusCheck();
            if(check.IsConnected)
            {
                WriteLog("Connection established. Initializing download");
                WriteLog("Downloading TexMod");
				downloadWindow.Open();
            }
            else
            {
                MessageBox.Show(strings.GetString("mb_no_internet"), strings.GetString("mb_tm_title"), MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
    }
}
