﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MELauncher.Notifications
{
    class NotificationService
    {
        private static Thread NotificationServiceTask;
        private static NotificationService _instance = null;
        private volatile bool running = false;
        private Notification NotificationWindow;
        private MainWindow mainWindow;

        public static void StartService(MainWindow parent)
        {
            if (_instance == null)
                _instance = new NotificationService(parent);

            NotificationServiceTask.Start();
        }

        private delegate void DisplayNotificationDelegate();
        private void DisplayNotification()
        {
            NotificationWindow.ShowDialog();
        }

        public NotificationService(MainWindow parent)
        {
            NotificationServiceTask = new Thread(() => { ServiceThread(); });
            NotificationServiceTask.SetApartmentState(ApartmentState.STA);

            NotificationWindow = new Notification();
            mainWindow = parent;
        }

        private void ServiceThread()
        {
            running = true;
            while(running)
            {
                ProcessStartInfo psi = new ProcessStartInfo();
                psi.FileName = Environment.CurrentDirectory + @"\Updater.exe";
                psi.Arguments = "/FromNotificationService";
                Process NCP = Process.Start(psi);
                NCP.WaitForExit();
                int result = NCP.ExitCode;
                
                if(result == 500)
                {
                    //mainWindow.Dispatcher.Invoke(DisplayNotificationDelegate(DisplayNotification));
                }
            }
        }
    }
}
