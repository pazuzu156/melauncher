﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MEL.Settings;
using MEL.Locale;
using System.IO;

using MEL.Logger;

using MahApps.Metro.Controls.Dialogs;

namespace MELauncher
{
    /// <summary>
    /// Interaction logic for SelectOpenWindow.xaml
    /// </summary>
    public partial class SelectOpenWindow
    {
        private string location;
        Settings s;
        Strings strings;
        Titles titles;
        Buttons buttons;
        RadioButtons rButtons;

		string DRM;

        LoggerWindow logger;
        private void WriteLog(string write)
        {
            if (logger != null)
                logger.WriteToLog(write);
        }

        public SelectOpenWindow(string location, string drm, LoggerWindow logger)
        {
            InitializeComponent();

			// make sure to hide binaries directory for andromeda
			if (File.Exists(location + @"\MassEffectAndromeda.exe"))
			{
				rbBinDir.Visibility = System.Windows.Visibility.Hidden;
			}

            this.logger = logger;
			DRM = drm;

            WriteLog("Initializing selection window");

            WriteLog("Loading settings");

            s = Settings.getInstance();
            s.LoadSettings();

            WriteLog("Applying application theme");
			string theme = (Settings.Theme == Settings.ThemeType.Light) ? "Light" : "Dark";

			App.Current.Resources.MergedDictionaries.Add(
				new ResourceDictionary { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme." + theme + ".xaml", UriKind.Absolute) });

            SetLocale();

            rbBaseDir.IsChecked = true;
            this.location = location;

			//if (File.Exists(location + @"\Binaries\MassEffect.exe") && drm.Equals("origin"))
			//	bDesktopShortcut.IsEnabled = false;
        }

        private void SetLocale()
        {
            WriteLog("Setting locale");
            strings = Strings.Load(Settings.Language);
            titles = Titles.Load(Settings.Language);
            buttons = Buttons.Load(Settings.Language);
            rButtons = RadioButtons.Load(Settings.Language);

            this.Title = titles.GetTitle("melauncher/selectopenwindow");
            lblOpenQuestion.Text = strings.GetString("select_open_question");
            rbBaseDir.Content = rButtons.GetButton("open_base_dir");
            rbBinDir.Content = rButtons.GetButton("open_bin_dir");
            bOpen.Content = buttons.GetButton("open");
            bCancel.Content = buttons.GetButton("cancel");
			bDesktopShortcut.Content = buttons.GetButton("create_shortcut");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Terminating selection window");
            this.Close();
        }

        private void OpenFolder(string location)
        {
            WriteLog("Opening Windows explorer at: " + location);
            ProcessStartInfo psi = new ProcessStartInfo();
            psi.FileName = Environment.GetFolderPath(Environment.SpecialFolder.Windows) + @"\explorer.exe";
            psi.Arguments = location;
            Process.Start(psi);
        }

        private async void bOpen_Click(object sender, RoutedEventArgs e)
        {
            if(rbBaseDir.IsChecked == true || rbBinDir.IsChecked == true)
            {
                if(rbBaseDir.IsChecked == true)
                {
                    WriteLog("Base directory option selected");
                    OpenFolder(location);
                }
                else if(rbBinDir.IsChecked == true)
                {
                    WriteLog("Game binaries location selected");
                    string bin = location + @"\Binaries";
                    if(Directory.Exists(bin + @"\Win32"))
                    {
                        OpenFolder(bin + @"\Win32");
                    }
                    else
                    {
                        OpenFolder(bin);
                    }
                }

                this.Close();
            }
            else
            {
                WriteLog("Nothing selected, canceling action");
				//MessageBox.Show("You must select one of the radio buttons!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				await this.ShowMessageAsync(strings.GetString("mb_error_title"), strings.GetString("no_option_selected"), MessageDialogStyle.Affirmative, new MetroDialogSettings() { ColorScheme = MetroDialogColorScheme.Accented });
            }
        }

		private void bDesktopShortcut_Click(object sender, RoutedEventArgs e)
		{
			string shortcutContent = @"[{000214A0-0000-0000-C000-000000000046}]
Prop3=19,0
[InternetShortcut]
IDList=
";
			string game = "";
			if (File.Exists(location + @"\Binaries\MassEffect.exe"))
			{
				game = "Mass Effect";
				shortcutContent += "URL=melauncher://launch/" + DRM + "/me\n";
				shortcutContent += "IconFile=" + location + @"\Binaries\MassEffect.exe";
			}
			else if (File.Exists(location + @"\Binaries\MassEffect2.exe") || File.Exists(location + @"\Binaries\ME2Game.exe"))
			{
				game = "Mass Effect 2";
				shortcutContent += "URL=melauncher://launch/" + DRM + "/me2\n";
				shortcutContent += "IconFile=" + location + @"\Binaries\MassEffect2.exe";
			}
			else if (File.Exists(location + @"\Binaries\Win32\MassEffect3.exe"))
			{
				game = "Mass Effect 3";
				shortcutContent += "URL=melauncher://launch/" + DRM + "/me3\n";
				shortcutContent += "IconFile=" + location + @"\Binaries\Win32\MassEffect3.exe";
			}
			else if (File.Exists(location + @"\MassEffectAndromeda.exe"))
			{
				game = "Mass Effect Andromeda";
				shortcutContent += "URL=melauncher://launch/" + DRM + "/mea\n";
				shortcutContent += "IconFile=" + location + @"\MassEffectAndromeda.exe";
			}
			shortcutContent += @"
IconIndex=0
HotKey=0
";

			string filename = string.Format("{0} (MELauncher).url", game);

			FileStream file = File.Open(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + filename, FileMode.OpenOrCreate, FileAccess.Write);
			StreamWriter writer = new StreamWriter(file);
			writer.Write(shortcutContent);
			writer.Flush();
			writer.Close();
			file.Close();

			MessageBox.Show("Desktop shortcut: \"" + filename + "\" created!");
		}
    }
}
