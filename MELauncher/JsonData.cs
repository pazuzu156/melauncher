﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace MELauncher
{
	public class JsonData
	{
		public RootObject Data
		{
			get
			{
				return _data;
			}
			private set
			{
				_data = value;
			}
		}
		private static RootObject _data;

		public static JsonData Load(string url)
		{
			var j = new JsonData();
			_data = j.load(url);

			return j;
		}

		private RootObject load(string url)
		{
			RootObject data = null;

			WebRequest request = WebRequest.Create(new Uri(url));
			WebResponse response = request.GetResponse();

			using (var reader = new StreamReader(response.GetResponseStream()))
			{
				Data = new JavaScriptSerializer().Deserialize<RootObject>(reader.ReadToEnd());
			}

			//return Data;

			//Task.Factory.StartNew(() =>
			//	{
			//		WebRequest request = WebRequest.Create(new Uri(url));
			//		request.Timeout = 5000;
			//		WebResponse response = request.GetResponse();
					

			//		using(var reader = new StreamReader(response.GetResponseStream()))
			//		{
			//			Data = new JavaScriptSerializer().Deserialize<RootObject>(reader.ReadToEnd());
			//		}

			//		return Data;
			//	});

			return Data;
		}

		public class Updater
		{
			public string version { get; set; }
			public List<string> packages { get; set; }
		}

		public class Melauncher
		{
			public string version { get; set; }
			public List<string> packages { get; set; }
		}

		public class Locale
		{
			public List<string> packages { get; set; }
		}

		public class RootObject
		{
			public Updater updater { get; set; }
			public Melauncher melauncher { get; set; }
			public Locale locale { get; set; }
		}
	}
}
