﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MEL.Settings;
using MEL.Locale;
using System.Data;
using System.Diagnostics;

using MahApps.Metro.Controls.Dialogs;

using MEL.Logger;

namespace MELauncher
{
    /// <summary>
    /// Interaction logic for AboutWindow.xaml
    /// </summary>
    public partial class AboutWindow
    {
        Settings settings;
        Strings strings;
        Buttons buttons;
        Titles titles;

        LoggerWindow logger;

		MetroDialogSettings mds = new MetroDialogSettings() { ColorScheme = MetroDialogColorScheme.Accented };

        private void WriteLog(string write)
        {
            if (logger != null)
                logger.WriteToLog(write);
        }

        public AboutWindow(LoggerWindow logger)
        {
            InitializeComponent();

            this.logger = logger;

            WriteLog("Initializing settings");

            settings = Settings.getInstance();

            if (Settings.AOT)
                this.Topmost = true;

            WriteLog("Applying application theme to window");

			string theme = (Settings.Theme == Settings.ThemeType.Light) ? "Light" : "Dark";

			App.Current.Resources.MergedDictionaries.Add(
				new ResourceDictionary { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme." + theme + ".xaml", UriKind.Absolute) });

            SetLocale();
        }

        private void SetLocale()
        {
            WriteLog("Setting window locale");
            strings = Strings.Load(Settings.Language);
            buttons = Buttons.Load(Settings.Language);
            titles = Titles.Load(Settings.Language);

            this.Title = titles.GetTitle("MELauncher/AboutWindow");

            lblAbout.Text = strings.GetString("aboutBoxText");
            lblHeader.Text = strings.GetString("aboutBoxTextHeader");
            lblVersion.Text = strings.GetString("aboutVersion") + " " + Settings.Version;
			
			#if DEBUG
			string aboutAppIdString = strings.GetString("aboutAppID") + " " + Settings.AppID + "-DEBUG";
			#else
			string aboutAppIdString = strings.GetString("aboutAppID") + " " + Settings.AppID;
			#endif
			
            lblAppID.Text = aboutAppIdString;

            bClose.Content = buttons.GetButton("closeAbout");
            bCheck.Content = buttons.GetButton("updateCheck");
            bChangelog.Content = buttons.GetButton("viewChangelog");
        }

        private void bClose_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Close button clicked");
            WriteLog("Terminating about window");
            this.Close();
        }

        private void bCheck_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Checking for updates");
			new UpdateChecker(Settings.Version, false);
        }

        private void bChangelog_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Launching changelog window");
            try
			{
				new ChangelogWindow().ShowDialog();
			}
			catch { }
        }

        private async void bHelp_Click(object sender, RoutedEventArgs e)
        {
			bool error = false;
            try
            {
                WriteLog("Launching compiled help file");
                Process.Start("MELauncherHelp.chm");
            }
            catch (Exception ex)
            {
                WriteLog("Error launching compiled help file. Error: " + ex.Message);
				//MessageBox.Show(strings.GetString("no_help"), strings.GetString("mb_error_title"), MessageBoxButton.OK, MessageBoxImage.Error);
				error = true;
            }

			if(error)
				await this.ShowMessageAsync(strings.GetString("mb_error_title"), strings.GetString("no_help"), MessageDialogStyle.Affirmative, mds);
        }

        private async void bSite_Click(object sender, RoutedEventArgs e)
        {
			bool error = false;
            try
            {
                WriteLog("Opening web page in default browser");
                Process.Start("http://www.kalebklein.com");
            }
            catch(Exception ex)
            {
                WriteLog("Error opening web page. Error: " + ex.Message);
				//MessageBox.Show(strings.GetString("site_no_load"), strings.GetString("mb_error_title"), MessageBoxButton.OK, MessageBoxImage.Error);
				error = true;
            }

			if(error)
				await this.ShowMessageAsync(strings.GetString("mb_error_title"), strings.GetString("site_no_load"), MessageDialogStyle.Affirmative, mds);
        }
    }
}
