﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MEL.Settings;
using MEL.Locale;
using System.Data;
using System.Management;

using MEL.Logger;

namespace MELauncher
{
    /// <summary>
    /// Interaction logic for BugReporter.xaml
    /// </summary>
    public partial class BugReporter
    {
        Settings settings;
        Titles titles;
        Strings strings;
        Buttons buttons;

        LoggerWindow logger;
        private void WriteLog(string write)
        {
            if (logger != null)
                logger.WriteToLog(write);
        }

        public BugReporter(LoggerWindow logger)
        {
            this.logger = logger;

            settings = Settings.getInstance();
            settings.LoadSettings();

            InitializeComponent();

            WriteLog("Loading settings");

            if (Settings.AOT)
                this.Topmost = true;

            WriteLog("Applying application theme to window");
			string theme = (Settings.Theme == Settings.ThemeType.Light) ? "Light" : "Dark";

			App.Current.Resources.MergedDictionaries.Add(
				new ResourceDictionary { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme." + theme + ".xaml", UriKind.Absolute) });

            titles = Titles.Load(Settings.Language);
            strings = Strings.Load(Settings.Language);
            buttons = Buttons.Load(Settings.Language);

            SetLocale();
        }

        private void SetLocale()
        {
			WriteLog("Setting locale");
            this.Title = titles.GetTitle("MELauncher/BugReporterWindow");
            lblBugInfo.Text = strings.GetString("bug_report_text");
            lblEmail.Text = strings.GetString("bug_report_email");
			lblMessage.Text = strings.GetString("bug_report_message");
            bCancel.Content = buttons.GetButton("cancel");
            bSubmit.Content = buttons.GetButton("submit");
        }

        private void bCancel_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Setting button clicked");
            WriteLog("Terminating bug reporting tool");
            this.Close();
        }

        private void bSubmit_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Preparing to submit bug report");
            WriteLog("Checking for an Internet connection");
            StatusCheck sc = new StatusCheck("http://cdn.kalebklein.com/mel/BugReport/hsc.txt");
            if(sc.IsConnected)
            {
                WriteLog("Connection to reporting server established. Checking parameters");
                if (tbReport.Text.Equals(""))
                {
                    WriteLog("No report supplied. Canceling request");
                    MessageBox.Show(strings.GetString("mb_br_empty"), strings.GetString("mb_br_error_title"), MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    WriteLog("Establishing connection to reporting server");
                    var request = (HttpWebRequest)WebRequest.Create("http://cdn.kalebklein.com/mel/BugReport/submit");
                    request.ContentType = "text/json";
                    request.Method = "POST";

                    using (var writer = new StreamWriter(request.GetRequestStream()))
                    {
                        var email = "NoEmail";
                        if (!tbEmail.Text.Equals(""))
                        {
                            WriteLog("Email supplied, setting report author");
                            email = tbEmail.Text;
                        }
                        else
                            WriteLog("No email given. Anonymous submission");

                        WriteLog("Compiling report");
                        string json = new JavaScriptSerializer().Serialize(new
                        {
                            appID = Settings.AppID,
                            report = tbReport.Text,
                            processorInfo = GetSystemInfo("Win32_Processor", new List<string> { "Name", "NumberOfCores", "NumberOfLogicalProcessors", "AdressWidth" }),
                            videoInfo = GetSystemInfo("Win32_VideoController", new List<string> { "Name", "AdapterCompatibility" }),
                            osInfo = GetSystemInfo("Win32_OperatingSystem", new List<string> { "Name" }),
                            emailAddress = email
                        });
                        WriteLog("Submitting report to server");
                        writer.Write(json);
                        writer.Flush();
                        writer.Close();

                        WriteLog("Waiting on response from server");
                        //var response = (HttpWebResponse)request.GetResponse();
						HttpWebResponse response;
						try
						{
							response = (HttpWebResponse)request.GetResponse();
							using (var reader = new StreamReader(response.GetResponseStream()))
							{
								var result = reader.ReadToEnd();
								var error = new JavaScriptSerializer().Deserialize<Dictionary<string, string>>(result);
								if (error["error"].Equals("False"))
								{
									WriteLog("Server responded with no error. Submission was successful");
									if (MessageBox.Show(strings.GetString("mb_br_success"), strings.GetString("mb_br_success_title"), MessageBoxButton.OK, MessageBoxImage.Information) == MessageBoxResult.OK)
									{
										WriteLog("Terminating bug reporting tool");
										this.Close();
									}
								}
								else
								{
									WriteLog("Server responded with an error. Submission was not sent successfully");
									MessageBox.Show(strings.GetString("mb_br_nosubmit"), strings.GetString("mb_br_error_title"), MessageBoxButton.OK, MessageBoxImage.Error);
								}
								reader.Close();
							}
							response.Close();
						}
						catch
						{
							MessageBox.Show(strings.GetString("mb_br_no_res"), strings.GetString("mb_br_error_title"), MessageBoxButton.OK, MessageBoxImage.Error);
						}
                    }
                }
            }
            else
            {
                WriteLog("Error connecting to server. No connection could be established");
                MessageBox.Show(strings.GetString("mb_br_nointernet"), strings.GetString("mb_br_error_title"), MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private string GetSystemInfo(string key, List<string> value)
        {
            WriteLog("Gathering system information for: " + key);
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from " + key);
            var itemList = new Dictionary<string, string>();
            try
            {
                foreach (ManagementObject share in searcher.Get())
                {
                    foreach (PropertyData PC in share.Properties)
                    {
                        if (PC.Value != null && PC.Value.ToString() != "")
                        {
                            if (value.Contains(PC.Name))
                            {
                                WriteLog("Gathering information from " + key + " using " + PC.Name);
                                itemList.Add(PC.Name, PC.Value.ToString().Trim());
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                WriteLog("Error obtaining info on " + key + ". Error: " + ex.Message);
                MessageBox.Show("Error: " + ex);
            }

            var result = new JavaScriptSerializer().Serialize(itemList);
            return result;
        }
    }
}
