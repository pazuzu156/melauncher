﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.IO;
using System.Windows.Media;
using Microsoft.Win32;

using MEL.Logger;

namespace MELauncher
{
    class LocationDetector
    {
        private Dictionary<string, string> locationInfo;
        private static LocationDetector m_Instance;

        public LocationDetector()
        {
            locationInfo = new Dictionary<string, string>();
        }

        public static bool CheckLocation(string location, TextBox tb, Button b, TextBlock lbl)
        {
            var strings = MEL.Locale.Strings.Load();
            var buttons = MEL.Locale.Buttons.Load();
            if(File.Exists(location))
            {
                SetText(lbl, strings.GetString("found"), (SolidColorBrush)(new BrushConverter().ConvertFrom("#00b800")));
                tb.IsEnabled = false;
                //b.IsEnabled = false;
                b.Content = buttons.GetButton("open");
                return true;
            }
            else
            {
                SetText(lbl, strings.GetString("nfound"), Brushes.DarkRed);
                return false;
            }
        }

        public Dictionary<string, string> ItemsList(string[] keys, string[] values)
        {
            var result = new Dictionary<string, string>();
            for(int i = 0; i < keys.Length; i++)
            {
                result.Add(keys[i], values[i]);
            }

            return result;
        }

        public LocationDetector SniffForLocation(string registryKey, Dictionary<string, string> registryValues, string implementation)
        {
            RegistryKey key = Registry.LocalMachine.OpenSubKey(registryKey);
            string location;
            string flavor;
            locationInfo.Clear();

            try
            {
                location = key.GetValue(registryValues["location"]).ToString();
            }
            catch (Exception)
            {
                location = null;
            }

            if (registryValues.Count > 1)
            {
                try
                {
                    flavor = key.GetValue(registryValues["steam"]).ToString();
                }
                catch (Exception)
                {
                    flavor = null;
                }
            }
            else
                flavor = null;

			if ((location != null && (flavor == "steam" || flavor == "Steam") && implementation == "steam"))
            {
                locationInfo.Add("steamFlavor", "true");
                locationInfo.Add("location", location);
            }
			else if (location != null && flavor == "DVD" && implementation == "origin")
            {
                locationInfo.Add("steamFlavor", "false");
                locationInfo.Add("location", location);
            }
			else if (location != null && flavor == null)
            {
                locationInfo.Add("steamFlavor", "false");
                locationInfo.Add("location", location);
            }

            return this;
        }

        public string getLocation()
        {
            try
            {
                return locationInfo["location"];
            }
            catch { return null; }
        }

        public bool IsSteamFlavor()
        {
            try
            {
                return bool.Parse(locationInfo["steamFlavor"]);
            }
            catch { return true; }
        }

        public static void SetText(TextBlock label, string text, Brush brush)
        {
            label.Text = text;
            label.Foreground = brush;
        }

        public static void SetText(TextBlock label, string text)
        {
            label.Text = text;
        }
    }
}
