﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using MEL.Settings;
using MEL.Locale;
using System.Diagnostics;

using MEL.Logger;

namespace MELauncher
{
    /// <summary>
    /// Interaction logic for SelectRunWindow.xaml
    /// </summary>
    public partial class SelectRunWindow
    {
        Settings s;
        Strings strings;
        Buttons buttons;
        RadioButtons rButtons;
        Titles titles;
		bool runOnStartup;

		public bool DisableTexMod
		{
			get
			{
				return (bool)cbDisableTexMod.IsChecked;
			}
		}

		enum DRM
		{
			Steam, Origin
		}

		DRM drm;

        LoggerWindow logger;
        private void WriteLog(string write)
        {
            if (logger != null)
                logger.WriteToLog(write);
        }

        private string m_GameID;

        public SelectRunWindow(string game, string gameID, string drm, LoggerWindow logger, bool runOnStartup = false)
        {
            InitializeComponent();

			this.runOnStartup = runOnStartup;
			if (this.runOnStartup)
			{
				this.cbDisableTexMod.Visibility = System.Windows.Visibility.Visible;
				this.rbSteam.Visibility = System.Windows.Visibility.Hidden;
			}

			switch(drm)
			{
				case "origin":
					this.drm = DRM.Origin;
					break;
				case "steam":
					this.drm = DRM.Steam;
					break;
			}

            this.logger = logger;

            WriteLog("Initializing game selection window");
            WriteLog("Loading settings");

            s = Settings.getInstance();
            s.LoadSettings();

            WriteLog("Applying application theme");
			string theme = (Settings.Theme == Settings.ThemeType.Light) ? "Light" : "Dark";

			App.Current.Resources.MergedDictionaries.Add(
				new ResourceDictionary { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme." + theme + ".xaml", UriKind.Absolute) });

            m_GameID = gameID;

            SetLocale(game);
        }

        private void SetLocale(string game)
        {
            WriteLog("Setting game specific locale");
            strings = Strings.Load(Settings.Language);
            buttons = Buttons.Load(Settings.Language);
            rButtons = RadioButtons.Load(Settings.Language);
            titles = Titles.Load(Settings.Language);

            this.Title = titles.GetTitle("melauncher/selectrunwindow");
			rbSteam.Content = (drm == DRM.Steam) ? rButtons.GetButton("open_steam") : rButtons.GetButton("open_origin");
            bOpen.Content = buttons.GetButton("run");
            bCancel.Content = buttons.GetButton("cancel");

			switch(drm)
			{
				case DRM.Steam:
					rbGame.Content = (game.Equals("me")) ? rButtons.GetButton("open_game_me") : rButtons.GetButton("open_game_me2");
					break;
				case DRM.Origin:
					if (game.Equals("me"))
						rbGame.Content = rButtons.GetButton("open_game_me");
					else if (game.Equals("me2"))
						rbGame.Content = rButtons.GetButton("open_game_me2");
					else if (game.Equals("me3"))
						rbGame.Content = rButtons.GetButton("open_game_me3");
					else if (game.Equals("mea"))
						rbGame.Content = rButtons.GetButton("open_game_mea");
					break;
			}
        }

        private void bOpen_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Launch button clicked");
            if ((bool)rbSteam.IsChecked)
            {
                WriteLog("Steam option selected, launching Steam");
                if(drm == DRM.Steam)
					Process.Start(Settings.SteamLocation + @"\Steam.exe");
				else
					Process.Start(Settings.OriginLocation + @"\Origin.exe");
            }
            else if ((bool)rbGame.IsChecked && !runOnStartup)
            {
				// Make sure TexMod is run as admin
				this.RunGame();
            }
			else
			{
				if (runOnStartup)
				{
					if ((bool)cbDisableTexMod.IsChecked)
					{
						RunGame();
						App.Current.Shutdown();
					}
				}
			}

            this.Close();
        }

		public void RunGame()
		{
			Process p;

			WriteLog("Launching game...");
			if (drm == DRM.Steam)
				//psi = new ProcessStartInfo("start steam://rungameid/" + m_GameID);
				p = Process.Start("steam://rungameid/" + m_GameID);
			else
				p = Process.Start("origin://launchgame/" + m_GameID);

			p.StartInfo.Verb = "runas";
		}

        private void bClose_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Terminating selection window");
            //this.Close();

			if (runOnStartup)
			{
				App.Current.Shutdown();
			}
			else
			{
				this.Close();
			}
        }
    }
}
