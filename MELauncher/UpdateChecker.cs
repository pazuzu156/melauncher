﻿using MEL.Settings;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows;

namespace MELauncher
{
	public class UpdateChecker
	{
		private Version version;
		private MEL.Locale.Strings strings;
		private JsonData jsonData;

		private Version ov;
		private bool startup = false;

		public UpdateChecker(string version, bool startup)
		{
			strings = MEL.Locale.Strings.Load(MEL.Settings.Settings.Language);
			this.version = new Version(version);
			this.startup = startup;
			
			StatusCheck sc = new StatusCheck("http://cdn.kalebklein.com/mel/testcl.txt");
			if(sc.IsConnected)
			{
				try
				{
#if DEBUG
					this.jsonData = JsonData.Load("http://cdn.kalebklein.com/mel/v5/debug/updatev2.json");
#else
					this.jsonData = JsonData.Load("http://cdn.kalebklein.com/mel/v5/updatev2.json");
#endif

					ov = new Version(this.jsonData.Data.melauncher.version);
					Version uov = new Version(this.jsonData.Data.updater.version);

					FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(Settings.LauncherSettingsRootDir + "Updater.exe");

					

					if(uov > new Version(fvi.FileVersion))
					{
						WebClient client = new WebClient();
						client.DownloadFileCompleted += client_DownloadFileCompleted;
						client.DownloadFile(new Uri(this.jsonData.Data.updater.packages[0]),
							Settings.LauncherSettingsRootDir + @"\Updater.exe");
					}
					else
					{
						Run();
					}
				}
				catch(Exception ex)
				{
#if DEBUG
					MessageBox.Show(ex.ToString());
#endif
				}
			}
		}

		void client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
		{
			MessageBox.Show("Updater updated!");
			Run();
		}

		private void Run()
		{
			if (ov > this.version)
			{
				string message = string.Format(@"{0}

Current version: {1}
Update version: {2}

{3}", strings.GetString("new_update_heading"), version, ov.ToString(), strings.GetString("new_update_footer"));

				var result = MessageBox.Show(message, "Update", MessageBoxButton.YesNo, MessageBoxImage.Question);
				if (result == MessageBoxResult.Yes)
				{
					ProcessStartInfo psi = new ProcessStartInfo();
					psi.Verb = "runas";
					psi.Arguments = "\"" + Environment.CurrentDirectory + "\"";
					psi.FileName = Settings.LauncherSettingsRootDir + @"\Updater.exe";
					Process.Start(psi);
				}
			}
			else
				if (!startup)
					MessageBox.Show(strings.GetString("uptodate"), "Update");
		}
	}
}
