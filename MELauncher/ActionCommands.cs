﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MELauncher
{
    public static class ActionCommands
    {
        static ActionCommands()
        {
            Bugs = new RoutedUICommand("Bug Reporter", "BugReporter", typeof(ActionCommands),
                new InputGestureCollection
                {
                    new KeyGesture(Key.F2, ModifierKeys.None, "F2")
                });

            Help = new RoutedUICommand("MELauncher Help", "Help", typeof(ActionCommands),
                new InputGestureCollection
                {
                    new KeyGesture(Key.F1, ModifierKeys.None, "F1")
                });

            Logger = new RoutedUICommand("MELauncher Logger", "Logger", typeof(ActionCommands),
                new InputGestureCollection
                {
                    new KeyGesture(Key.F5, ModifierKeys.None, "F5")
                });
            Reload = new RoutedUICommand("Reload Window", "Reload", typeof(ActionCommands),
                new InputGestureCollection
                {
                    new KeyGesture(Key.R, ModifierKeys.Control, "Ctrl+R")
                });
        }

        public static RoutedUICommand Help { get; private set; }
        public static RoutedUICommand Bugs { get; private set; }
        public static RoutedUICommand Logger { get; private set; }
        public static RoutedUICommand Reload { get; private set; }
    }
}
