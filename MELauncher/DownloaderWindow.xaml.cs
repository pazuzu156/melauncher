﻿using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO.Compression;
using System.IO;

using MEL.Settings;
using MEL.Locale;
using MEL.Logger;
using System.Net;
using System.Threading;

namespace MELauncher
{
	/// <summary>
	/// Interaction logic for DownloaderWindow.xaml
	/// </summary>
	public partial class DownloaderWindow : MetroWindow
	{
		Settings settings;
		Strings strings;
		Titles titles;
		LoggerWindow logger;
		TexModDownloadWindow dlw;

		string melocation;

		public DownloaderWindow(TexModDownloadWindow dlw, LoggerWindow logger, string melocation)
		{
			InitializeComponent();
			this.Topmost = true;

			this.logger = logger;
			this.dlw = dlw;

			settings = Settings.getInstance();
			settings.LoadSettings();

			string theme = (Settings.Theme == Settings.ThemeType.Light) ? "Light" : "Dark";

			App.Current.Resources.MergedDictionaries.Add(
				new ResourceDictionary { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme." + theme + ".xaml", UriKind.Absolute) });

			strings = Strings.Load(Settings.Language);
			titles = Titles.Load(Settings.Language);
			SetLocale();

			this.melocation = melocation;
		}

		public void Open()
		{
			this.Show();
			download();
		}

		private void SetLocale()
		{
			this.Title = titles.GetTitle("MELauncher/DLWindow");
			lblProgress.Text = strings.GetString("tm_dl_progress") + ": 0%";
		}

		public void download()
		{
			WebClient client = new WebClient();
			client.DownloadProgressChanged += client_DownloadProgressChanged;
			client.DownloadFileCompleted += client_DownloadFileCompleted;
			client.DownloadFileAsync(new Uri("https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/texmod/texmod.zip"), @".\texmod.zip");
		}

		void client_DownloadFileCompleted(object sender, System.ComponentModel.AsyncCompletedEventArgs e)
		{
			lblProgress.Text = strings.GetString("tm_dl_done");
			SetMarqueeProgress();
			Unzip(melocation);
			if (MessageBox.Show(strings.GetString("mb_tm_done"), strings.GetString("mb_tm_title"), MessageBoxButton.OK, MessageBoxImage.Information) == MessageBoxResult.OK)
			{
				dlw.WriteLog("Terminating download window");
				dlw.Close();
				this.Close();
			}
		}

		void client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
		{
			double bytesIn = double.Parse(e.BytesReceived.ToString());
			double totalBytes = double.Parse(e.TotalBytesToReceive.ToString());
			double percent = bytesIn / totalBytes * 100;
			update(percent);
		}

		public void SetMarqueeProgress()
		{
			pbProgress.IsIndeterminate = true;
			pbProgress.Maximum = 0;
			pbProgress.Value = 0;
		}

		public void Unzip(string melocation)
		{
			ZipFile.ExtractToDirectory(@".\texmod.zip", @".\tm");
			File.Move(@".\tm\Texmod.exe", melocation + @"TexMod.exe");
			File.Delete(@".\texmod.zip");
			Directory.Delete(@".\tm", true);
		}

		public void update(double percent)
		{
			if (!Dispatcher.CheckAccess())
				Dispatcher.Invoke(() => { update(percent); });
			else
			{
				if (percent == 100)
				{
					lblProgress.Text = strings.GetString("tm_dl_done");
					Thread.Sleep(1000);
				}
				else
				{
					pbProgress.Value = int.Parse(Math.Truncate(percent).ToString());
					string ptext = pbProgress.Value.ToString();
					lblProgress.Text = String.Format("{0} {1} %", strings.GetString("tm_dl_progress"), ptext);
				}
			}
		}
	}
}
