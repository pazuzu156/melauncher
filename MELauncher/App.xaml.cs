﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Reflection;
using MEL.Settings;
using System.IO;
using System.Net;
using System.ComponentModel;
using System.Diagnostics;
using System.Web.Script.Serialization;
using System.Threading;
using Microsoft.Win32;

namespace MELauncher
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
		public string Version;
		public static bool updated { get; private set; }
		private bool appRestart = false;

		public static bool LaunchFromShortcut { get; private set; }
		public static string LaunchGameFromShortcut { get; private set; }

		public static JsonData jsonData {get; private set; }

		private Dictionary<string, string> args = new Dictionary<string, string>();

		private static Mutex _mutex = null;

        public App()
        {
			Version = GetVersion();
			updated = false;
			string[] cmdArgs = Environment.GetCommandLineArgs();
			jsonData = new JsonData();

			// Loop through arguments, add to dict.
			if(cmdArgs.Length > 0)
			{
				for(int i = 0; i < cmdArgs.Length; i++)
				{
					if(cmdArgs[i].StartsWith("/"))
					{
						string arg = cmdArgs[i];
						string val = "";
						try
						{
							if (!cmdArgs[i + 1].StartsWith("/"))
								val = cmdArgs[i + 1];
						}
						catch
						{
							val = "";
						}
						args.Add(arg, val);
					}
					else if(cmdArgs[i].StartsWith("melauncher:"))
					{
						string cmd = cmdArgs[i].Replace("melauncher://", "");
						string[] parts = cmd.Split(new[] { '/' });
						LaunchFromShortcut = true;
						LaunchGameFromShortcut = parts[2];
					}
				}
			}

			// Loop through dict. keys
			foreach(var key in args.Keys)
			{
				switch(key.ToLower())
				{
					case "/uninstall":
						removeAll();
						break;
					case "/updated":
						updated = true;
						break;
					case "/apprestart":
						appRestart = true;
						break;
				}
			}

			start();
        }

		private string GetVersion()
		{
			return Assembly.GetExecutingAssembly().GetName().Version.ToString();
		}

        private void removeAll()
        {
            // Clean up TexMod from system
            Settings s = Settings.getInstance();
            s.LoadSettings();

            if (File.Exists(Settings.MELocation + @"\Binaries\TexMod.exe"))
                File.Delete(Settings.MELocation + @"\Binaries\TexMod.exe");
            if (File.Exists(Settings.ME2Location + @"\Binaries\TexMod.exe"))
                File.Delete(Settings.ME2Location + @"\Binaries\TexMod.exe");
            if (File.Exists(Settings.ME3Location + @"\Binaries\Win32\TexMod.exe"))
                File.Delete(Settings.ME3Location + @"\Binaries\Win32\TexMod.exe");

			string[] scs = Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\");
			foreach (string file in scs)
			{
				if (file.Contains("(MELauncher).url"))
				{
					File.Delete(file);
				}
			}

			string a = "b";

			try
			{
				RegistryKey key = Registry.ClassesRoot.OpenSubKey("MELauncher", true);
				key.DeleteSubKey("DefaultIcon");
				key.DeleteSubKey("shell\\open\\command");
				key.DeleteSubKey("shell\\open");
				key.DeleteSubKey("shell");
				Registry.ClassesRoot.DeleteSubKey("MELauncher");
			}
			catch(Exception e)
			{
				MessageBox.Show(e.ToString());
			}

            s.SaveSettings("", "", "", "", "", "", "", "", Settings.DRMType.NULL, Settings.ThemeType.Light, "", false, false, false);

			App.Current.Shutdown();
        }

        private void start()
        {
            Settings s = Settings.getInstance();
            s.LoadSettings();

            // Force correct version in settings file
            // Generate app id if not exists
            Guid appID;
            if (Settings.AppID.Equals(""))
                appID = Guid.NewGuid();
            else
                appID = Guid.Parse(Settings.AppID);

            
            s.SaveSettings(
                appID.ToString(),
                Version,
                Settings.SteamLocation,
                Settings.OriginLocation,
                Settings.MELocation,
                Settings.ME2Location,
                Settings.ME3Location,
				Settings.MEALocation,
                Settings.DRM,
                Settings.Theme,
                Settings.Language,
                Settings.CloseOnRun,
                Settings.AOT,
                Settings.StartUp
            );

			string appName = "MELauncher-" + Settings.AppID;

			_mutex = new Mutex(false, appName);

			MEL.Locale.Strings strings = MEL.Locale.Strings.Load(Settings.Language);

			if (!appRestart)
			{
				if (!_mutex.WaitOne(TimeSpan.FromSeconds(0.5), false))
				{
					MessageBox.Show(strings.GetString("mb_new_instance"), strings.GetString("mb_new_instance_title"), MessageBoxButton.OK, MessageBoxImage.Error);
					_mutex.ReleaseMutex();
					Application.Current.Shutdown();
				}
			}

			UpdateURLSchemeRegistryEntries();
        }

		private void UpdateURLSchemeRegistryEntries()
		{
			RegistryKey urlBaseKey = Registry.ClassesRoot.OpenSubKey("MELauncher", true);

			try
			{
				Registry.ClassesRoot.CreateSubKey("MELauncher");
				Registry.ClassesRoot.CreateSubKey(@"MELauncher\DefaultIcon");
				Registry.ClassesRoot.CreateSubKey(@"MELauncher\shell\open\command");
			}
			catch { }

			urlBaseKey = Registry.ClassesRoot.OpenSubKey("MELauncher", true);
			RegistryKey iconKey = Registry.ClassesRoot.OpenSubKey(@"MELauncher\DefaultIcon", true);
			RegistryKey shellKey = Registry.ClassesRoot.OpenSubKey(@"MELauncher\shell\open\command", true);

			string def = (string)urlBaseKey.GetValue("", "");
			if(def.Equals(""))
			{
				urlBaseKey.SetValue("", "URL:melauncher Protocol");
				urlBaseKey.SetValue("URL Protocol", "");
				iconKey.SetValue("", "MELauncher.exe,1");

				string MELFile = Environment.CurrentDirectory + @"\MELauncher.exe";
				var opencmdKey = shellKey.GetValue("");

				if (opencmdKey == null || opencmdKey.ToString().Equals(MELFile))
				{
					var ass = Assembly.GetExecutingAssembly();
					shellKey.SetValue("", string.Format("\"{0}\" \"%1\"",
						ass.Location));
				}
			}
		}

        private void downloadUpdater()
        {
            if (File.Exists(@"Updater.exe"))
                File.Move(@"Updater.exe", @"Updater.exe.old");
            WebClient client = new WebClient();
            client.DownloadFile("http://cdn.kalebklein.com/mel/Updater.exe", @"Updater.exe");
        }
    }
}
