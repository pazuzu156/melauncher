﻿using MEL.Settings;
using MEL.Locale;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Diagnostics;

namespace MELauncher
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        Settings settings;
        public MainWindow()
        {
            settings = Settings.getInstance();
            settings.LoadSettings();

            if(Settings.DRM == Settings.DRMType.Origin)
            {
                new OriginWindow().Show();
                this.Close();
            }
            else if(Settings.DRM == Settings.DRMType.Steam)
            {
                new SteamWindow().Show();
                this.Close();
            }

            InitializeComponent();

            string theme = (Settings.Theme == Settings.ThemeType.Light) ? "Light" : "Dark";

			App.Current.Resources.MergedDictionaries.Add(
				new ResourceDictionary { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme." + theme + ".xaml", UriKind.Absolute) });

            SetLocale();

            //Notifications.NotificationService.StartService(this);
        }

        private void SetLocale()
        {
            var strings = Strings.Load(Settings.Language);
            var buttons = Buttons.Load(Settings.Language);
            var titles = Titles.Load(Settings.Language);
            this.Title = titles.GetTitle("MELauncher/MainWindow");
            lblMainWindow.Text = strings.GetString("lblMainWindow");
            bOrigin.Content = buttons.GetButton("origin");
            bSteam.Content = buttons.GetButton("steam");
        }

        private void bOrigin_Click(object sender, RoutedEventArgs e)
        {
            SaveSettings(Settings.DRMType.Origin);
            new OriginWindow().Show();
            this.Close();
        }

        private void bSteam_Click(object sender, RoutedEventArgs e)
        {
            SaveSettings(Settings.DRMType.Steam);
            new SteamWindow().Show();
            this.Close();
        }

        private void SaveSettings(Settings.DRMType drm)
        {
            settings.SaveSettings(
                Settings.AppID,
                Settings.Version,
                Settings.SteamLocation,
                Settings.OriginLocation,
                Settings.MELocation,
                Settings.ME2Location,
                Settings.ME3Location,
				Settings.MEALocation,
                drm,
                Settings.Theme,
                Settings.Language,
                Settings.CloseOnRun,
                Settings.AOT,
                Settings.StartUp
            );
        }
    }
}
