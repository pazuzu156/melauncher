﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using MEL.Settings;
using MEL.Locale;
using System.Net;
using System.IO;
using System.Security.Cryptography;

namespace MELauncher
{
    /// <summary>
    /// Interaction logic for ChangelogWindow.xaml
    /// </summary>
    public partial class ChangelogWindow
    {
        Settings settings;
        string changelog;
        System.Windows.Threading.Dispatcher disp;

        Titles titles;
        Buttons buttons;
		Strings strings;

		string theme;

		bool dirtyClose = false;

        public ChangelogWindow()
        {
            InitializeComponent();

            settings = Settings.getInstance();
            settings.LoadSettings();

            changelog = Settings.ChangelogFile;

            if (Settings.AOT)
                this.Topmost = true;

			theme = (Settings.Theme == Settings.ThemeType.Light) ? "Light" : "Dark";

			App.Current.Resources.MergedDictionaries.Add(
				new ResourceDictionary { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme." + theme + ".xaml", UriKind.Absolute) });

            disp = tbChangelog.Dispatcher;

			SetLocale();

            loadChangelog();
        }

        private void SetLocale()
        {
            titles = Titles.Load(Settings.Language);
            buttons = Buttons.Load(Settings.Language);
			strings = Strings.Load(Settings.Language);

            this.Title = titles.GetTitle("MELauncher/ChangelogWindow");
            bClose.Content = buttons.GetButton("closeCL");
        }

        private void loadChangelog()
        {
			MD5 md5 = MD5.Create();
			byte[] hashBytes = new UTF8Encoding().GetBytes(theme.ToLower());
			byte[] hashedBytes = md5.ComputeHash(hashBytes);
			string hash = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();

			if (new StatusCheck("http://cdn.kalebklein.com/mel/v5/changelog.php").IsConnected)
			{
				downloadTask = Task.Factory.StartNew(() => loadChangelogFromWeb("http://cdn.kalebklein.com/mel/v5/changelog.php?d=" + hash));
			}
			else
				LoadCachedChangelog();

			if (dirtyClose)
				this.Close();
        }

        Task downloadTask;
        private void loadChangelogFromWeb(string url)
        {
            try
            {
				WebBrowserNavigate(url);

				WebRequest r = WebRequest.Create(url);
				WebResponse re = r.GetResponse();
				using(var reader = new StreamReader(re.GetResponseStream()))
				{
					string line, resp = "";
					while((line = reader.ReadLine()) != null)
					{
						resp += line + "\r\n";
					}
					using(var writer = new StreamWriter(File.Open(changelog, FileMode.OpenOrCreate, FileAccess.Write)))
					{
						writer.Write(resp);
					}
				}
            }
            catch(Exception ex)
            {
                MessageBox.Show("Error: " + ex);
                if (!Dispatcher.CheckAccess())
                    Dispatcher.Invoke(new LoadCachedChangelogDelegate(LoadCachedChangelog));
                else
                    LoadCachedChangelog();
            }
        }

		private delegate void d_WebBrowserNavigate(string url);

		private void WebBrowserNavigate(string url)
		{
			if (!Dispatcher.CheckAccess())
				Dispatcher.Invoke(new d_WebBrowserNavigate(WebBrowserNavigate), url);
			else
				tbChangelog.Navigate(url);
		}
        private delegate void LoadCachedChangelogDelegate();

        private void LoadCachedChangelog()
        {
			if(File.Exists(changelog))
			{
				tbChangelog.Navigate(changelog);
			}
			else
			{
				MessageBox.Show(strings.GetString("mb_error_change"), strings.GetString("mb_title_error"), MessageBoxButton.OK, MessageBoxImage.Error);
				dirtyClose = true;
			}
        }

        private void bClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
