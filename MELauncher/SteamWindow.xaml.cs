﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WinForms = System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using Microsoft.Win32;
using MahApps.Metro.Controls.Dialogs;

using MEL.Settings;
using MEL.Locale;
using MEL.Logger;

namespace MELauncher
{
    /// <summary>
    /// Interaction logic for SteamWindow.xaml
    /// </summary>
    public partial class SteamWindow
    {
        #region Variables
        Settings settings;
        Titles titles;
        Strings strings;
        Buttons buttons;
        RadioButtons radioButtons;
        bool isTexModEnabled = false,
            hasSteam = false,
            hasME = false,
            hasME2 = false,
            autoDetect = false,
            tempAutoClose = false,
            isResetting = false;
        private volatile bool threadRunning = false;
        private volatile bool closeWindow = false;
        private Task listenTask;
        GameSelected gamesel;

        LoggerWindow logger;

		MetroDialogSettings mds = new MetroDialogSettings() { ColorScheme = MetroDialogColorScheme.Accented };
        #endregion

        #region Delegates
        // Delegate for killing the TM Listening thread
        private delegate void d_KillThread();
        private delegate void d_WriteLog(string write);
        #endregion

        #region Enums
        // This is for setting the game lock when a game is selected
        private enum GameSelected
        {
            MESelected,
            ME2Selected,
            NoneSelected
        }
        #endregion

        #region Constructor
        public SteamWindow()
        {
            settings = Settings.getInstance();
            settings.LoadSettings();

            InitializeComponent();

            if (Settings.AOT)
                this.Topmost = true;

            titles = Titles.Load(Settings.Language);
            strings = Strings.Load(Settings.Language);
            buttons = Buttons.Load(Settings.Language);
            radioButtons = RadioButtons.Load(Settings.Language);

            SetLocale();

            if(!Settings.SteamLocation.Equals(""))
            {
                tbSteam.Text = Settings.SteamLocation;
                tbME.Text = Settings.MELocation;
                tbME2.Text = Settings.ME2Location;

                checkLocations();
            }

			string theme = (Settings.Theme == Settings.ThemeType.Light) ? "Light" : "Dark";

			App.Current.Resources.MergedDictionaries.Add(
				new ResourceDictionary { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme." + theme + ".xaml", UriKind.Absolute) });

            DataContext = this;

			//Task.Factory.StartNew(() => { new UpdateChecker(Settings.Version, true); });

			new UpdateChecker(Settings.Version, true);

            runCrashHandler(); // Crash handling

			if (App.updated)
				AlertUpdated();

			if (App.LaunchFromShortcut)
				LaunchFromShortcut();
        }
        #endregion

        #region Extra Methods
        private void SetLocale()
        {
            this.Title = titles.GetTitle("MELauncher/SteamWindow");
            lblSteamBase.Text = strings.GetString("steamBase");
            lblMEBase.Text = strings.GetString("MEBase");
            lblME2Base.Text = strings.GetString("ME2Base");
            gbGames.Header = strings.GetString("gamesGroup");

            bBrowseSteam.Content = buttons.GetButton("browse");
            bBrowseME.Content = buttons.GetButton("browse");
            bBrowseME2.Content = buttons.GetButton("browse");

            bSettings.Content = buttons.GetButton("settings");
            bCheckLocations.Content = buttons.GetButton("checkLocations");
            bAutoDetect.Content = buttons.GetButton("autoDetect");
            bEnable.Content = buttons.GetButton("enable");
            bClose.Content = buttons.GetButton("closeME");
            bStLaunch.Content = buttons.GetButton("steamLaunch");

            rbME.Content = radioButtons.GetButton("me");
            rbME2.Content = radioButtons.GetButton("me2");
        }

		private void AlertUpdated()
		{
			new ChangelogWindow().ShowDialog();
			//MessageBox.Show(strings.GetString("mb_updated"), strings.GetString("mb_updated_title"));
		}
        #endregion

        #region Main Component Methods
        private void bSettings_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Settings button clicked");
            new SettingsWindow(logger).ShowDialog();
        }

        private void bBrowseSteam_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Steam browser button clicked");
            if(bBrowseSteam.Content.Equals(buttons.GetButton("open")))
            {
                WriteLog("Opening Steam location in Windows Explorer");
                ProcessStartInfo psi = new ProcessStartInfo();
                psi.FileName = Environment.GetFolderPath(Environment.SpecialFolder.Windows) + @"\explorer.exe";
                psi.Arguments = Settings.SteamLocation;
                Process.Start(psi);
            }
            else
            {
                WriteLog("Opening folder explorer dialog");
                string path = BrowseForLocation();
                if (path != null)
                    tbSteam.Text = path;
            }
        }

        private void bBrowseME_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Mass Effect browsing button clicked");
            if(bBrowseME.Content.Equals(buttons.GetButton("open")))
            {
                WriteLog("Opening selection window");
                new SelectOpenWindow(Settings.MELocation, "steam", logger).ShowDialog();
            }
            else
            {
                WriteLog("Opening folder explorer dialog");
                string path = BrowseForLocation();
                if (path != null)
                    tbME.Text = path;
            }
        }

        private void bBrowseME2_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Mass Effect 2 browser button clicked");
            if(bBrowseME2.Content.Equals(buttons.GetButton("open")))
            {
                WriteLog("Opening selection window");
                new SelectOpenWindow(Settings.ME2Location, "steam", logger).ShowDialog();
            }
            else
            {
                WriteLog("Opening folder explorer dialog");
                string path = BrowseForLocation();
                if (path != null)
                    tbME2.Text = path;
            }
        }

        private void bClose_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Close button clicked");
            this.Close();
        }

        private void bCheckLocations_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Check locations button clicked");
            checkLocations();
        }

        private void bAutoDetect_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Auto detect button clicked");
            autoDetect = true;
            findLocations();
        }

        private void bEnable_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Enable TexMod button clicked");
            if (!isTexModEnabled)
            {
                WriteLog("Starting TexMod enabling process");
                enable();
            }
            else
            {
                WriteLog("Disabling TexMod");
                if (listenTask != null && threadRunning == true)
                {
                    closeWindow = false;
                    threadRunning = false;
                }
            }
        }

        private async void bStLaunch_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Game launch button clicked");
            string game = "";
            if ((bool)rbME.IsChecked)
                game = "me";
            else if ((bool)rbME2.IsChecked)
                game = "me2";
			else
				await this.ShowMessageAsync(strings.GetString("nogamesel_title"), strings.GetString("nogamesel_notm"), MessageDialogStyle.Affirmative, mds);

            WriteLog("Launching game selection window");
			//var gameID = Settings.SteamGameIDs;
			//new SelectRunWindow(game, gameID[game], "steam", logger).ShowDialog();
			
			
			if(game.Equals("me") || game.Equals("me2"))
			{
				var gameID = Settings.SteamGameIDs;
				//new SelectRunWindow(game, gameID[game], "origin", logger).ShowDialog();
				var win = new SelectRunWindow(game, gameID[game], "steam", logger);
				if (isTexModEnabled)
				{
					win.ShowDialog();
				}
				else
				{
					win.RunGame();
				}
			}
        }

        private void BugReporterCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            WriteLog("Launching bug reporting tool");
            new BugReporter(logger).ShowDialog();
        }

        private void SteamWindowName_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            WriteLog("Starting application shutdown procedure");
            if (isTexModEnabled)
            {
                WriteLog("TexMod is still enabled, disabling");
                if (
                    File.Exists(Settings.MELocation + @"\Binaries\mel.backup") ||
                    File.Exists(Settings.ME2Location + @"\Binaries\mel.backup") ||
                    File.Exists(Settings.ME3Location + @"\Binaries\Win32\mel.backup")
                  )
                {
                    MessageBox.Show(strings.GetString("texmod_still_on"), strings.GetString("texmod_still_on_title"), MessageBoxButton.OK, MessageBoxImage.Warning);
                    threadRunning = false;
                    stopListening();
                }
            }
            WriteLog("Terminating application");

            if (this.isResetting)
            {
                this.isResetting = false;
                new SteamWindow().Show();
            }
            else
            {
                App.Current.Shutdown();
            }
        }

        private async void HelpCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            WriteLog("Preparing to launch compiled help file");
			bool error = false;
            try
            {
                WriteLog("Launching compiled help file");
                Process.Start("MELauncherHelp.chm");
            }
            catch (Exception ex)
            {
                WriteLog("Error opening compiled help file. Error: " + ex.Message);
				//MessageBox.Show(strings.GetString("no_help"), strings.GetString("mb_error_title"), MessageBoxButton.OK, MessageBoxImage.Error);
				error = true;
            }

			if(error)
				await this.ShowMessageAsync(strings.GetString("mb_error_title"), strings.GetString("no_help"), MessageDialogStyle.Affirmative, mds);
        }

        private void LoggerCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            logger = new LoggerWindow("steam");
            logger.Closing += logger_Closing;
            logger.Show();
        }

        private void ReloadCommand_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            this.isResetting = true;
            this.Close();
        }
        #endregion

        #region Event Called Methods
        private void WriteLog(string write)
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(new d_WriteLog(WriteLog), write);
            else
            {
                if (logger != null)
                    logger.WriteToLog(write);
            }
        }

        private void logger_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            WriteLog("Terminating logger");
            logger.WriteToLogFile();
            logger = null;
        }

        private String BrowseForLocation()
        {
            WinForms.FolderBrowserDialog fbd = new WinForms.FolderBrowserDialog();
            WinForms.DialogResult result = fbd.ShowDialog();
            if (result == WinForms.DialogResult.OK)
            {
                WriteLog("Directory selected: " + fbd.SelectedPath);
                return fbd.SelectedPath;
            }
            WriteLog("User canceled browsing action");
            return null;
        }

        private async void checkLocations()
        {
            WriteLog("Checking supplied locations");
            string sl = tbSteam.Text;
            string ml = tbME.Text;
            string m2l = tbME2.Text;

            WriteLog("Initializing location detector");

            WriteLog("Checking if supplied Steam location exists");
            hasSteam = LocationDetector.CheckLocation(sl + @"\Steam.exe", tbSteam, bBrowseSteam, lblSteamCheck);
            if (!hasSteam)
                WriteLog("Steam was not found");

            hasME = LocationDetector.CheckLocation(ml + @"\Binaries\MassEffect.exe", tbME, bBrowseME, lblMECheck);
            if (!hasME)
                WriteLog("Mass Effect was not found");

            hasME2 = LocationDetector.CheckLocation(m2l + @"\Binaries\MassEffect2.exe", tbME2, bBrowseME2, lblME2Check);
            if (!hasME2)
                WriteLog("Mass Effect 2 was not found");

            WriteLog("Detection complete");
            if(hasSteam && (hasME || hasME2))
            {
                bEnable.Visibility = System.Windows.Visibility.Visible;
                bStLaunch.Visibility = System.Windows.Visibility.Visible;
                gbGames.Visibility = System.Windows.Visibility.Visible;

                string message = null;
                if (hasME2 && hasME)
                {
                    WriteLog("All supplied locations were found");
                    message = strings.GetString("mb_ad_s_all");
                }
                else if (hasME2)
                {
                    WriteLog("Locations for Steam and Mass Effect 2 were found");
                    message = strings.GetString("mb_ad_s_2");
                }
                else if (hasME)
                {
                    WriteLog("Locations for Steam and Mass Effect were found");
                    message = strings.GetString("mb_ad_s_1");
                }

                WriteLog("Writing new locations to application settings");
                settings.SaveSettings(Settings.AppID,
                    Settings.Version,
                    sl,
                    Settings.OriginLocation,
                    ml, m2l,
                    Settings.ME3Location,
					Settings.MEALocation,
                    Settings.DRM,
                    Settings.Theme,
                    Settings.Language,
                    Settings.CloseOnRun,
                    Settings.AOT,
                    Settings.StartUp);

				if (autoDetect)
					//MessageBox.Show(message, strings.GetString("mb_ad_title"), MessageBoxButton.OK, MessageBoxImage.Information);
					await this.ShowMessageAsync(strings.GetString("mb_ad_title"), message, MessageDialogStyle.Affirmative, mds);
            }
        }

        private async void findLocations()
        {
            string sl;
            string ml;
            string m2l;

            WriteLog("Initializing location detector");
            LocationDetector sld = new LocationDetector();
            LocationDetector mld = new LocationDetector();
            LocationDetector m2ld = new LocationDetector();

            LocationDetector.SetText(lblSteamCheck, strings.GetString("searching"), Brushes.Brown);
            LocationDetector.SetText(lblMECheck, strings.GetString("searching"), Brushes.Brown);
            LocationDetector.SetText(lblME2Check, strings.GetString("searching"), Brushes.Brown);

            WriteLog("Sniffing registry for Steam's location");
            sld.SniffForLocation(@"SOFTWARE\Wow6432Node\Valve\Steam",
                sld.ItemsList(new String[] { "location" }, new String[] { "InstallPath" }), "steam");
            sl = sld.getLocation();

            WriteLog("Sniffing registry for Mass Effect's location");
            mld.SniffForLocation(@"SOFTWARE\Wow6432Node\BioWare\Mass Effect",
                mld.ItemsList(new String[] { "location", "steam" }, new String[] { "path", "flavour" }), "steam");
            ml = mld.getLocation();

            WriteLog("Sniffing registry for Mass Effect 2's location");
            m2ld.SniffForLocation(@"SOFTWARE\Wow6432Node\BioWare\Mass Effect 2",
                m2ld.ItemsList(new String[] { "location", "steam" }, new String[] { "Path", "flavour" }), "steam");
            m2l = m2ld.getLocation();

            if (sl != null)
            {
                WriteLog("Steam found in Windows registry");
                tbSteam.Text = sl;
            }
            else
            {
                WriteLog("Origin was not found in Windows registry!");
                //MessageBox.Show(strings.GetString("mb_no_steam"));
				await this.ShowMessageAsync(strings.GetString("mb_check_locations"), strings.GetString("mb_no_steam"), MessageDialogStyle.Affirmative, mds);
                LocationDetector.SetText(lblSteamCheck, strings.GetString("nfound"), Brushes.DarkRed);
            }

            if (ml != null)
            {
                if (mld.IsSteamFlavor())
                {
                    WriteLog("Mass Effect was found in the Windows registry");
                    tbME.Text = ml.TrimEnd('\\');
                }
                else
                {
                    WriteLog("Mass Effect was found in the Windows registry, however it's the Origin version. Disregarding registry entry");
					//MessageBox.Show(strings.GetString("mb_no_me_steam"));
					await this.ShowMessageAsync(strings.GetString("mb_check_locations"), strings.GetString("mb_no_me_steam"), MessageDialogStyle.Affirmative, mds);
                    LocationDetector.SetText(lblMECheck, strings.GetString("nfound"), Brushes.Red);
                }
            }
            else
            {
                WriteLog("Mass Effect was now found in the Windows registry!");
				//MessageBox.Show(strings.GetString("mb_no_me"));
				await this.ShowMessageAsync(strings.GetString("mb_check_locations"), strings.GetString("mb_no_me"), MessageDialogStyle.Affirmative, mds);
                LocationDetector.SetText(lblMECheck, strings.GetString("nfound"), Brushes.Red);
            }

            if (m2l != null)
            {
                if(m2ld.IsSteamFlavor())
                {
                    WriteLog("Mass Effect 2 was found in the Windows registry");
                    tbME2.Text = m2l.TrimEnd('\\');
                }
                else
                {
                    WriteLog("Mass Effect 2 was found in the Windows registry, however it's the Origin version. Disregarding registry entry");
					//MessageBox.Show(strings.GetString("mb_no_me2_steam"));
					await this.ShowMessageAsync(strings.GetString("mb_check_locations"), strings.GetString("mb_no_me2_steam"), MessageDialogStyle.Affirmative, mds);
                    LocationDetector.SetText(lblME2Check, strings.GetString("nfound"), Brushes.Red);
                }
            }
            else
            {
                WriteLog("Mass Effect was now found in the Windows registry!");
				//MessageBox.Show(strings.GetString("mb_no_me2"));
				await this.ShowMessageAsync(strings.GetString("mb_check_locations"), strings.GetString("mb_no_me2"), MessageDialogStyle.Affirmative, mds);
                LocationDetector.SetText(lblME2Check, strings.GetString("nfound"), Brushes.Red);
            }

            WriteLog("Beginning location checking of locations obtained from the Windows registry");
            checkLocations();
        }

        private void runCrashHandler()
        {
            WriteLog("Checking if the application exited successfully in the previous session");
            if(File.Exists(Settings.CrashHandlerFile))
            {
                WriteLog("Application crash handler detected an unexpected exit");
                if(MessageBox.Show(strings.GetString("crashHandlerErrorMessage"), strings.GetString("mb_error_title"), MessageBoxButton.OK, MessageBoxImage.Error) == MessageBoxResult.OK)
                {
                    WriteLog("Reverting changes from previous session according the application crash handler");
                    disable();
                    File.Delete(Settings.CrashHandlerFile);
                }
            }
        }

        private void StartUp(bool check)
        {
            RegistryKey startupKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true);
            if (check)
            {
                WriteLog("Writing startup key to registry");
                startupKey.SetValue("MELauncher", Environment.CurrentDirectory + @"\MELauncher.exe");
            }
            else
            {
                WriteLog("Removing startup key from registry");
                startupKey.DeleteValue("MELauncher", false);
            }
        }

		private void LaunchFromShortcut()
		{
			tempAutoClose = true;
			switch (App.LaunchGameFromShortcut)
			{
				case "me":
					gamesel = GameSelected.MESelected;
					rbME.IsChecked = true;
					break;
				case "me2":
					gamesel = GameSelected.ME2Selected;
					rbME2.IsChecked = true;
					break;
			}
			//bEnable_Click(null, null);
			//bStLaunch_Click(null, null);

			SelectRunWindow win = new SelectRunWindow(App.LaunchGameFromShortcut, Settings.SteamGameIDs[App.LaunchGameFromShortcut], "steam", logger, true);
			win.ShowDialog();

			if (!win.DisableTexMod)
			{
				bEnable_Click(null, null);
				win.RunGame();
			}
		}
        #endregion

        #region Enable / Disable Methods
        private async void enable()
        {
            WriteLog("Initializing TexMod enable methods");
            GameSelected gs = GameSelected.NoneSelected;
            if ((bool)rbME.IsChecked)
            {
                WriteLog("Mass Effect is currently selected");
                gs = GameSelected.MESelected;
            }
            else if ((bool)rbME2.IsChecked)
            {
                WriteLog("Mass Effect 2 is currently selected");
                gs = GameSelected.ME2Selected;
            }

            if(gamesel == GameSelected.NoneSelected && File.Exists(Settings.CrashHandlerFile))
            {
                WriteLog("Error: TexMod is already enabled");
				//MessageBox.Show(strings.GetString("tmAlreadyEnabled"), strings.GetString("mb_error_title"), MessageBoxButton.OK, MessageBoxImage.Error);
				await this.ShowMessageAsync(strings.GetString("mb_error_title"), strings.GetString("tmAlreadyEnabled"), MessageDialogStyle.Affirmative, mds);
            }
            else
            {
                gamesel = gs;
                bool enabled = false;

                switch(gs)
                {
                    case GameSelected.MESelected:
                        if (hasME)
                        {
                            WriteLog("Checking if TexMod is installed for Mass Effect");
                            if (File.Exists(Settings.MELocation + @"\Binaries\TexMod.exe"))
                            {
                                WriteLog("TexMod was found. Initializing replacements");

                                WriteLog("Replacing MassEffect.exe with TexMod, backing up game executable");
                                File.Move(Settings.MELocation + @"\Binaries\MassEffect.exe", Settings.MELocation + @"\Binaries\mel.backup");
                                File.Move(Settings.MELocation + @"\Binaries\TexMod.exe", Settings.MELocation + @"\Binaries\MassEffect.exe");

                                WriteLog("Initializing game launch thread");
                                ListenForTexModLaunch("me");
                                enabled = true;
                            }
                            else
                            {
                                WriteLog("TexMod was not found, launching TexMod downloader window");
                                new TexModDownloadWindow(Settings.OriginLocation, Settings.MELocation + @"\Binaries\", logger).ShowDialog();
                            }
                        }
                        else
                        {
                            WriteLog("Mass Effect is not currently installed onto the system");
							//MessageBox.Show(strings.GetString("ni_me"), "MELauncher", MessageBoxButton.OK, MessageBoxImage.Error);
							await this.ShowMessageAsync(strings.GetString("mb_error_title"), strings.GetString("ni_me"), MessageDialogStyle.Affirmative, mds);
                        }
                        break;
                    case GameSelected.ME2Selected:
                        if (hasME2)
                        {
                            WriteLog("Checking if TexMod is installed for Mass Effect 2");
                            if (File.Exists(Settings.ME2Location + @"\Binaries\TexMod.exe"))
                            {
                                WriteLog("TexMod was found. Initializing replacements");

                                WriteLog("Replacing ME2Game.exe with TexMod, backing up game executable");
                                File.Move(Settings.ME2Location + @"\Binaries\MassEffect2.exe", Settings.ME2Location + @"\Binaries\mel.backup");
                                File.Move(Settings.ME2Location + @"\Binaries\TexMod.exe", Settings.ME2Location + @"\Binaries\MassEffect2.exe");

                                WriteLog("Initializing game launch thread");
                                ListenForTexModLaunch("me2");
                                enabled = true;
                            }
                            else
                            {
                                WriteLog("TexMod was not found, launching TexMod downloader window");
                                new TexModDownloadWindow(Settings.OriginLocation, Settings.ME2Location + @"\Binaries\", logger).ShowDialog();
                            }
                        }
                        else
                        {
                            WriteLog("Mass Effect 2 is not currently installed onto the system");
							//MessageBox.Show(strings.GetString("ni_me2"), "MELauncher", MessageBoxButton.OK, MessageBoxImage.Error);
							await this.ShowMessageAsync(strings.GetString("mb_error_title"), strings.GetString("ni_me2"), MessageDialogStyle.Affirmative, mds);
                        }
                        break;
                    case GameSelected.NoneSelected:
						//MessageBox.Show(strings.GetString("nogamesel"), strings.GetString("nogamesel_title"), MessageBoxButton.OK, MessageBoxImage.Error);
						await this.ShowMessageAsync(strings.GetString("nogamesel_title"), strings.GetString("nogamesel"), MessageDialogStyle.Affirmative, mds);
                        break;
                }

                if (enabled)
                {
                    WriteLog("TexMod is enabled, taking care of a few UI changes to reflect it");
                    bEnable.Content = buttons.GetButton("disable");
                    bStLaunch.IsEnabled = true;
                    bClose.IsEnabled = false;
                    bSettings.IsEnabled = false;
                    bCheckLocations.IsEnabled = false;
                    bAutoDetect.IsEnabled = false;
                    gbGames.IsEnabled = false;
                    isTexModEnabled = true;

                    WriteLog("Starting up crash handler");
                    FileStream fs = File.Open(Settings.CrashHandlerFile, FileMode.OpenOrCreate, FileAccess.ReadWrite);
                    StreamWriter writer = new StreamWriter(fs);
                    writer.WriteLine(gs);
                    writer.Flush();
                    writer.Dispose();
                    fs.Close();

                    WriteLog("Initializing application startup flow upon app crash");
                    if (!Settings.StartUp)
                        StartUp(true);
                }
            }
        }

        private void disable()
        {
            WriteLog("Initializing TexMod disable methods");
            GameSelected gs = (gamesel == GameSelected.NoneSelected) ? GameSelected.NoneSelected : gamesel;
            if ((bool)rbME.IsChecked)
                gs = GameSelected.MESelected;
            else if ((bool)rbME2.IsChecked)
                gs = GameSelected.ME2Selected;

            bool hasToRecheck = false;

            switch (gs)
            {
                case GameSelected.MESelected:
                    WriteLog("Making sure everything is as it should be for reverting");
                    if (File.Exists(Settings.MELocation + @"\Binaries\mel.backup"))
                    {
                        WriteLog("Reverting changes to MassEffect.exe");
                        File.Move(Settings.MELocation + @"\Binaries\MassEffect.exe", Settings.MELocation + @"\Binaries\TexMod.exe");
                        File.Move(Settings.MELocation + @"\Binaries\mel.backup", Settings.MELocation + @"\Binaries\MassEffect.exe");
                    }
                    break;
                case GameSelected.ME2Selected:
                    WriteLog("Making sure everything is as it should be for reverting");
                    if (File.Exists(Settings.ME2Location + @"\Binaries\mel.backup"))
                    {
                        WriteLog("Reverting changes to ME2Game.exe");
                        File.Move(Settings.ME2Location + @"\Binaries\MassEffect2.exe", Settings.ME2Location + @"\Binaries\TexMod.exe");
                        File.Move(Settings.ME2Location + @"\Binaries\mel.backup", Settings.ME2Location + @"\Binaries\MassEffect2.exe");
                    }
                    break;
                case GameSelected.NoneSelected:
                    WriteLog("No game was selected. This is indicative of an app crash, doing a second pass through");
                    hasToRecheck = true;
                    break;
            }

            FileStream fs = null;
            try
            {
                WriteLog("Checking integrity of crash handler");
                fs = new FileStream(Settings.CrashHandlerFile, FileMode.Open, FileAccess.Read);
            }
            catch (Exception)
            {
                WriteLog("Crash handler was corrupted. This happens when another instance interferes with another one, fixing...");
                if (MessageBox.Show(strings.GetString("secondInstance"), strings.GetString("mb_error_title"), MessageBoxButton.OK, MessageBoxImage.Error) == MessageBoxResult.OK)
                    this.Close();
            }
            StreamReader reader = new StreamReader(fs);
            string line = "";
            while ((line = reader.ReadLine()) != null)
            {
                switch (line)
                {
                    case "MESelected":
                        gamesel = GameSelected.MESelected;
                        break;
                    case "ME2Selected":
                        gamesel = GameSelected.ME2Selected;
                        break;
                    default:
                        gamesel = GameSelected.NoneSelected;
                        break;
                }
                break;
            }

            reader.Dispose();
            fs.Close();

            if (hasToRecheck)
                disable();
            else
            {
                bEnable.Content = buttons.GetButton("enable");
                bStLaunch.IsEnabled = false;
                bClose.IsEnabled = true;
                bSettings.IsEnabled = true;
                bCheckLocations.IsEnabled = true;
                bAutoDetect.IsEnabled = true;
                gbGames.IsEnabled = true;
                isTexModEnabled = false;

                File.Delete(Settings.CrashHandlerFile);

                if (!Settings.StartUp)
                    StartUp(false);

				if (closeWindow || tempAutoClose)
					this.Close();
            }
        }
        #endregion

        #region Threaded Methods
        private void ListenForTexModLaunch(string launched)
        {
            threadRunning = true;
            WriteLog("Starting game launch thread");
            listenTask = Task.Factory.StartNew(() => { listen(launched); stopListening(); });
        }

        private void listen(string launched)
        {
            string game = null;
            closeWindow = (Settings.CloseOnRun) ? true : false;
            switch(launched)
            {
                case "me":
                    game = "MassEffect";
                    break;
                case "me2":
                    game = "MassEffect2";
                    break;
            }

            WriteLog("Listening for game launch...");
            while(threadRunning)
            {
                Process[] p = Process.GetProcessesByName(game);
                if (p.Length >= 1)
                    threadRunning = false;
                System.Threading.Thread.Sleep(500);
            }
            WriteLog("Game launched. Terminating thread");

            return;
        }

        private void stopListening()
        {
            if (!Dispatcher.CheckAccess())
                Dispatcher.Invoke(new d_KillThread(disable));
            else
            {
                WriteLog("Thread terminated. Initializing TexMod disable methods");
                disable();
            }
        }
        #endregion
    }
}
