﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using MahApps.Metro.Controls.Dialogs;

using MEL.Logger;
using MEL.Settings;
using MEL.Locale;
using System.IO;

namespace MELauncher
{
    /// <summary>
    /// Interaction logic for SettingsWindow.xaml
    /// </summary>
    public partial class SettingsWindow
    {
        Settings settings;
        Strings strings;
        Titles titles;
        Buttons buttons;
        RadioButtons radioButtons;
        CheckBoxes checkBoxes;

        StringDictionary languages;

        LoggerWindow logger;

		MetroDialogSettings mds = new MetroDialogSettings() { ColorScheme = MetroDialogColorScheme.Accented };

        public SettingsWindow(LoggerWindow logger)
        {
            InitializeComponent();
            this.logger = logger;

            WriteLog("Initializing settings window");

            initWindow();
            SetLocale();

            if (Settings.AOT)
                this.Topmost = true;
        }

        private void WriteLog(string write)
        {
            if (logger != null)
                logger.WriteToLog(write);
        }

        private void SetLocale()
        {
            WriteLog("Setting settings window locale");
            strings = Strings.Load(Settings.Language);
            titles = Titles.Load(Settings.Language);
            buttons = Buttons.Load(Settings.Language);
            radioButtons = RadioButtons.Load(Settings.Language);
            checkBoxes = CheckBoxes.Load(Settings.Language);

            this.Title = titles.GetTitle("MELauncher/SettingsWindow");

            lblSettingsInfo.Text = strings.GetString("settingsInfo");
            gbThemes.Header = strings.GetString("themesGroup");
            lblLangSelect.Text = strings.GetString("langSelect");
            lblDonate.Text = strings.GetString("donate_text");

            rbThemeDark.Content = radioButtons.GetButton("themeDark");
            rbThemeLight.Content = radioButtons.GetButton("themeLight");
            cbStartup.Content = checkBoxes.GetButton("startup");
            cbAOT.Content = checkBoxes.GetButton("toplevel");
            cbCloseOnComplete.Content = checkBoxes.GetButton("closeOnLaunch");

            bCancel.Content = buttons.GetButton("cancel");
            bSave.Content = buttons.GetButton("saveSettings");
            bAbout.Content = buttons.GetButton("about");
            bDRM.Content = buttons.GetButton("changeDRM");
            bBugReporter.Content = buttons.GetButton("reportBug");
        }

        private void initWindow()
        {
            WriteLog("Initializing settings window");

            WriteLog("Loading application settings");
            settings = Settings.getInstance();

            WriteLog("Setting UI elements based on application settings");
            if (Settings.AOT)
            {
                this.Topmost = true;
                cbAOT.IsChecked = true;
            }
            else
                this.Topmost = false;

            if (Settings.StartUp)
                cbStartup.IsChecked = true;
            if (Settings.CloseOnRun)
                cbCloseOnComplete.IsChecked = true;

            WriteLog("Loading up application theme");

			string theme = (Settings.Theme == Settings.ThemeType.Light) ? "Light" : "Dark";

			App.Current.Resources.MergedDictionaries.Add(
				new ResourceDictionary { Source = new Uri(@"pack://application:,,,/MaterialDesignThemes.Wpf;component/Themes/MaterialDesignTheme." + theme + ".xaml", UriKind.Absolute) });

			if (theme.Equals("Light"))
				rbThemeLight.IsChecked = true;
			else if (theme.Equals("Dark"))
				rbThemeDark.IsChecked = true;

            WriteLog("Loading installed language packs");
            FillLanguageComboBox();
        }
		
		private void FillLanguageComboBox()
        {
            WriteLog("Populating language combo box with installed language packs");
            string[] files = System.IO.Directory.GetFiles(Locale.LocaleDir);
            languages = LocaleList.GetLocales();
            var lfiles = new List<string>();
            foreach (string file in files)
            {
                int start = file.IndexOf("locale\\");
                int end = file.Length;
                string ld = file.Substring(start, end - start);
                lfiles.Add(ld.Substring(ld.IndexOf("\\") + 1, ld.Length - ld.IndexOf("\\") - 1));
            }

            var inclangs = new List<string>();
            for(int i = 0; i < lfiles.Count; i++)
            {
                if (languages.ContainsKey(lfiles[i].Remove(lfiles[i].IndexOf(".lang"))))
                {
                    inclangs.Add(lfiles[i].Remove(lfiles[i].IndexOf(".lang")) + " - " + languages[lfiles[i].Remove(lfiles[i].IndexOf(".lang"))]);
                }
            }

            cmbLanguage.ItemsSource = inclangs;
            
            if (languages.ContainsKey(Settings.Language))
            {
                cmbLanguage.SelectedValue = Settings.Language + " - " + languages[Settings.Language];
            }
        }

        private void bCancel_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Terminating settings window");
            this.Close();
        }

        private async void bSave_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Save button clicked");
            Save();
			MessageDialogResult result = await this.ShowMessageAsync(strings.GetString("mb_set_title"), strings.GetString("mb_set_saved"), MessageDialogStyle.Affirmative, mds);
			if (result == MessageDialogResult.Affirmative)
				Restart();
        }

        private void bAbout_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Loading about window");
            new AboutWindow(logger).Show();
        }

        private void bDRM_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Change DRM button clicked. Reverting all app settings to null, restarting...");
            if (System.IO.File.Exists(Settings.MELocation + @"\Binaries\TexMod.exe"))
                System.IO.File.Delete(Settings.MELocation + @"\Binaries\TexMod.exe");
            if (System.IO.File.Exists(Settings.ME2Location + @"\Binaries\TexMod.exe"))
                System.IO.File.Delete(Settings.ME2Location + @"\Binaries\TexMod.exe");
            if (System.IO.File.Exists(Settings.ME3Location + @"\Binaries\Win32\TexMod.exe"))
                System.IO.File.Delete(Settings.ME3Location + @"\Binaries\Win32\TexMod.exe");

			string[] scs = Directory.GetFiles(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + @"\");
			foreach(string file in scs)
			{
				if(file.Contains("(MELauncher).url"))
				{
					File.Delete(file);
				}
			}

            settings.SaveSettings(
                Settings.AppID,
                Settings.Version,
                "", "", "", "", "", "",
                Settings.DRMType.NULL,
                Settings.ThemeType.Light,
                Settings.Language,
                Settings.CloseOnRun,
                Settings.AOT,
                Settings.StartUp);

            Restart();
        }

        private void Save()
        {
            WriteLog("Saving settings");
            Settings.ThemeType themeType = Settings.ThemeType.Dark;
            if ((bool)rbThemeLight.IsChecked)
                themeType = Settings.ThemeType.Light;
            else if ((bool)rbThemeDark.IsChecked)
                themeType = Settings.ThemeType.Dark;

            string lang = cmbLanguage.SelectedValue.ToString();
            string culture = lang.Substring(0, lang.IndexOf(" "));

            WriteLog("Writing all settings to application settings file");
            settings.SaveSettings(
                Settings.AppID,
                Settings.Version,
                Settings.SteamLocation,
                Settings.OriginLocation,
                Settings.MELocation,
                Settings.ME2Location,
                Settings.ME3Location,
				Settings.MEALocation,
                Settings.DRM,
                themeType,
                culture,
                (bool)cbCloseOnComplete.IsChecked,
                (bool)cbAOT.IsChecked,
                (bool)cbStartup.IsChecked);
        }

        private void Restart()
        {
            WriteLog("Restarting application");
			ProcessStartInfo psi = new ProcessStartInfo();
			psi.FileName = Application.ResourceAssembly.Location;
			psi.Arguments = "/AppRestart";
			Process.Start(psi);
            Application.Current.Shutdown();
        }

        private void bBugReporter_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Launching bug reporting tool");
            new BugReporter(logger).ShowDialog();
        }

        private async void Donate_Click(object sender, RoutedEventArgs e)
        {
            WriteLog("Donate button clicked");
			MessageDialogResult result = await this.ShowMessageAsync(strings.GetString("mb_donate_title"), strings.GetString("mb_donate_text"), MessageDialogStyle.AffirmativeAndNegative, mds);
            if (result == MessageDialogResult.Affirmative)
            {
                WriteLog("Opening donation page in default web browser");
                Process.Start("https://www.paypal.me/pazuzu156/1");
            }
            else
                WriteLog("User canceled action");
        }
    }
}
